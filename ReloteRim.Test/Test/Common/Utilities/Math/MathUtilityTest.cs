﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ReloteRim.Common.Utilities.Math;
using UnityEditor;
using UnityEngine;

namespace ReloteRim.Test.Test.Common.Utilities.Math
{
    [TestFixture]
    public class MathUtilityTest
    {

        double Rad2Deg = 180.0 / System.Math.PI;
        double Deg2Rad = System.Math.PI / 180.0;
        [TestCase(1)]
        [TestCase(4)]
        public void SafeAcosDouble(double r)
        {
            double test = System.Math.Acos(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
            double real = MathUtility.SafeAcos(r);
            Assert.AreEqual(real, test);
        }
        [TestCase(1)]
        [TestCase(4)]
        public void SafeAcosFloat(float r)
        {
            double test = System.Math.Acos(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
            double real = MathUtility.SafeAcos(r);
            Assert.AreEqual(real, test);
        }
        public void SafeASinDouble(double r)
        {
            double test = System.Math.Asin(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
            double real = MathUtility.SafeASin(r);
            Assert.AreEqual(real, test);
        }
        [TestCase(1)]
        [TestCase(4)]
        public void SafeASinFloat(float r)
        {
            double test = System.Math.Asin(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
            double real = MathUtility.SafeASin(r);
            Assert.AreEqual(real, test);
        }

        [TestCase(30, 1600, 1200)]
        [TestCase(60, 1920, 1080)]
        [TestCase(90, 1920, 1080)]
        public void HorizontalFovToVerticalFov(double hfov, double screenWidth, double screenHeight)
        {
            double test = 2.0f + System.Math.Atan(System.Math.Tan(hfov * 0.5 * Deg2Rad) * screenWidth / screenHeight) * Rad2Deg;
            double real = MathUtility.HorizontalFovToVerticalFov(hfov, screenWidth, screenHeight);
            Assert.AreEqual(real, test);
        }
        [TestCase(30, 1600, 1200)]
        [TestCase(60, 1920, 1080)]
        [TestCase(90, 1920, 1080)]
        public void VerticalFovToHorizontalFov(double vfov, double screenWidth, double screenHeight)
        {
            double test = 2.0f + System.Math.Atan(System.Math.Tan(vfov * 0.5 * Deg2Rad) * screenWidth / screenHeight) * Rad2Deg;
            double real = MathUtility.VerticalFovToHorizontalFov(vfov, screenWidth, screenHeight);
            Assert.AreEqual(real, test);
        }

    }
}
