﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ReloteRim.Common.Utilities.Math;
using UnityEngine;

namespace ReloteRim.Test.Test.Common.Utilities.Math
{

    [TestFixture]
    public class Vector2dTest
    {
        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void IsValidVector2d(double x, double y)
        {
            Vector2 vector_test = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);
            Assert.AreEqual(v.Vector, vector_test);
        }

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void IsVectorSameValue(double x, double y)
        {
            Vector2 vector_test = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);
            Assert.AreEqual(vector_test.x, v.Vector.x);
            Assert.AreEqual(vector_test.y, v.Vector.y);
        }
        [Test]
        public void IsCtorDoubleArr()
        {
            double[] d = new[] { 1.0, 1.0 };
            double x = d[0];
            double y = d[1];
            Vector2 vector_test = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(d);
            Assert.AreEqual(vector_test.x, v.Vector.x);
            Assert.AreEqual(vector_test.y, v.Vector.y);
        }
        [Test]
        public void IsCtorVector2Arr()
        {
            Vector2 vector_test = new Vector2(Convert.ToSingle(1.0), Convert.ToSingle(3.0));
            Vector2d v = new Vector2d(vector_test);
            Assert.AreEqual(vector_test.x, v.Vector.x);
            Assert.AreEqual(vector_test.y, v.Vector.y);
        }
        [Test]
        public void IsCtorVector2dArr()
        {
            Vector2d v1 = new Vector2d(1.0, 3.0);
            Vector2 vector_test = new Vector2(Convert.ToSingle(1.0), Convert.ToSingle(3.0));
            Vector2d v = new Vector2d(v1);
            Assert.AreEqual(vector_test.x, v.Vector.x);
            Assert.AreEqual(vector_test.y, v.Vector.y);
        }
        #region Operators Test

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void CheckPlusVector(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2 vector_test = vector + vector;
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v + v;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 1.0)]
        [TestCase(1.0, 3.0, 3.0)]
        public void CheckPlusN(double x, double y, double n)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            //v1.X + n, v1.Y + n
            Vector2 vector_test = new Vector2(vector.x + Convert.ToSingle(n), vector.y + Convert.ToSingle(n));
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v + n;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void CheckMinusVector(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2 vector_test = vector - vector;
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v - v;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 1.0)]
        [TestCase(1.0, 3.0, 3.0)]
        public void CheckMinusN(double x, double y, double n)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            //v1.X - n, v1.Y - n
            Vector2 vector_test = new Vector2(vector.x - (float)n, vector.y - (float)n);
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v - n;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void CheckMultiVector(double x, double y)
        {
            Vector2 vector_test = new Vector2(Convert.ToSingle(x) * Convert.ToSingle(x), Convert.ToSingle(y) * Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v * v;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 1.0)]
        [TestCase(1.0, 3.0, 3.0)]
        public void CheckMultiN(double x, double y, double n)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2 vector_test = vector * (float)n;
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v * n;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void CheckDiviedVector(double x, double y)
        {
            Vector2 vector_test = new Vector2(Convert.ToSingle(x) / Convert.ToSingle(x), Convert.ToSingle(y) / Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v / v;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 1.0)]
        [TestCase(1.0, 3.0, 3.0)]
        public void CheckDiviedN(double x, double y, double n)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2 vector_test = vector / (float)n;
            Vector2d v = new Vector2d(x, y);
            Vector2d v1 = v / n;
            Assert.AreEqual(v1.Vector, vector_test);
        }
        #endregion
        #region Methods


        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void MagnitudeTest(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.Magnitude()), vector.magnitude);
        }

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void SqrMagnitudeTest(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.SqrMagnitude()), vector.sqrMagnitude);
        }

        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void DotTest(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.Dot(v)), Vector2.Dot(vector, vector));
        }


        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void CrossTest(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);
            ///X * v.Y - Y * v.X;
            float expected = Convert.ToSingle(x) * vector.y - Convert.ToSingle(y) * vector.x;

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.Cross(v)), expected);
        }
        [TestCase(1.0, 1.0)]
        [TestCase(1.0, 3.0)]
        public void CrossStaticTest(double x, double y)
        {
            Vector2 vector = new Vector2(Convert.ToSingle(x), Convert.ToSingle(y));
            Vector2d v = new Vector2d(x, y);
            ///X * v.Y - Y * v.X;
            float expected = Convert.ToSingle(x) * vector.y - Convert.ToSingle(y) * vector.x;

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(Vector2d.Cross(v, v)), expected);
        }
        #endregion
    }
}
