﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ReloteRim.Common.Utilities.Math;
using UnityEngine;

namespace ReloteRim.Test.Test.Common.Utilities.Math
{

    [TestFixture]
    public class Vector3dTest
    {
        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void IsValidVector3d(double x, double y, double z)
        {
            Vector3 vector_test = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);
            Assert.AreEqual(v.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void IsVectorSameValue(double x, double y, double z)
        {
            Vector3 vector_test = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);
            Assert.AreEqual(vector_test.x, v.Vector.x);
            Assert.AreEqual(vector_test.y, v.Vector.y);
            Assert.AreEqual(vector_test.z, v.Vector.z);
        }

        #region Operators Test

        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void CheckPlusVector(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = vector + vector;
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = v + v;
            Assert.AreEqual(v1.Vector, vector_test);
        }


        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void CheckMinusVector(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = vector - vector;
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = v - v;
            Assert.AreEqual(v1.Vector, vector_test);
        }



        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void CheckMultiVector(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = new Vector3(vector.x * vector.x, vector.y * vector.y, vector.z * vector.z);
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = v * v;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 1.0, 2.0)]
        [TestCase(1.0, 3.0, 3.0, 2.0)]
        public void CheckMultiNAndVector(double x, double y, double z, double n)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = (float)n * vector;
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = n * v;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        [TestCase(1.0, 1.0, 1.0, 2.0)]
        [TestCase(1.0, 3.0, 3.0, 2.0)]
        public void CheckMultiVectorAndN(double x, double y, double z, double n)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = vector * (float)n;
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = v * n;
            Assert.AreEqual(v1.Vector, vector_test);
        }


        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void CheckDiviedVector(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = new Vector3(vector.x / vector.x, vector.y / vector.y, vector.z / vector.z);
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = v / v;
            Assert.AreEqual(v1.Vector, vector_test);
        }


        [TestCase(1.0, 1.0, 1.0, 2.0)]
        [TestCase(1.0, 3.0, 3.0, 2.0)]
        public void CheckDiviedVectorAndN(double x, double y, double z, double n)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3 vector_test = vector / (float)n;
            Vector3d v = new Vector3d(x, y, z);
            Vector3d v1 = v / n;
            Assert.AreEqual(v1.Vector, vector_test);
        }

        #endregion

        #region Methods
        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void MagnitudeTest(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.Magnitude()), vector.magnitude);
        }

        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void SqrMagnitudeTest(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.SqrMagnitude()), vector.sqrMagnitude);
        }

        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void DotTest(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Convert.ToSingle(v.Dot(v)), Vector3.Dot(vector, vector));
        }


        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void CrossTest(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);

            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(v.Cross(v).Vector, Vector3.Cross(vector, vector));
        }

        [TestCase(1.0, 1.0, 6.0)]
        [TestCase(1.0, 3.0, 5.0)]
        public void CrossStaticTest(double x, double y, double z)
        {
            Vector3 vector = new Vector3(Convert.ToSingle(x), Convert.ToSingle(y), Convert.ToSingle(z));
            Vector3d v = new Vector3d(x, y, z);
            Assert.AreEqual(v.Vector, vector);
            Assert.AreEqual(Vector3d.Cross(v, v).Vector, Vector3.Cross(vector, vector));
        }

        #endregion
    }
}
