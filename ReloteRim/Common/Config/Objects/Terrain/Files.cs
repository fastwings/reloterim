﻿using System;

namespace ReloteRim.Common.Config.Objects.Terrain
{
    public class Files
    {
        public String Transmittance { get; set; }
        public String Irradiance { get; set; }
        public String Inscatter { get; set; }
    }
}
