﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;

namespace ReloteRim.Common.Config.Objects.Gamemode
{
    public class Debug
    {
        public bool Active { get; set; }
        public bool RotationViaMouseSun { get; set; }
        public LinesDebug DebugLines { get; set; }

    }
}
