﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ReloteRim.Common.Config.Objects;
using ReloteRim.Common.Config.Objects.Gamemode;
using ReloteRim.Common.Config.Objects.Terrain;
using ReloteRim.Common.Utilities.Misc;
using YamlDotNet.Core;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ReloteRim.Common.Config
{
    public class ConfigSystem
    {
        private List<string> configCorePath = new List<string>() { Application.dataPath, "Configs", "Core" };
        enum ConfigTypes
        {

        }
        private static ConfigSystem instance;
        private ConfigSystem()
        {

        }

        public static ConfigSystem Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConfigSystem();
                    instance.loadConfigs();
                }
                return instance;
            }
        }

        public GamemodeConfigControl GameModeConfig { get; private set; }
        public TerrainConfigControl TerrainConfig { get; private set; }
        private void loadConfigs()
        {
            loadTerrienFilesConfig();
            loadGamemodeConfig();
        }

        private void loadTerrienFilesConfig()
        {
            List<string> configPath = configCorePath;
            configPath.Add("terrain.cfg");
            string text = System.IO.File.ReadAllText(PathUtills.CombinePaths(configPath.ToArray()));
            var steam = new StringReader(text);
            var deserializer = new Deserializer(namingConvention: new CamelCaseNamingConvention());
            TerrainConfig = deserializer.Deserialize<TerrainConfigControl>(steam);
        }
        private void loadGamemodeConfig()
        {
            List<string> configPath = configCorePath;
            configPath.Add("mode.cfg");
            string text = System.IO.File.ReadAllText(PathUtills.CombinePaths(configPath.ToArray()));
            var steam = new StringReader(text);
            var deserializer = new Deserializer(namingConvention: new CamelCaseNamingConvention());
            GameModeConfig = deserializer.Deserialize<GamemodeConfigControl>(steam);
        }

    }
}
