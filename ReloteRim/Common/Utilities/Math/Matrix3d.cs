﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
#pragma warning disable 660, 661

namespace ReloteRim.Common.Utilities.Math
{
    public class Matrix3d
    {
        private double[,] matrix = new double[3, 3];
        public double[,] Matrix
        {
            get
            {
                return matrix;
            }
            set
            {
                matrix = value;
            }
        }
        public Matrix3d()
        {
        }
        public Matrix3d(double p0, double p1, double p2,
                        double p3, double p4, double p5,
                        double p6, double p7, double p8)
        {
            matrix[0, 0] = p0;
            matrix[0, 1] = p1;
            matrix[0, 2] = p2;

            matrix[1, 0] = p3;
            matrix[1, 1] = p4;
            matrix[1, 2] = p5;

            matrix[2, 0] = p6;
            matrix[2, 1] = p7;
            matrix[2, 2] = p8;
        }
        public Matrix3d(Matrix3d matrix)
        {
            System.Array.Copy(matrix.matrix, this.matrix, 9);
        }
        public Matrix3d(double[,] m)
        {
            System.Array.Copy(m, this.matrix, 9);
        }

        #region Operators

        public static Matrix3d operator +(Matrix3d m1, Matrix3d m2)
        {
            Matrix3d sum = new Matrix3d();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] + m2.Matrix[row, col];
                }
            }
            return sum;
        }

        public static Matrix3d operator -(Matrix3d m1, Matrix3d m2)
        {
            Matrix3d sum = new Matrix3d();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] - m2.Matrix[row, col];
                }
            }
            return sum;
        }


        public static Matrix3d operator *(Matrix3d m1, Matrix3d m2)
        {
            Matrix3d sum = new Matrix3d();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    sum.matrix[row, col] = m1.Matrix[row, 0] * m2.Matrix[0, col] +
                                            m1.Matrix[row, 1] * m2.Matrix[1, col] +
                                            m1.Matrix[row, 2] * m2.Matrix[2, col];
                }
            }
            return sum;
        }

        public static Matrix3d operator *(Matrix3d m1, double n)
        {
            Matrix3d sum = new Matrix3d();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] * n;
                }
            }
            return sum;
        }
        public static Vector3d operator *(Matrix3d m1, Vector3d v)
        {
            Vector3d vv = new Vector3d();
            vv.X = m1.Matrix[0, 0] * v.X + m1.Matrix[0, 1] * v.Y + m1.Matrix[0, 2] * v.Z;
            vv.Y = m1.Matrix[1, 0] * v.X + m1.Matrix[1, 1] * v.Y + m1.Matrix[1, 2] * v.Z;
            vv.Z = m1.Matrix[2, 0] * v.X + m1.Matrix[2, 1] * v.Y + m1.Matrix[2, 2] * v.Z;
            return vv;
        }

        public static bool operator ==(Matrix3d m1, Matrix3d m2)
        {

            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (m1.Matrix[row, col] != m2.Matrix[row, col]) return false;
                }
            }
            return true;
        }

        public static bool operator !=(Matrix3d m1, Matrix3d m2)
        {

            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    if (m1.Matrix[row, col] == m2.Matrix[row, col]) return false;
                }
            }
            return true;
        }
        #endregion

        #region Methods
        public Matrix3d Transpose()
        {
            Matrix3d transpose = new Matrix3d();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    transpose.Matrix[row, col] = matrix[col, row];
                }
            }
            return transpose;
        }

        public bool Inverse(ref Matrix3d mInv, double tolerance = 1e-06)
        {
            // Invert a 3x3 using cofactors.  This is about 8 times faster than
            // the Numerical Recipes code which uses Gaussian elimination.
            mInv.matrix[0, 0] = matrix[1, 1] * matrix[2, 2] - matrix[1, 2] * matrix[2, 1];
            mInv.matrix[0, 1] = matrix[0, 2] * matrix[2, 1] - matrix[0, 1] * matrix[2, 2];
            mInv.matrix[0, 2] = matrix[0, 1] * matrix[1, 2] - matrix[0, 2] * matrix[1, 1];
            mInv.matrix[1, 0] = matrix[1, 2] * matrix[2, 0] - matrix[1, 0] * matrix[2, 2];
            mInv.matrix[1, 1] = matrix[0, 0] * matrix[2, 2] - matrix[0, 2] * matrix[2, 0];
            mInv.matrix[1, 2] = matrix[0, 2] * matrix[1, 0] - matrix[0, 0] * matrix[1, 2];
            mInv.matrix[2, 0] = matrix[1, 0] * matrix[2, 1] - matrix[1, 1] * matrix[2, 0];
            mInv.matrix[2, 1] = matrix[0, 1] * matrix[2, 0] - matrix[0, 0] * matrix[2, 1];
            mInv.matrix[2, 2] = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            double det = matrix[0, 0] * mInv.matrix[0, 0] + matrix[0, 1] * mInv.matrix[1, 0] + matrix[0, 2] * mInv.matrix[2, 0];
            if (System.Math.Abs(det) <= tolerance)
            {
                return false;
            }

            double fInvDet = 1.0 / det;

            for (int iRow = 0; iRow < 3; iRow++)
            {
                for (int iCol = 0; iCol < 3; iCol++)
                    mInv.matrix[iRow, iCol] *= fInvDet;
            }
            return true;
        }
        public Matrix3d Inverse(double tolerance = 1e-06)
        {
            Matrix3d kInverse = new Matrix3d();
            Inverse(ref kInverse, tolerance);
            return kInverse;
        }
        public static Matrix3d Identity()
        {
            return new Matrix3d(1, 0, 0, 0, 1, 0, 0, 0, 1);
        }
        public Vector3d GetColumn(int col)
        {
            return new Vector3d(matrix[0, col], matrix[1, col], matrix[2, col]);
        }
        public void SetColumn(int col, Vector3d v)
        {
            matrix[0, col] = v.X;
            matrix[1, col] = v.Y;
            matrix[2, col] = v.Z;
        }
        public void SetRow(int row, Vector3d v)
        {
            matrix[row, 0] = v.X;
            matrix[row, 1] = v.Y;
            matrix[row, 2] = v.Z;
        }
        public Vector3d GetRow(int row)
        {
            return new Vector3d(matrix[row, 0], matrix[row, 1], matrix[row, 2]);
        }
        public Matrix4x4 ToMatrix4x4()
        {
            Matrix4x4 mat = new Matrix4x4();

            mat.m00 = (float)matrix[0, 0]; mat.m01 = (float)matrix[0, 1]; mat.m02 = (float)matrix[0, 2];
            mat.m10 = (float)matrix[1, 0]; mat.m11 = (float)matrix[1, 1]; mat.m12 = (float)matrix[1, 2];
            mat.m20 = (float)matrix[2, 0]; mat.m21 = (float)matrix[2, 1]; mat.m22 = (float)matrix[2, 2];
            return mat;
        }
        public Matrix4d ToMatrix4d()
        {
            return new Matrix4d(matrix[0, 0], matrix[0, 1], matrix[0, 2], 0.0,
                                  matrix[1, 0], matrix[1, 1], matrix[1, 2], 0.0,
                                  matrix[2, 0], matrix[2, 1], matrix[2, 2], 0.0,
                                  0.0, 0.0, 0.0, 0.0);
        }
        #endregion

        public override string ToString()
        {
            return string.Format("[Matrix3D: ({0} , {1} , {2} \n, {3} , {4} , {5} \n, {6} ,{7} ,{8})]", matrix[0, 0], matrix[0, 1], matrix[0, 2], matrix[1, 0], matrix[1, 1], matrix[1, 2], matrix[2, 0], matrix[2, 1], matrix[2, 2]);
        }
    }
}
