﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Common.Utilities.Math
{
    public class Quaterniond
    {
        public Vector4d Vector { get; set; }

        public Quaternion Quaternion
        {
            get
            {
                return new Quaternion((float)Vector.X, (float)Vector.Y, (float)Vector.Z, (float)Vector.W);
            }
        }
        public Vector3d Vector3d
        {
            get
            {
                return new Vector3d((float)Vector.X, (float)Vector.Y, (float)Vector.Z);
            }
        }
        public Matrix3d Matrix3d
        {
            get
            {
                double xx = Vector.X * Vector.X,
                xy = Vector.X * Vector.Y,
                xz = Vector.X * Vector.Z,
                xw = Vector.X * Vector.W,
                yy = Vector.Y * Vector.Y,
                yz = Vector.Y * Vector.Z,
                yw = Vector.Y * Vector.W,
                zz = Vector.Z * Vector.Z,
                zw = Vector.Z * Vector.W;

                return new Matrix3d(
                    1.0 - 2.0 * (yy + zz), 2.0 * (xy - zw), 2.0 * (xz + yw),
                    2.0 * (xy + zw), 1.0 - 2.0 * (xx + zz), 2.0 * (yz - xw),
                    2.0 * (xz - yw), 2.0 * (yz + xw), 1.0 - 2.0 * (xx + yy)
                );
            }
        }

        public Matrix4d Matrix4d
        {
            get
            {
                double xx = Vector.X * Vector.X,
                xy = Vector.X * Vector.Y,
                xz = Vector.X * Vector.Z,
                xw = Vector.X * Vector.W,
                yy = Vector.Y * Vector.Y,
                yz = Vector.Y * Vector.Z,
                yw = Vector.Y * Vector.W,
                zz = Vector.Z * Vector.Z,
                zw = Vector.Z * Vector.W;


                return new Matrix4d
                (
                    1.0 - 2.0 * (yy + zz), 2.0 * (xy - zw), 2.0 * (xz + yw), 0.0,
                    2.0 * (xy + zw), 1.0 - 2.0 * (xx + zz), 2.0 * (yz - xw), 0.0,
                    2.0 * (xz - yw), 2.0 * (yz + xw), 1.0 - 2.0 * (xx + yy), 0.0,
                    0.0, 0.0, 0.0, 1.0
                );
            }
        }
        public Quaterniond()
        {
            Vector = new Vector4d();
        }

        public Quaterniond(double x, double y, double z, double w)
        {
            Vector = new Vector4d(x, y, z, w);
        }

        public Quaterniond(double x, double y, double z)
        {
            Vector = new Vector4d(x, y, z);
        }
        public Quaterniond(double[] v)
        {
            Vector = new Vector4d(v);
        }
        public Quaterniond(Quaternion q)
        {
            Vector = new Vector4d(q.x, q.y, q.z, q.w);
        }

        public Quaterniond(Vector3d v, double angle)
        {
            Vector3d axis = v.Normalized();
            double a = angle * 0.5;
            double sin = System.Math.Sin(a);
            double cos = System.Math.Cos(a);
            Vector = new Vector4d(axis.X * sin, axis.Y * sin, axis.Z * sin, cos);
        }

        public Quaterniond(Vector3 v, double angle)
        {
            Vector3d axis = new Vector3d(v.normalized);
            double a = angle * 0.5;
            double sin = System.Math.Sin(a);
            double cos = System.Math.Cos(a);
            Vector = new Vector4d(axis.X * sin, axis.Y * sin, axis.Z * sin, cos);
        }

        public Quaterniond(Vector3d to, Vector3d from)
        {
            Vector4d quaternionVector = new Vector4d();
            Vector3d t = to.Normalized();
            Vector3d f = from.Normalized();
            double dotProdPlus = 1.0 + f.Dot(t);
            if (dotProdPlus < 1e-7)
            {
                quaternionVector.W = 0;
                if (System.Math.Abs(f.X) < 0.6)
                {
                    double norm = System.Math.Sqrt(1 - f.X * f.X);
                    quaternionVector.X = 0;
                    quaternionVector.Y = f.Z / norm;
                    quaternionVector.Z = -f.Z / norm;
                }
                else if (System.Math.Abs(f.Y) < 0.6)
                {
                    double norm = System.Math.Sqrt(1 - f.Y * f.Y);
                    quaternionVector.X = -f.Z / norm;
                    quaternionVector.Y = 0;
                    quaternionVector.Z = f.X / norm;
                }
                else
                {
                    double norm = System.Math.Sqrt(1 - f.Z * f.Z);
                    quaternionVector.X = f.Y / norm;
                    quaternionVector.Y = -f.X / norm;
                    quaternionVector.Z = 0;
                }
            }
            else
            {
                double norm = System.Math.Sqrt(0.5 * dotProdPlus);
                Vector3d v = (f.Cross(t) / 2.0 * norm);
                quaternionVector.X = v.X;
                quaternionVector.Y = v.Y;
                quaternionVector.Z = v.Z;
                quaternionVector.W = norm;
            }
        }
        public Quaterniond(Vector3 to, Vector3 from)
        {
            Vector4d quaternionVector = new Vector4d();
            Vector3d t = new Vector3d(to.normalized);
            Vector3d f = new Vector3d(from.normalized);
            double dotProdPlus = 1.0 + f.Dot(t);
            if (dotProdPlus < 1e-7)
            {
                quaternionVector.W = 0;
                if (System.Math.Abs(f.X) < 0.6)
                {
                    double norm = System.Math.Sqrt(1 - f.X * f.X);
                    quaternionVector.X = 0;
                    quaternionVector.Y = f.Z / norm;
                    quaternionVector.Z = -f.Z / norm;
                }
                else if (System.Math.Abs(f.Y) < 0.6)
                {
                    double norm = System.Math.Sqrt(1 - f.Y * f.Y);
                    quaternionVector.X = -f.Z / norm;
                    quaternionVector.Y = 0;
                    quaternionVector.Z = f.X / norm;
                }
                else
                {
                    double norm = System.Math.Sqrt(1 - f.Z * f.Z);
                    quaternionVector.X = f.Y / norm;
                    quaternionVector.Y = -f.X / norm;
                    quaternionVector.Z = 0;
                }
            }
            else
            {
                double norm = System.Math.Sqrt(0.5 * dotProdPlus);
                Vector3d v = (f.Cross(t) / 2.0 * norm);
                quaternionVector.X = v.X;
                quaternionVector.Y = v.Y;
                quaternionVector.Z = v.Z;
                quaternionVector.W = norm;
            }
        }

        #region Operators
        public static Quaterniond operator *(Quaterniond q1, Quaterniond q2)
        {

            return new Quaterniond(q2.Vector.W * q1.Vector.X + q2.Vector.X * q1.Vector.W + q2.Vector.Y * q1.Vector.Z - q2.Vector.Z * q1.Vector.Y,
                            q2.Vector.W * q1.Vector.Y - q2.Vector.X * q1.Vector.Z + q2.Vector.Y * q1.Vector.W + q2.Vector.Z * q1.Vector.X,
                            q2.Vector.W * q1.Vector.Z + q2.Vector.X * q1.Vector.Y - q2.Vector.Y * q1.Vector.X + q2.Vector.Z * q1.Vector.W,
                            q2.Vector.W * q1.Vector.W - q2.Vector.X * q1.Vector.X - q2.Vector.Y * q1.Vector.Y - q2.Vector.Z * q1.Vector.Z);
        }
        public static Vector3d operator *(Quaterniond q, Vector3d v)
        {
            return q.Matrix3d * v;
        }
        #endregion

        #region Method
        public Quaterniond Inverse()
        {
            return new Quaterniond(-Vector.X, -Vector.Y, -Vector.Z, Vector.W);
        }
        public void Normalize()
        {
            double invLength = 1.0 / Vector.SqrMagnitude();
            Vector.X *= invLength;
            Vector.Y *= invLength;
            Vector.Z *= invLength;
            Vector.W *= invLength;
        }
        public Quaterniond Normalized()
        {
            double invLength = 1.0 / Vector.SqrMagnitude();
            return new Quaterniond(Vector.X * invLength, Vector.Y * invLength, Vector.Z * invLength, Vector.Z * invLength);
        }

        public Quaterniond Slerp(Quaterniond from, Quaterniond to, double t)
        {
            Quaternion q = Quaternion.Slerp(from.Quaternion, to.Quaternion, (float)t);
            return new Quaterniond(q);
        }
        #endregion
    }
}
