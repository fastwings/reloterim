﻿using ReloteRim.Common.Utilities.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Common.Utilities.Math
{
    public class FourierGPU
    {
        const int PASS_X_1 = 0, PASS_Y_1 = 1;
        const int PASS_X_2 = 2, PASS_Y_2 = 3;
        const int PASS_X_3 = 4, PASS_Y_3 = 5;
        int size;
        float fsize;
        int pass;
        Texture2D[] m_butterflyLookupTable = null;
        Material mat;

        public FourierGPU(int size)
        {
            if (size > 256)
            {
                Debug.LogError(string.Format("FourierGPU -  fourier grid size must not be greater than 256, changing to 256 , Receive: {0}", size));
            }
            if (!Mathf.IsPowerOfTwo(size))
            {
                Debug.LogWarning("FourierGPU - fourier grid size must be pow2 number, changing to nearest pow2 number");
                size = Mathf.NextPowerOfTwo(size);
            }
            Shader shader = Shader.Find("ReloteRim/Planets/Core/Math/Fourier");
            if (shader == null)
            {
                Debug.LogError(string.Format("FourierGPU -  Could not find shader ReloteRim/Planets/Core/Math/Fourier"));
            }
            mat = new Material(shader);
            this.size = size;
            fsize = (float)size;
            pass = (int)(Mathf.Log(fsize) / Mathf.Log(2.0f));
            m_butterflyLookupTable = new Texture2D[pass];
            mat.SetFloat("_Size", fsize);

        }
        #region Private Mathods
        private int BitReverse(int i)
        {
            int j = i;
            int Sum = 0;
            int W = 1;
            int M = size / 2;
            while (M != 0)
            {
                j = ((i & M) > M - 1) ? 1 : 0;
                Sum += j * W;
                W *= 2;
                M /= 2;
            }
            return Sum;
        }
        private Texture2D Make1DTex(int i)
        {
            Texture2D tex = new Texture2D(size, 1, TextureFormat.ARGB32, false, true);
            tex.filterMode = FilterMode.Point;
            tex.wrapMode = TextureWrapMode.Clamp;
            return tex;
        }
        private void ComputeButterflyLookupTable()
        {
            for (int i = 0; i < pass; i++)
            {
                int nBlocks = (int)Mathf.Pow(2, pass - 1 - i);
                int nHInputs = (int)Mathf.Pow(2, i);
                for (int j = 0; j < nBlocks; j++)
                {
                    for (int k = 0; k < nHInputs; k++)
                    {
                        int i1, i2, j1, j2;
                        if (i == 0)
                        {
                            i1 = j * nHInputs * 2 + k;
                            i2 = j * nHInputs * 2 + nHInputs + k;
                            j1 = BitReverse(i1);
                            j2 = BitReverse(i2);
                        }
                        else
                        {
                            i1 = j * nHInputs * 2 + k;
                            i2 = j * nHInputs * 2 + nHInputs + k;
                            j1 = i1;
                            j2 = i2;
                        }

                        m_butterflyLookupTable[i].SetPixel(i1, 0, new Color((float)j1 / 255.0f, (float)j2 / 255.0f, (float)(k * nBlocks) / 255.0f, 0));
                        m_butterflyLookupTable[i].SetPixel(i2, 0, new Color((float)j1 / 255.0f, (float)j2 / 255.0f, (float)(k * nBlocks) / 255.0f, 1));
                    }
                }
                m_butterflyLookupTable[i].Apply();
            }
        }
        #endregion

        public int PeformFFT(RenderTexture[] data0, RenderTexture[] data1)
        {
            RenderTexture[] pass0 = new RenderTexture[] { data0[0], data1[0] };
            RenderTexture[] pass1 = new RenderTexture[] { data0[1], data1[1] };
            int i = 0;
            int idx = 0;
            int idx1 = 0;
            int j = 0;
            for (i = 0; i < pass; i++, j++)
            {
                idx = j % 2;
                idx1 = (j + 1) % 2;

                mat.SetTexture("_ButterFlyLookUp", m_butterflyLookupTable[i]);

                mat.SetTexture("_ReadBuffer0", data0[idx1]);
                mat.SetTexture("_ReadBuffer1", data1[idx1]);

                if (idx == 0)
                    RTUtility.MultiTargetBlit(pass0, mat, PASS_Y_2);
                else
                    RTUtility.MultiTargetBlit(pass1, mat, PASS_Y_2);

            }
            return idx;
        }
        public int PeformFFT(RenderTexture[] data0, RenderTexture[] data1, RenderTexture[] data2)
        {

            RenderTexture[] pass0 = new RenderTexture[] { data0[0], data1[0] };
            RenderTexture[] pass1 = new RenderTexture[] { data0[1], data1[1] };
            int i = 0;
            int idx = 0;
            int idx1 = 0;
            int j = 0;
            for (i = 0; i < pass; i++, j++)
            {
                idx = j % 2;
                idx1 = (j + 1) % 2;

                mat.SetTexture("_ButterFlyLookUp", m_butterflyLookupTable[i]);

                mat.SetTexture("_ReadBuffer0", data0[idx1]);
                mat.SetTexture("_ReadBuffer1", data1[idx1]);
                mat.SetTexture("_ReadBuffer2", data2[idx1]);

                if (idx == 0)
                    RTUtility.MultiTargetBlit(pass0, mat, PASS_X_3);
                else
                    RTUtility.MultiTargetBlit(pass1, mat, PASS_X_3);
            }
            for (i = 0; i < pass; i++, j++)
            {
                idx = j % 2;
                idx1 = (j + 1) % 2;

                mat.SetTexture("_ButterFlyLookUp", m_butterflyLookupTable[i]);

                mat.SetTexture("_ReadBuffer0", data0[idx1]);
                mat.SetTexture("_ReadBuffer1", data1[idx1]);
                mat.SetTexture("_ReadBuffer2", data2[idx1]);
                if (idx == 0)
                    RTUtility.MultiTargetBlit(pass0, mat, PASS_Y_3);
                else
                    RTUtility.MultiTargetBlit(pass1, mat, PASS_Y_3);
            }
            return idx;
        }
    }
}