﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#pragma warning disable 660, 661

namespace ReloteRim.Common.Utilities.Math
{
    public class Matrix2d
    {
        private double[,] matrix = new double[2, 2];
        public double[,] Matrix
        {
            get
            {
                return matrix;
            }
            set
            {
                matrix = value;
            }
        }

        public Matrix2d()
        {
        }
        public Matrix2d(double p0, double p1, double p2, double p3)
        {
            matrix[0, 0] = p0;
            matrix[0, 1] = p1;
            matrix[1, 0] = p2;
            matrix[1, 1] = p3;
        }
        public Matrix2d(Matrix2d matrix)
        {
            System.Array.Copy(matrix.matrix, this.matrix, 4);
        }

        #region Operators

        public static Matrix2d operator +(Matrix2d m1, Matrix2d m2)
        {
            Matrix2d sum = new Matrix2d();
            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] + m2.Matrix[row, col];
                }
            }
            return sum;
        }

        public static Matrix2d operator -(Matrix2d m1, Matrix2d m2)
        {
            Matrix2d sum = new Matrix2d();
            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] - m2.Matrix[row, col];
                }
            }
            return sum;
        }


        public static Matrix2d operator *(Matrix2d m1, Matrix2d m2)
        {
            Matrix2d sum = new Matrix2d();
            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, 0] * m2.Matrix[0, col] + m1.Matrix[row, 1] * m2.Matrix[1, col];
                }
            }
            return sum;
        }

        public static Matrix2d operator *(Matrix2d m1, double n)
        {
            Matrix2d sum = new Matrix2d();
            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] * n;
                }
            }
            return sum;
        }
        public static Vector2d operator *(Matrix2d m1, Vector2d v)
        {
            Vector2d vv = new Vector2d();
            vv.X = m1.Matrix[0, 0] * v.X + m1.Matrix[0, 1] * v.Y;
            vv.Y = m1.Matrix[1, 0] * v.X + m1.Matrix[1, 1] * v.Y;
            return vv;
        }

        public static bool operator ==(Matrix2d m1, Matrix2d m2)
        {

            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    if (m1.Matrix[row, col] != m2.Matrix[row, col]) return false;
                }
            }
            return true;
        }

        public static bool operator !=(Matrix2d m1, Matrix2d m2)
        {

            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    if (m1.Matrix[row, col] == m2.Matrix[row, col]) return false;
                }
            }
            return true;
        }
        #endregion
        #region Methods
        public Matrix2d Transpose()
        {
            Matrix2d transpose = new Matrix2d();
            for (int row = 0; row < 2; row++)
            {
                for (int col = 0; col < 2; col++)
                {
                    transpose.Matrix[row, col] = matrix[col, row];
                }
            }
            return transpose;
        }
        public bool Inverse(ref Matrix2d mInv, double tolerance = 1e-06)
        {
            double det = Determinant();
            if (System.Math.Abs(det) <= tolerance)
            {
                return false;
            }

            double invDet = 1.0 / det;
            mInv.matrix[0, 0] = matrix[1, 1] * invDet;
            mInv.matrix[0, 1] = -matrix[0, 1] * invDet;
            mInv.matrix[1, 0] = -matrix[1, 0] * invDet;
            mInv.matrix[1, 1] = matrix[0, 0] * invDet;
            return true;
        }
        public Matrix2d Inverse(double tolerance = 1e-06)
        {
            Matrix2d kInverse = new Matrix2d();
            Inverse(ref kInverse, tolerance);
            return kInverse;
        }
        public static Matrix2d Identity()
        {
            return new Matrix2d(1, 0, 0, 1);
        }
        #endregion
        private double Determinant()
        {
            return matrix[0, 0] * matrix[1, 1] - matrix[1, 0] * matrix[0, 1];
        }
        public override string ToString()
        {
            return "[Matrix2D: (" + matrix[0, 0] + "," + matrix[0, 1] + "\n," + matrix[1, 0] + "," + matrix[1, 1] + ")]";
        }
    }
}
