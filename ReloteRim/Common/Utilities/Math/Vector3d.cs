﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Common.Utilities.Math
{
    public class Vector3d
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public Vector3 Vector
        {
            get
            {
                return new Vector3((float)X, (float)Y, (float)Z);
            }
        }
        public Vector2d Vector2D
        {
            get
            {
                return new Vector2d((float)X, (float)Y);
            }
        }

        public Vector3d()
        {
            X = 0.0f;
            Y = 0.0f;
            Z = 0.0f;
        }
        public Vector3d(double n)
        {
            X = Y = Z = n;
        }

        public Vector3d(double x, double y)
        {

            X = x;
            Y = y;
            Z = 0.0f;
        }
        public Vector3d(Vector2d v)
        {

            X = v.X;
            Y = v.Y;
            Z = 0.0f;
        }

        public Vector3d(Vector2d v, double z)
        {

            X = v.X;
            Y = v.Y;
            Z = z;
        }

        public Vector3d(Vector2 v)
        {

            X = (double)v.x;
            Y = (double)v.y;
            Z = 0.0f;
        }

        public Vector3d(Vector2 v, double z)
        {

            X = (double)v.x;
            Y = (double)v.y;
            Z = z;
        }

        public Vector3d(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public Vector3d(double[] n)
        {
            X = n[0];
            Y = n[1];
            Z = n[2];
        }
        public Vector3d(Vector3 v)
        {
            X = v.x;
            Y = v.y;
            Z = v.z;
        }
        public Vector3d(Vector3d v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
        }

        #region Operators
        public static Vector3d operator +(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }
        public static Vector3d operator -(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }
        public static Vector3d operator *(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
        }
        public static Vector3d operator *(double n, Vector3d v)
        {
            return new Vector3d(v.X * n, v.Y * n, v.Z * n);
        }
        public static Vector3d operator *(Vector3d v, double n)
        {
            return new Vector3d(v.X * n, v.Y * n, v.Z * n);
        }
        public static Vector3d operator /(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.X / v2.X, v1.Y / v2.Y, v1.Z / v2.Z);
        }
        public static Vector3d operator /(Vector3d v, double n)
        {
            return new Vector3d(v.X / n, v.Y / n, v.Z / n);
        }
        #endregion

        #region Methods
        /// <summary>
        /// will get Magnitude length of the vector 
        /// </summary>
        /// <returns>double</returns>
        public double SqrMagnitude()
        {
            return X * X + Y * Y + Z * Z;
        }
        /// <summary>
        /// will get Magnitude squared length  of the vector
        /// </summary>
        /// <returns>double</returns>
        public double Magnitude()
        {
            return System.Math.Sqrt(X * X + Y * Y + Z * Z);
        }

        public double Dot(Vector3d v)
        {
            return Convert.ToDouble(Vector3.Dot(Vector, v.Vector));
        }
        /// <summary>
        /// Makes this vector have a magnitude of 1
        /// </summary>
        public Vector3d Normalize()
        {
            double invLength = 1.0 / Magnitude();
            return new Vector3d(X * invLength, Y * invLength, Y * invLength);
        }
        /// <summary>
        /// Makes this vector have a magnitude of l
        /// </summary>
        /// <param name="l"></param>
        public Vector3d Normalize(double l)
        {
            double length = System.Math.Sqrt(X * X + Y * Y + Z * Z);
            double invLength = l / length;
            return new Vector3d(X * invLength, Y * invLength, Y * invLength);
        }
        /// <summary>
        /// Makes this vector have a magnitude of 1
        /// </summary>
        /// <returns>Vector2d</returns>
        public Vector3d Normalized()
        {
            double invLength = 1.0 / Magnitude();
            return new Vector3d(X * invLength, Y * invLength, Z * invLength);
        }
        /// <summary>
        /// Makes this vector have a magnitude of 1
        /// </summary>
        /// <returns>Vector2d</returns>
        public Vector3d Normalized(double l)
        {
            double length = System.Math.Sqrt(X * X + Y * Y + Z * Z);
            double invLength = l / length;
            return new Vector3d(X * invLength, Y * invLength, Z * invLength);
        }
        /// <summary>
        /// Makes this vector have a magnitude of 1
        /// </summary>
        /// <returns>Vector2d</returns>
        public Vector3d Normalized(ref double length)
        {
            length = System.Math.Sqrt(X * X + Y * Y + Z * Z);
            double invLength = length / length;
            return new Vector3d(X * invLength, Y * invLength, Z * invLength);
        }
        /// <summary>
        /// get Cross product 
        /// </summary>
        /// <param name="v">Vector3d</param>
        /// <returns>Vector3d</returns>
        public Vector3d Cross(Vector3d v)
        {
            return new Vector3d(Y * v.Z - Z * v.Y, Z * v.X - X * v.Z, X * v.Y - Y * v.X);
        }

        /// <summary>
        /// get Cross product 
        /// </summary>
        /// <param name="v1">Vector3d</param>
        /// <param name="v2">Vector3d</param>
        /// <returns>Vector3d</returns>
        public static Vector3d Cross(Vector3d v1, Vector3d v2)
        {
            return new Vector3d(v1.Y * v2.Z - v1.Z * v2.Y, v1.Z * v2.X - v1.X * v2.Z, v1.X * v2.Y - v1.Y * v2.X);
        }

        public Vector2d ToVector2d()
        {
            return new Vector2d(X, Y);
        }
        public static Vector3d UnitX()
        {
            return new Vector3d(1, 0, 0);
        }
        public static Vector3d UnitY()
        {
            return new Vector3d(0, 1, 0);
        }

        public static Vector3d UnitZ()
        {
            return new Vector3d(0, 0, 1);
        }

        public static Vector3d Zero()
        {
            return new Vector3d(0, 0, 0);
        }
        #endregion

        public override string ToString()
        {
            return string.Format("[Vector 3D: X={0}, Y={1}, Z={2}]", X, Y, Z);
        }
    }
}
