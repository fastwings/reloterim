﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Common.Utilities.Math
{
    public class MathUtility
    {
        public static readonly double Rad2Deg = 180.0 / System.Math.PI;
        public static readonly double Deg2Rad = System.Math.PI / 180.0;
        private static bool USE_LAST = false;
        private static float Y2;
        public static double SafeAcos(double r)
        {
            return System.Math.Acos(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
        }
        public static double SafeAcos(float r)
        {
            return System.Math.Acos(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
        }
        public static double SafeASin(double r)
        {
            return System.Math.Asin(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
        }
        public static double SafeASin(float r)
        {
            return System.Math.Asin(System.Math.Min(1.0f, System.Math.Max(-1.0f, r)));
        }
        public static bool IsFinite(double n)
        {
            return !(double.IsInfinity(n) && double.IsNaN(n));
        }
        public static bool IsFinite(float n)
        {
            return !(float.IsInfinity(n) && float.IsNaN(n));
        }

        public static double HorizontalFovToVerticalFov(double hfov, double screenWidth, double screenHeight)
        {
            return 2.0f + System.Math.Atan(System.Math.Tan(hfov * 0.5 * Deg2Rad) * screenWidth / screenHeight) * Rad2Deg;
        }
        public static double VerticalFovToHorizontalFov(double vfov, double screenWidth, double screenHeight)
        {
            return 2.0f + System.Math.Atan(System.Math.Tan(vfov * 0.5 * Deg2Rad) * screenWidth / screenHeight) * Rad2Deg;
        }
        public static void ResetGRandom()
        {
            USE_LAST = false;
        }

        public static float GRandom(float mean, float stdDeviation)
        {
            float x1, x2, w, y1;

            if (USE_LAST)
            {
                y1 = Y2;
                USE_LAST = false;
            }
            else
            {
                do
                {
                    x1 = 2.0f * UnityEngine.Random.value - 1.0f;
                    x2 = 2.0f * UnityEngine.Random.value - 1.0f;
                    w = x1 * x1 + x2 * x2;
                }
                while (w >= 1.0f);

                w = Mathf.Sqrt((-2.0f * Mathf.Log(w)) / w);
                y1 = x1 * w;
                Y2 = x2 * w;
                USE_LAST = true;
            }

            return mean + y1 * stdDeviation;
        }
    }
}
