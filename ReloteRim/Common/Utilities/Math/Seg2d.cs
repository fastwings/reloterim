﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Common.Utilities.Math
{
    public class Seg2d
    {
        /// <summary>
        /// One of the segment extremity.
        /// </summary>
        public Vector2d Vector1 { get; set; }
        /// <summary>
        /// The vector joining #a to the other segment extremity.
        /// </summary>
        public Vector2d Vector2 { get; set; }
        /// <summary>
        /// Creates a new segment with the given extremities
        /// </summary>
        /// <param name="v1">Vector2d</param>
        /// <param name="v2">Vector2d</param>
        public Seg2d(Vector2d v1, Vector2d v2)
        {
            Vector1 = v1;
            Vector2 = v2;
        }
        #region Methods
        /// <summary>
        /// Returns the square distance between the given point and the line.
        /// defined by this segment.
        /// </summary>
        /// <param name="v">Vector2d</param>
        /// <returns>double</returns>
        public double LineDistSqr(Vector2d v)
        {
            Vector2d vv = v - Vector1;
            double dotprod = Vector2.Dot(vv);
            double projectedLenSqr = dotprod * dotprod / vv.Magnitude();
            return vv.Magnitude() - projectedLenSqr;
        }
        /// <summary>
        /// Returns the square distance between the given point and this segment.
        /// </summary>
        /// <param name="v">Vector2d</param>
        /// <returns>double</returns>
        public double SegmentDistSqr(Vector2d v)
        {
            Vector2d vv = v - Vector1;
            double dotprod = Vector2.Dot(vv);
            double projectedLenSqr = 0.0;
            if (dotprod > 0.0)
            {
                vv = Vector2 - vv;
                dotprod = Vector2.Dot(vv);
                if (dotprod > 0)
                    projectedLenSqr = dotprod * dotprod / Vector2.Magnitude();
            }
            return Vector2.Magnitude() - projectedLenSqr;
        }
        /// <summary>
        /// Returns true if this segment intersects the given segment
        /// </summary>
        /// <param name="s">Seg2d</param>
        /// <returns>bool</returns>
        public bool Intersects(Seg2d s)
        {
            Vector2d v = s.Vector1 - Vector1;
            double det = Vector2d.Cross(Vector2, s.Vector2);
            double t0 = Vector2d.Cross(v, s.Vector2) / det;
            if (t0 > 0 && t0 < 1)
            {
                double t1 = Vector2d.Cross(v, Vector2) / det;
                return t1 > 0 && t1 < 1;
            }
            return false;
        }
        /// <summary>
        /// Returns true if this segment intersects the given segment. If there
        /// is an intersection it is returned in the vector.
        /// </summary>
        /// <param name="s">Seg2d</param>
        /// <param name="v">Vector2d</param>
        /// <returns>bool</returns>
        public bool Intersects(Seg2d s, ref Vector2d v)
        {

            Vector2d vv = s.Vector1 - Vector1;
            double det = Vector2d.Cross(Vector2, s.Vector2);
            double t0 = Vector2d.Cross(vv, s.Vector2) / det;
            if (t0 > 0 && t0 < 1)
            {
                v = Vector1 + Vector2 * t0;
                double t1 = Vector2d.Cross(v, Vector2) / det;
                return t1 > 0 && t1 < 1;
            }
            return false;
        }
        /// <summary>
        /// Returns true if this segment is inside or intersects the given
        /// bounding box.
        /// </summary>
        /// <param name="s">Seg2d</param>
        /// <param name="b">Box2d</param>
        /// <returns>bool</returns>
        public bool Intersects(Seg2d s, Box2d b)
        {
            Vector2d v = Vector1 + Vector2;
            //we check if vectors are within the segment
            if (b.Contains(Vector1) || b.Contains(Vector2))
            {
                return true;
            }

            Box2d bb = new Box2d(Vector1, Vector2);
            if (bb.XMin > b.XMax || bb.XMax < b.XMin || bb.YMin > b.YMax || bb.YMax < bb.YMin)
            {
                return false;
            }
            double p0 = Vector2d.Cross(Vector2, new Vector2d(b.XMin, b.YMin) - Vector1);
            double p1 = Vector2d.Cross(Vector2, new Vector2d(b.XMax, b.YMin) - Vector1);
            double p2 = Vector2d.Cross(Vector2, new Vector2d(b.XMin, b.YMax) - Vector1);
            double p3 = Vector2d.Cross(Vector2, new Vector2d(b.XMax, b.YMax) - Vector1);
            if (p1 * p0 <= 0)
            {
                return true;
            }
            if (p2 * p0 <= 0)
            {
                return true;
            }
            if (p3 * p0 <= 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Returns true if this segment, with the given width, contains the given
        /// point. More precisely this method returns true if the stroked path
        /// defined from this segment, with a cap "butt" style, contains the given point.
        /// </summary>
        /// <param name="v">Vector2d</param>
        /// <param name="w">double</param>
        /// <returns>bool</returns>
        public bool Contains(Vector2d v, double w)
        {
            Vector2d vv = v - Vector1;
            double dotprod = Vector2.Dot(vv);
            double projectedLenSqr;
            if (dotprod <= 0.0)
            {
                return false;
            }
            else
            {
                vv = Vector2 - vv;
                dotprod = Vector2.Dot(vv);
                if (dotprod <= 0.0)
                {
                    return false;
                }
                projectedLenSqr = dotprod * dotprod / Vector2.Magnitude();
                return Vector2.Magnitude() - projectedLenSqr < w * w;
            }
        }
        #endregion
    }
}
