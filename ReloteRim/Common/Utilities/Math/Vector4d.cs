﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Common.Utilities.Math
{
    public class Vector4d
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double W { get; set; }
        public Vector4 Vector
        {
            get
            {
                return new Vector4((float)X, (float)Y, (float)Z, (float)W);
            }
        }
        public Vector4d()
        {
            X = Y = Z = W = 0.0f;
        }
        public Vector4d(double n)
        {
            X = Y = Z = W = n;
        }
        public Vector4d(double x, double y)
        {
            X = x;
            Y = y;
            Z = W = 0.0f;
        }
        public Vector4d(Vector2d v, double z, double w)
        {
            X = v.X;
            Y = v.Y;
            Z = z;
            W = w;
        }
        public Vector4d(Vector2d v)
        {
            X = v.X;
            Y = v.Y;
            Z = W = 0.0f;
        }
        public Vector4d(Vector2 v, double z, double w)
        {
            X = (double)v.x;
            Y = (double)v.y;
            Z = z;
            W = w;
        }
        public Vector4d(Vector2 v)
        {
            X = (double)v.x;
            Y = (double)v.y;
            Z = W = 0.0f;
        }
        public Vector4d(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
            W = 0.0f;
        }

        public Vector4d(Vector3d v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
            W = 0.0f;
        }

        public Vector4d(Vector3 v)
        {
            X = (double)v.x;
            Y = (double)v.y;
            Z = (double)v.z;
            W = 0.0f;
        }
        public Vector4d(Vector3d v, double w)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
            W = w;
        }
        public Vector4d(Vector3 v, double w)
        {
            X = (double)v.x;
            Y = (double)v.y;
            Z = (double)v.z;
            W = w;
        }
        public Vector4d(double x, double y, double z, double w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }
        public Vector4d(Vector4d v)
        {
            X = v.X;
            Y = v.Y;
            Z = v.Z;
            W = v.W;
        }
        public Vector4d(Vector4 v)
        {
            X = (double)v.x;
            Y = (double)v.y;
            Z = (double)v.z;
            W = (double)v.w;
        }
        public Vector4d(double[] n)
        {
            X = n[0];
            Y = n[1];
            Z = n[2];
            W = n[3];
        }
        #region Operators
        public static Vector4d operator +(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, v1.W + v2.W);
        }
        public static Vector4d operator -(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, v1.W - v2.W);
        }
        public static Vector4d operator *(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z, v1.W * v2.W);
        }
        public static Vector4d operator *(Vector4d v1, double n)
        {
            return new Vector4d(v1.X * n, v1.Y * n, v1.Z * n, v1.W * n);
        }

        public static Vector4d operator *(double n, Vector4d v1)
        {
            return new Vector4d(v1.X * n, v1.Y * n, v1.Z * n, v1.W * n);
        }
        public static Vector4d operator /(Vector4d v1, Vector4d v2)
        {
            return new Vector4d(v1.X / v2.X, v1.Y / v2.Y, v1.Z / v2.Z, v1.W / v2.W);
        }
        public static Vector4d operator /(Vector4d v, double n)
        {
            return new Vector4d(v.X / n, v.Y / n, v.Z / n, v.W / n);
        }
        #endregion

        #region Methods
        /// <summary>
        /// will get Magnitude length of the vector 
        /// </summary>
        /// <returns>double</returns>
        public double SqrMagnitude()
        {
            return X * X + Y * Y + Z * Z + W * W;
        }
        /// <summary>
        /// will get Magnitude squared length  of the vector
        /// </summary>
        /// <returns>double</returns>
        public double Magnitude()
        {
            return System.Math.Sqrt(X * X + Y * Y + Z * Z + W * W);
        }

        public double Dot(Vector4d v)
        {
            return System.Convert.ToDouble(Vector4.Dot(Vector, v.Vector));
        }
        public double Dot(Vector3d v)
        {
            return X * v.X + Y * v.Y + Z * v.Z + W;
        }
        /// <summary>
        /// Makes this vector have a magnitude of 1
        /// </summary>
        public void Normalize()
        {
            double invLength = 1.0 / Magnitude();
            X *= invLength;
            Y *= invLength;
            Z *= invLength;
            W *= invLength;
        }
        /// <summary>
        /// Makes this vector have a magnitude of 1
        /// </summary>
        /// <returns>Vector2d</returns>
        public Vector4d Normalized()
        {
            double invLength = 1.0 / Magnitude();
            return new Vector4d(X * invLength, Y * invLength, Z * invLength, W * invLength);
        }

        public static Vector4d UnitX()
        {
            return new Vector4d(1, 0, 0, 0);
        }
        public static Vector4d UnitY()
        {
            return new Vector4d(0, 1, 0, 0);
        }
        public static Vector4d UnitZ()
        {
            return new Vector4d(0, 0, 1, 0);
        }
        public static Vector4d UnitW()
        {
            return new Vector4d(0, 0, 0, 1);
        }
        public static Vector4d Zero()
        {
            return new Vector4d(0, 0, 0, 0);
        }

        public Vector3d ToVector3d()
        {
            return new Vector3d(X, Y, Z);
        }

        public Vector4d ToVector4WZero()
        {
            return new Vector4d(X, Y, Z, 0);
        }
        #endregion
        public override string ToString()
        {
            return string.Format("[Vector 4D: X={0}, Y={1}, Z={2},W={3}]", X, Y, Z, W);
        }
    }
}
