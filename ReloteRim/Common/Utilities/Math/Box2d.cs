using System;
using UnityEngine;
namespace ReloteRim.Common.Utilities.Math
{
    public class Box2d
    {
        public double XMin
        {
            get;
            set;
        }

        public double XMax
        {
            get;
            set;
        }

        public double YMin
        {
            get;
            set;
        }

        public double YMax
        {
            get;
            set;
        }

        public Box2d()
        {
            this.XMin = double.PositiveInfinity;
            this.XMax = double.NegativeInfinity;
            this.YMin = double.PositiveInfinity;
            this.YMax = double.NegativeInfinity;
        }

        public Box2d(double xmin, double xmax, double ymin, double ymax)
        {
            this.XMin = XMin;
            this.XMax = XMax;
            this.YMin = ymin;
            this.YMax = ymin;
        }

        public Box2d(Vector2d v1, Vector2d v2)
        {
            XMin = System.Math.Min(v1.X, v2.X);
            XMax = System.Math.Max(v1.X, v2.X);
            YMin = System.Math.Min(v1.Y, v2.Y);
            YMax = System.Math.Max(v1.Y, v2.Y);
        }

        #region Methods

        /// <summary>
        /// will get the b
        /// </summary>
        /// <returns></returns>
        public Vector2d Center()
        {
            return new Vector2d((this.XMin + this.XMax) / 2.0f, (this.YMin + this.YMax) / 2.0f);
        }
        /// <summary>
        /// will get the width of the box
        /// </summary>
        /// <returns>double</returns>
        public double Width()
        {
            return this.XMax - this.XMin;
        }
        /// <summary>
        /// will get the height of the box
        /// </summary>
        /// <returns>double</returns>
        public double Height()
        {
            return this.YMax - this.YMin;
        }
        /// <summary>
        /// will get the area size
        /// </summary>
        /// <returns>double</returns>
        public double Area()
        {
            return (XMin - XMax) * (YMax - YMin);
        }
        /// <summary>
        /// Returns the bounding box containing this box and the given border.
        /// </summary>
        /// <param name="w">double</param>
        /// <returns>Box2d</returns>
        public Box2d Enlarge(double w)
        {
            return new Box2d(XMin - w, XMax + w, YMin - w, YMax + w);
        }
        /// <summary>
        /// Returns the bounding box containing this box and the given point.
        /// </summary>
        /// <param name="v">Vector2d</param>
        /// <returns>Box2d</returns>
        public Box2d Enlarge(Vector2d v)
        {
            return new Box2d(System.Math.Min(XMin, v.X), System.Math.Max(XMax, v.X), System.Math.Min(YMin, v.Y), System.Math.Max(YMax, v.Y));
        }
        /// <summary>
        /// Returns the bounding box containing this box and the given box.
        /// </summary>
        /// <param name="v">Box2d</param>
        /// <returns>Box2d</returns>
        public Box2d Enlarge(Box2d b)
        {
            return new Box2d(System.Math.Min(XMin, b.XMin), System.Math.Max(XMax, b.XMax), System.Math.Min(YMin, b.YMin), System.Math.Max(YMax, b.YMax));
        }
        /// <summary>
        /// Returns true if this bounding box contains the given point.
        /// </summary>
        /// <param name="v">vector</param>
        /// <returns>bool</returns>
        public bool Contains(Vector2d v)
        {
            return (v.X >= XMin && v.X <= XMax && v.Y >= YMin && v.Y <= YMax);
        }
        /// <summary>
        /// Returns true if this bounding box contains the given bounding box.
        /// </summary>
        /// <param name="b">Box2d</param>
        /// <returns>bool</returns>
        public bool Contains(Box2d b)
        {
            return (b.XMin >= XMin && b.XMax <= XMax && b.YMin >= YMin && b.YMax <= YMax);
        }
        /// <summary>
        /// return true if the bounding area of the box intersect with given box
        /// </summary>
        /// <param name="b">Box2d</param>
        /// <returns>bool</returns>
        public bool Intersects(Box2d b)
        {
            return (b.XMax >= XMin && b.XMin <= XMax && b.YMax >= YMin && b.YMin <= YMax);
        }
        public Vector2d NearestInnerPoint(Vector2d v)
        {
            Vector2d nearest = new Vector2d(v);
            //we calc the vector x position
            if (v.X < XMin)
            {
                nearest.X = XMin;
            }
            else if (v.X > XMax)
            {
                nearest.X = XMax;
            }
            //we calc the vector y position
            if (v.Y < YMin)
            {
                nearest.Y = YMin;
            }
            else if (v.Y > YMax)
            {
                nearest.Y = YMax;
            }
            return nearest;
        }
        #endregion
    }
}