﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
#pragma warning disable 660, 661

namespace ReloteRim.Common.Utilities.Math
{
    public class Matrix4d
    {

        private double[,] matrix = new double[4, 4];
        public double[,] Matrix
        {
            get
            {
                return matrix;
            }
            set
            {
                matrix = value;
            }
        }
        public Matrix4d()
        {
        }
        public Matrix4d(double p0, double p1, double p2, double p3,
                        double p4, double p5, double p6, double p7,
                        double p8, double p9, double p10, double p11,
                        double p12, double p13, double p14, double p15)
        {
            int row = 0;
            matrix[row, 0] = p0;
            matrix[row, 1] = p1;
            matrix[row, 2] = p2;
            matrix[row, 3] = p3;

            row++;
            matrix[row, 0] = p4;
            matrix[row, 1] = p5;
            matrix[row, 2] = p6;
            matrix[row, 3] = p7;

            row++;
            matrix[row, 0] = p8;
            matrix[row, 1] = p9;
            matrix[row, 2] = p10;
            matrix[row, 3] = p11;

            row++;
            matrix[row, 0] = p12;
            matrix[row, 1] = p13;
            matrix[row, 2] = p14;
            matrix[row, 3] = p15;
        }
        public Matrix4d(Matrix4x4 mat)
        {
            int row = 0;
            matrix[row, 0] = mat.m00;
            matrix[row, 1] = mat.m01;
            matrix[row, 2] = mat.m02;
            matrix[row, 3] = mat.m03;

            row++;
            matrix[row, 0] = mat.m10;
            matrix[row, 1] = mat.m11;
            matrix[row, 2] = mat.m12;
            matrix[row, 3] = mat.m13;

            row++;
            matrix[row, 0] = mat.m20;
            matrix[row, 1] = mat.m21;
            matrix[row, 2] = mat.m22;
            matrix[row, 3] = mat.m23;

            row++;
            matrix[row, 0] = mat.m30;
            matrix[row, 1] = mat.m31;
            matrix[row, 2] = mat.m32;
            matrix[row, 3] = mat.m33;
        }

        public Matrix4d(double[,] m)
        {
            System.Array.Copy(m, this.matrix, 16);
        }

        public Matrix4d(Matrix4d m)
        {
            System.Array.Copy(m.matrix, this.matrix, 16);
        }

        #region Operators

        public static Matrix4d operator +(Matrix4d m1, Matrix4d m2)
        {
            Matrix4d sum = new Matrix4d();
            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] + m2.Matrix[row, col];
                }
            }
            return sum;
        }

        public static Matrix4d operator -(Matrix4d m1, Matrix4d m2)
        {
            Matrix4d sum = new Matrix4d();
            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] - m2.Matrix[row, col];
                }
            }
            return sum;
        }


        public static Matrix4d operator *(Matrix4d m1, Matrix4d m2)
        {
            Matrix4d sum = new Matrix4d();
            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    sum.matrix[row, col] = m1.Matrix[row, 0] * m2.Matrix[0, col] +
                                            m1.Matrix[row, 1] * m2.Matrix[1, col] +
                                            m1.Matrix[row, 2] * m2.Matrix[2, col] +
                                            m1.Matrix[row, 3] * m2.Matrix[3, col];
                }
            }
            return sum;
        }

        public static Matrix4d operator *(Matrix4d m1, double n)
        {
            Matrix4d sum = new Matrix4d();
            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    sum.Matrix[row, col] = m1.Matrix[row, col] * n;
                }
            }
            return sum;
        }
        public static Vector3d operator *(Matrix4d m1, Vector3d v)
        {
            Vector3d vv = new Vector3d();
            double fInvW = 1.0 / (m1.Matrix[3, 0] * v.X + m1.Matrix[3, 1] * v.Y + m1.Matrix[3, 2] * v.Z + m1.Matrix[3, 3]);
            vv.X = m1.Matrix[0, 0] * v.X + m1.Matrix[0, 1] * v.Y + m1.Matrix[0, 2] * v.Z * fInvW;
            vv.Y = m1.Matrix[1, 0] * v.X + m1.Matrix[1, 1] * v.Y + m1.Matrix[1, 2] * v.Z * fInvW;
            vv.Z = m1.Matrix[2, 0] * v.X + m1.Matrix[2, 1] * v.Y + m1.Matrix[2, 2] * v.Z * fInvW;
            return vv;
        }

        public static Vector4d operator *(Matrix4d m1, Vector4d v)
        {
            Vector4d vv = new Vector4d();
            vv.X = m1.Matrix[0, 0] * v.X + m1.Matrix[0, 1] * v.Y + m1.Matrix[0, 2] * v.Z + m1.Matrix[0, 3] * v.W;
            vv.Y = m1.Matrix[1, 0] * v.X + m1.Matrix[1, 1] * v.Y + m1.Matrix[1, 2] * v.Z + m1.Matrix[1, 3] * v.W;
            vv.Z = m1.Matrix[2, 0] * v.X + m1.Matrix[2, 1] * v.Y + m1.Matrix[2, 2] * v.Z + m1.Matrix[2, 3] * v.W;
            vv.W = m1.Matrix[3, 0] * v.X + m1.Matrix[3, 1] * v.Y + m1.Matrix[3, 2] * v.Z + m1.Matrix[3, 3] * v.W;
            return vv;
        }

        public static bool operator ==(Matrix4d m1, Matrix4d m2)
        {

            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    if (m1.Matrix[row, col] != m2.Matrix[row, col]) return false;
                }
            }
            return true;
        }

        public static bool operator !=(Matrix4d m1, Matrix4d m2)
        {

            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    if (m1.Matrix[row, col] == m2.Matrix[row, col]) return false;
                }
            }
            return true;
        }
        #endregion

        #region Methods
        public Matrix4d Transpose()
        {
            Matrix4d transpose = new Matrix4d();
            for (int row = 0; row < 4; row++)
            {
                for (int col = 0; col < 4; col++)
                {
                    transpose.Matrix[row, col] = matrix[col, row];
                }
            }
            return transpose;
        }

        public Matrix4d Inverse()
        {
            return adjoint() * (1.0f / determinant());
        }

        public Vector4d GetColumn(int col)
        {
            return new Vector4d(matrix[0, col], matrix[1, col], matrix[2, col], matrix[3, col]);
        }

        public void SetColumn(int col, Vector4d v)
        {
            matrix[0, col] = v.X;
            matrix[1, col] = v.Y;
            matrix[2, col] = v.Z;
            matrix[3, col] = v.W;
        }

        public Vector4d GetRow(int row)
        {
            return new Vector4d(matrix[row, 0], matrix[row, 1], matrix[row, 2], matrix[row, 3]);
        }
        public void SetRow(int row, Vector4d v)
        {
            matrix[row, 0] = v.X;
            matrix[row, 1] = v.Y;
            matrix[row, 2] = v.Z;
            matrix[row, 3] = v.W;
        }

        public static Matrix4d Translate(Vector3d v)
        {
            return new Matrix4d(1, 0, 0, v.X,
                                0, 1, 0, v.Y,
                                0, 0, 1, v.Z,
                                0, 0, 0, 1);
        }

        public static Matrix4d Translate(Vector3 v)
        {
            return new Matrix4d(1, 0, 0, v.x,
                                0, 1, 0, v.y,
                                0, 0, 1, v.z,
                                0, 0, 0, 1);
        }

        public static Matrix4d Scale(Vector3d v)
        {
            return new Matrix4d(v.X, 0, 0, 0,
                                0, v.Y, 0, 0,
                                0, 0, v.Z, 0,
                                0, 0, 0, 1);
        }
        public static Matrix4d Scale(Vector3 v)
        {
            return new Matrix4d(v.x, 0, 0, 0,
                                0, v.y, 0, 0,
                                0, 0, v.z, 0,
                                0, 0, 0, 1);
        }

        public static Matrix4d RotateX(double angule)
        {
            double cos = System.Math.Cos(angule * Mathf.PI / 180.0);
            double sin = System.Math.Sin(angule * Mathf.PI / 180.0);
            return new Matrix4d(1, 0, 0, 0,
                                0, cos, -sin, 0,
                                0, sin, cos, 0,
                                0, 0, 0, 1);
        }

        public static Matrix4d RotateY(double angule)
        {
            double cos = System.Math.Cos(angule * Mathf.PI / 180.0);
            double sin = System.Math.Sin(angule * Mathf.PI / 180.0);
            return new Matrix4d(cos, 0, sin, 0,
                                0, 1, 0, 0,
                                -sin, 0, cos, 0,
                                0, 0, 0, 1);
        }

        public static Matrix4d RotateZ(double angule)
        {
            double cos = System.Math.Cos(angule * Mathf.PI / 180.0);
            double sin = System.Math.Sin(angule * Mathf.PI / 180.0);
            return new Matrix4d(cos, -sin, 0, 0,
                                sin, cos, 0, 0,
                                0, 0, 1, 0,
                                0, 0, 0, 1);
        }

        public static Matrix4d Rotate(Vector3 rotation)
        {
            Quaterniond x = new Quaterniond(new Vector3d(1, 0, 0), Convert.ToDouble(rotation.x * MathUtility.Deg2Rad));
            Quaterniond y = new Quaterniond(new Vector3d(0, 1, 0), Convert.ToDouble(rotation.y * MathUtility.Deg2Rad));
            Quaterniond z = new Quaterniond(new Vector3d(0, 0, 1), Convert.ToDouble(rotation.z * MathUtility.Deg2Rad));
            return (x * y * z).Matrix4d;
        }
        public static Matrix4d Rotate(Vector3d rotation)
        {
            Quaterniond x = new Quaterniond(new Vector3d(1, 0, 0), rotation.X * MathUtility.Deg2Rad);
            Quaterniond y = new Quaterniond(new Vector3d(0, 1, 0), rotation.Y * MathUtility.Deg2Rad);
            Quaterniond z = new Quaterniond(new Vector3d(0, 0, 1), rotation.Z * MathUtility.Deg2Rad);
            return (x * y * z).Matrix4d;
        }

        public static Matrix4d Perspective(double fovy, double aspect, double zNear, double zFar)
        {
            double f = 1.0 / System.Math.Tan((fovy * System.Math.PI / 180.0) / 2.0);
            return new Matrix4d(f / aspect, 0, 0, 0,
                                0, f, 0, 0,
                                0, 0, (zFar + zNear) / (zNear - zFar), (2.0 * zFar * zNear) / (zNear - zFar),
                                0, 0, -1, 0);
        }

        public static Matrix4d Ortho(double xRight, double xLeft, double yTop, double yBottom, double zNear, double zFar)
        {
            double x, y, z;
            x = (xRight + xLeft) / (xRight - xLeft);
            y = (yTop + yBottom) / (yTop - yBottom);
            z = (zFar + zNear) / (zFar - zNear);
            return new Matrix4d(2.0 / (xRight - xLeft), 0, 0, x,
                                0, 2.0 / (yTop - yBottom), 0, y,
                                0, 0, -2.0 / (zFar - zNear), z,
                                0, 0, 0, 1);
        }

        public static Matrix4d Identity()
        {
            return new Matrix4d(1, 0, 0, 0,
                                    0, 1, 0, 0,
                                    0, 0, 1, 0,
                                    0, 0, 0, 1);
        }
        public Matrix4x4 ToMatrix4x4()
        {
            Matrix4x4 mat = new Matrix4x4();

            mat.m00 = (float)matrix[0, 0]; mat.m01 = (float)matrix[0, 1]; mat.m02 = (float)matrix[0, 2]; mat.m03 = (float)matrix[0, 3];
            mat.m10 = (float)matrix[1, 0]; mat.m11 = (float)matrix[1, 1]; mat.m12 = (float)matrix[1, 2]; mat.m13 = (float)matrix[1, 3];
            mat.m20 = (float)matrix[2, 0]; mat.m21 = (float)matrix[2, 1]; mat.m22 = (float)matrix[2, 2]; mat.m23 = (float)matrix[2, 3];
            mat.m30 = (float)matrix[3, 0]; mat.m31 = (float)matrix[3, 1]; mat.m32 = (float)matrix[3, 2]; mat.m33 = (float)matrix[3, 3];

            return mat;
        }

        public Matrix3d ToMatrix3x3d()
        {
            Matrix3d mat = new Matrix3d();

            mat.Matrix[0, 0] = matrix[0, 0]; mat.Matrix[0, 1] = matrix[0, 1]; mat.Matrix[0, 2] = matrix[0, 2];
            mat.Matrix[1, 0] = matrix[1, 0]; mat.Matrix[1, 1] = matrix[1, 1]; mat.Matrix[1, 2] = matrix[1, 2];
            mat.Matrix[2, 0] = matrix[2, 0]; mat.Matrix[2, 1] = matrix[2, 1]; mat.Matrix[2, 2] = matrix[2, 2];

            return mat;
        }

        #endregion

        private Matrix4d adjoint()
        {
            return new Matrix4d(
                    MINOR(1, 2, 3, 1, 2, 3),
                    -MINOR(0, 2, 3, 1, 2, 3),
                    MINOR(0, 1, 3, 1, 2, 3),
                    -MINOR(0, 1, 2, 1, 2, 3),

                    -MINOR(1, 2, 3, 0, 2, 3),
                    MINOR(0, 2, 3, 0, 2, 3),
                    -MINOR(0, 1, 3, 0, 2, 3),
                    MINOR(0, 1, 2, 0, 2, 3),

                    MINOR(1, 2, 3, 0, 1, 3),
                    -MINOR(0, 2, 3, 0, 1, 3),
                    MINOR(0, 1, 3, 0, 1, 3),
                    -MINOR(0, 1, 2, 0, 1, 3),

                    -MINOR(1, 2, 3, 0, 1, 2),
                    MINOR(0, 2, 3, 0, 1, 2),
                    -MINOR(0, 1, 3, 0, 1, 2),
                    MINOR(0, 1, 2, 0, 1, 2));
        }
        private double determinant()
        {
            return (matrix[0, 0] * MINOR(1, 2, 3, 1, 2, 3) -
                    matrix[0, 1] * MINOR(1, 2, 3, 0, 2, 3) +
                    matrix[0, 2] * MINOR(1, 2, 3, 0, 1, 3) -
                    matrix[0, 3] * MINOR(1, 2, 3, 0, 1, 2));
        }
        private double MINOR(int r0, int r1, int r2, int c0, int c1, int c2)
        {
            return matrix[r0, c0] * (matrix[r1, c1] * matrix[r2, c2] - matrix[r2, c1] * matrix[r1, c2]) -
                   matrix[r0, c1] * (matrix[r1, c0] * matrix[r2, c2] - matrix[r2, c0] * matrix[r1, c2]) +
                   matrix[r0, c2] * (matrix[r1, c0] * matrix[r2, c1] - matrix[r2, c0] * matrix[r1, c1]);
        }
    }
}
