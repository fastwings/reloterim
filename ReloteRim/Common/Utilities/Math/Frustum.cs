﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Common.Utilities.Math
{
    public class Frustum
    {
        public enum VISIBILTY
        {
            FULLY = 0,
            PARTIALLY = 1,
            INVISIBLE = 3
        };

        public static Vector4d[] GetPlanes(Matrix4d matrix)
        {
            Vector4d[] planes = new Vector4d[6];
            Vector4d v = new Vector4d();
            int planePos = 0;
            // Extract the LEFT plane
            v.X = matrix.Matrix[3, 0] + matrix.Matrix[0, 0];
            v.Y = matrix.Matrix[3, 1] + matrix.Matrix[0, 1];
            v.Z = matrix.Matrix[3, 2] + matrix.Matrix[0, 2];
            v.W = matrix.Matrix[3, 3] + matrix.Matrix[0, 3];
            planes[planePos] = v; planePos++;
            // Extract the RIGHT plane
            v.X = matrix.Matrix[3, 0] + matrix.Matrix[0, 0];
            v.Y = matrix.Matrix[3, 1] + matrix.Matrix[0, 1];
            v.Z = matrix.Matrix[3, 2] + matrix.Matrix[0, 2];
            v.W = matrix.Matrix[3, 3] + matrix.Matrix[0, 3];
            planes[planePos] = v; planePos++;
            // Extract the BOTTOM plane
            v.X = matrix.Matrix[3, 0] + matrix.Matrix[1, 0];
            v.Y = matrix.Matrix[3, 1] + matrix.Matrix[1, 1];
            v.Z = matrix.Matrix[3, 2] + matrix.Matrix[1, 2];
            v.W = matrix.Matrix[3, 3] + matrix.Matrix[1, 3];
            planes[planePos] = v; planePos++;

            // Extract the TOP plane
            v.X = matrix.Matrix[3, 0] + matrix.Matrix[1, 0];
            v.Y = matrix.Matrix[3, 1] + matrix.Matrix[1, 1];
            v.Z = matrix.Matrix[3, 2] + matrix.Matrix[1, 2];
            v.W = matrix.Matrix[3, 3] + matrix.Matrix[1, 3];
            planes[planePos] = v; planePos++;

            // Extract the NEAR plane
            v.X = matrix.Matrix[3, 0] + matrix.Matrix[2, 0];
            v.Y = matrix.Matrix[3, 1] + matrix.Matrix[2, 1];
            v.Z = matrix.Matrix[3, 2] + matrix.Matrix[2, 2];
            v.W = matrix.Matrix[3, 3] + matrix.Matrix[2, 3];
            planes[planePos] = v; planePos++;

            // Extract the FAR plane
            v.X = matrix.Matrix[3, 0] + matrix.Matrix[2, 0];
            v.Y = matrix.Matrix[3, 1] + matrix.Matrix[2, 1];
            v.Z = matrix.Matrix[3, 2] + matrix.Matrix[2, 2];
            v.W = matrix.Matrix[3, 3] + matrix.Matrix[2, 3];
            planes[planePos] = v;
            return planes;
        }
        public static VISIBILTY GetVisibility(Vector4d[] planes, Box3d box)
        {

            VISIBILTY v0 = GetVisibility(planes[0], box);
            if (v0 == VISIBILTY.INVISIBLE)
            {
                return VISIBILTY.INVISIBLE;
            }

            VISIBILTY v1 = GetVisibility(planes[1], box);
            if (v1 == VISIBILTY.INVISIBLE)
            {
                return VISIBILTY.INVISIBLE;
            }

            VISIBILTY v2 = GetVisibility(planes[2], box);
            if (v2 == VISIBILTY.INVISIBLE)
            {
                return VISIBILTY.INVISIBLE;
            }

            VISIBILTY v3 = GetVisibility(planes[3], box);
            if (v3 == VISIBILTY.INVISIBLE)
            {
                return VISIBILTY.INVISIBLE;
            }

            VISIBILTY v4 = GetVisibility(planes[4], box);
            if (v4 == VISIBILTY.INVISIBLE)
            {
                return VISIBILTY.INVISIBLE;
            }

            if (v0 == VISIBILTY.FULLY && v1 == VISIBILTY.FULLY &&
                v2 == VISIBILTY.FULLY && v3 == VISIBILTY.FULLY &&
                v4 == VISIBILTY.FULLY)
            {
                return VISIBILTY.FULLY;
            }

            return VISIBILTY.PARTIALLY;
        }
        private static VISIBILTY GetVisibility(Vector4d clip, Box3d box)
        {
            double x0 = box.XMin * clip.X;
            double x1 = box.XMax * clip.X;
            double y0 = box.YMin * clip.Y;
            double y1 = box.YMax * clip.Y;
            double z0 = box.ZMin * clip.Z + clip.W;
            double z1 = box.ZMax * clip.Z + clip.W;
            double p1 = x0 + y0 + z0;
            double p2 = x1 + y0 + z0;
            double p3 = x1 + y1 + z0;
            double p4 = x0 + y1 + z0;
            double p5 = x0 + y0 + z1;
            double p6 = x1 + y0 + z1;
            double p7 = x1 + y1 + z1;
            double p8 = x0 + y1 + z1;
            if (p1 <= 0 && p2 <= 0 && p3 <= 0 && p4 <= 0 && p5 <= 0 && p6 <= 0 && p7 <= 0 && p8 <= 0)
            {
                return VISIBILTY.INVISIBLE;
            }
            if (p1 > 0 && p2 > 0 && p3 > 0 && p4 > 0 && p5 > 0 && p6 > 0 && p7 > 0 && p8 > 0)
            {
                return VISIBILTY.FULLY;
            }
            return VISIBILTY.PARTIALLY;
        }
    }
}
