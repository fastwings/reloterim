﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Common.Utilities.Math
{
    public class Box3d
    {
        public double XMin
        {
            get;
            set;
        }

        public double XMax
        {
            get;
            set;
        }

        public double YMin
        {
            get;
            set;
        }

        public double YMax
        {
            get;
            set;
        }

        public double ZMin
        {
            get;
            set;
        }

        public double ZMax
        {
            get;
            set;
        }

        public Box3d()
        {
            this.XMin = double.PositiveInfinity;
            this.XMax = double.NegativeInfinity;
            this.YMin = double.PositiveInfinity;
            this.YMax = double.NegativeInfinity;
            this.ZMin = double.PositiveInfinity;
            this.ZMax = double.NegativeInfinity;
        }

        public Box3d(double xmin, double xmax, double ymin, double ymax, double zmin, double zmax)
        {
            this.XMin = XMin;
            this.XMax = XMax;
            this.YMin = ymin;
            this.YMax = ymin;
            this.ZMin = zmin;
            this.ZMax = zmax;
        }

        public Box3d(Vector3d v1, Vector3d v2)
        {
            XMin = System.Math.Min(v1.X, v2.X);
            XMax = System.Math.Max(v1.X, v2.X);
            YMin = System.Math.Min(v1.Y, v2.Y);
            YMax = System.Math.Max(v1.Y, v2.Y);
            ZMin = System.Math.Min(v1.Z, v2.Z);
            ZMax = System.Math.Max(v1.Z, v2.Z);
        }


        #region Methods
        public Vector3d Center()
        {
            return new Vector3d((XMin + XMax) / 2.0, (YMin + YMax) / 2.0, (ZMin + ZMax) / 2.0);
        }
        /// <summary>
        /// Returns the bounding box containing this box and the given point.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Box3d Enlarge(Vector3d v)
        {
            return new Box3d(System.Math.Min(XMin, v.X), System.Math.Max(XMax, v.X),
                                System.Math.Min(YMin, v.Y), System.Math.Max(YMax, v.Y),
                                System.Math.Min(ZMin, v.Z), System.Math.Max(ZMax, v.Z));
        }
        /// <summary>
        /// Returns the bounding box containing this box and the given box.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Box3d Enlarge(Box3d b)
        {
            return new Box3d(System.Math.Min(XMin, b.XMin), System.Math.Max(XMax, b.XMax),
                                System.Math.Min(YMin, b.YMin), System.Math.Max(YMax, b.YMax),
                                System.Math.Min(ZMin, b.ZMin), System.Math.Max(ZMax, b.ZMax));
        }
        /// <summary>
        /// Returns true if this bounding box contains the given point.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool Contains(Vector3d p)
        {
            return (p.X >= XMin && p.X <= XMax && p.Y >= YMin && p.Y <= YMax && p.Z >= ZMin && p.Y <= ZMax);
        }
        #endregion
    }
}
