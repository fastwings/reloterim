﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Common.Utilities.Noise.Data
{
    public enum GradientType
    {
        D2,
        D3,
        D4
    }
    public class Gradients
    {
        public GradientType Type { get; private set; }
        public static Gradients Gradient2D
        {
            get
            {
                return new Gradients(GradientType.D2);
            }
        }
        public static Gradients Gradient3D
        {
            get
            {
                return new Gradients(GradientType.D3);
            }
        }
        public static Gradients Gradient4D
        {
            get
            {
                return new Gradients(GradientType.D4);
            }
        }
        public float[] Gradient { get; set; }
        public Gradients(GradientType type)
        {
            Type = type;
            initinitializeGradient(type);
        }
        public Gradients(float[] gradient, GradientType type)
        {
            Type = type;
            Gradient = gradient;
        }

        private void initinitializeGradient(GradientType type)
        {
            switch (type)
            {
                case GradientType.D2:
                    Gradient = new float[] 
                    { 
    	                0, 1, 
		                1, 1,
		                1, 0, 
		                1, -1, 
    	                0, -1, 
		                -1, -1, 
		                -1, 0, 
		                -1, 1,
	                };
                    break;
                case GradientType.D3:
                    Gradient = new float[] 
	                {
	                    1,1,0,
	                    -1,1,0,
	                    1,-1,0,
	                    -1,-1,0,
	                    1,0,1,
	                    -1,0,1,
	                    1,0,-1,
	                    -1,0,-1, 
	                    0,1,1,
	                    0,-1,1,
	                    0,1,-1,
	                    0,-1,-1,
	                    1,1,0,
	                    0,-1,1,
	                    -1,1,0,
	                    0,-1,-1,
	                };
                    break;
                case GradientType.D4:
                    Gradient = new float[]
	                {
		                0, -1, -1, -1,
		                0, -1, -1, 1,
		                0, -1, 1, -1,
		                0, -1, 1, 1,
		                0, 1, -1, -1,
		                0, 1, -1, 1,
		                0, 1, 1, -1,
		                0, 1, 1, 1,
		                -1, -1, 0, -1,
		                -1, 1, 0, -1,
		                1, -1, 0, -1,
		                1, 1, 0, -1,
		                -1, -1, 0, 1,
		                -1, 1, 0, 1,
		                1, -1, 0, 1,
		                1, 1, 0, 1,
		
		                -1, 0, -1, -1,
		                1, 0, -1, -1,
		                -1, 0, -1, 1,
		                1, 0, -1, 1,
		                -1, 0, 1, -1,
		                1, 0, 1, -1,
		                -1, 0, 1, 1,
		                1, 0, 1, 1,
		                0, -1, -1, 0,
		                0, -1, -1, 0,
		                0, -1, 1, 0,
		                0, -1, 1, 0,
		                0, 1, -1, 0,
		                0, 1, -1, 0,
		                0, 1, 1, 0,
		                0, 1, 1, 0,
	                };
                    break;
            }
        }
    }
}
