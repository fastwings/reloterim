﻿using UnityEngine;
using System.Collections.Generic;
using ReloteRim.Common.Utilities.Containers;

namespace ReloteRim.Common.Utilities.Tasks
{
    public class Schedular : MonoBehaviour
    {
        SetQueue<Task> m_taskQueue;
        Task.EqualityComparer m_comparer;

        public Schedular()
        {
            m_comparer = new Task.EqualityComparer();
            m_taskQueue = new SetQueue<Task>(m_comparer);
        }

        public void Add(Task task)
        {
            if (task.IsDone()) return;

            if (!m_taskQueue.Contains(task))
                m_taskQueue.AddLast(task);
        }

        public bool HasTask(Task task)
        {
            return m_taskQueue.Contains(task);
        }

        public void Run()
        {
            Task task = (m_taskQueue.Empty()) ? null : m_taskQueue.First();

            while (task != null)
            {

                task.Run();

                m_taskQueue.Remove(task);

                if (m_taskQueue.Empty())
                    task = null;
                else
                    task = m_taskQueue.First();

            }

        }
    }
}
