﻿using UnityEngine;
using System.Collections.Generic;

namespace ReloteRim.Common.Utilities.Tasks
{
    public abstract class Task
    {

        public class EqualityComparer : IEqualityComparer<Task>
        {
            public bool Equals(Task t1, Task t2)
            {
                return Object.ReferenceEquals(t1, t2);
            }

            public int GetHashCode(Task t)
            {
                return t.GetHashCode();
            }
        }

        bool m_done = false;

        public Task()
        {

        }

        public abstract void Run();

        public bool IsDone()
        {
            return m_done;
        }

        public void SetDone(bool b)
        {
            m_done = b;
        }

    }
}
