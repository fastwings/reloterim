﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Common.Utilities.Debugger.Actors
{
    [RequireComponent(typeof(Camera))]
    public class DrawCameraFrustum : MonoBehaviour
    {
        public Shader debugShader = Shader.Find("ReloteRim/Planets/Core/Math/Debug/GridDraw");
        public Color drawLineColor = new Color(1, 1, 1, 1);
        Material lineMaterial;
        Vector4[] volume = 
        {
            //near
            new Vector4(-1,-1,-1,-1), new Vector4( 1, -1, -1, 1), new Vector4( 1,  1, -1, 1),  new Vector4(-1,  1, -1, 1),
            // far
            new Vector4(-1, -1, 1, 1),	new Vector4( 1, -1, 1, 1),	new Vector4( 1,  1, 1, 1),  new Vector4(-1,  1, 1, 1)
        };

        int[,] indices = 
	    {
		    {0,1}, {1,2}, {2,3}, {3,0}, 
		    {4,5}, {5,6}, {6,7}, {7,4},
		    {0,4}, {1,5}, {2,6}, {3,7}
	    };

        // Use this for initialization
        void Start()
        {
            CreateLineMaterial();
        }
        void OnPostRender()
        {
            Matrix4x4 MVP = Camera.current.projectionMatrix * Camera.current.worldToCameraMatrix;
            Matrix4x4 invMVP = Matrix4x4.Inverse(MVP);
            Vector4[] frustumCorners = new Vector4[8];
            for (int i = 0; i < 8; i++)
            {
                frustumCorners[i] = invMVP * volume[i];
                frustumCorners[i].x /= frustumCorners[i].w;
                frustumCorners[i].y /= frustumCorners[i].w;
                frustumCorners[i].z /= frustumCorners[i].w;
                frustumCorners[i].w = 1.0f;
            }
            //now we update the GL 
            GL.PushMatrix();
            GL.LoadIdentity();
            GL.MultMatrix(Camera.current.worldToCameraMatrix);
            GL.LoadProjectionMatrix(Camera.current.projectionMatrix);
            //now we setup the color to see the grid
            lineMaterial.SetPass(0);
            GL.Begin(GL.LINES);
            GL.Color(drawLineColor);
            //we draw the lines show the grids
            for (int i = 0; i < 12; i++)
            {
                Vector4 p0 = frustumCorners[indices[i, 0]];
                Vector4 p1 = frustumCorners[indices[i, 1]];
                GL.Vertex3(p0.x, p0.y, p0.z);
                GL.Vertex3(p1.x, p1.y, p1.z);
            }
            //we draw and flash out 
            GL.End();
            GL.PopMatrix();
        }
        void CreateLineMaterial()
        {
            if (!lineMaterial)
            {
                lineMaterial = new Material(debugShader);
                lineMaterial.hideFlags = HideFlags.HideAndDontSave;
                lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
            }
        }
    }
}
