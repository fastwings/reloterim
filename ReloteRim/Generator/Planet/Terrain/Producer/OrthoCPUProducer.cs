﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Buffer;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Exceptions;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Terrain.Producer
{
    public class OrthoCPUProducer : TileProducer
    {
        /// <summary>
        ///  The name of the file containing the tiles to load.
        /// </summary>
        [SerializeField]
        string m_fileName = "/ReloteRim/Textures/Terrain/Final/Color";
        /// <summary>
        /// The number of components per pixel in the tiles to load.
        /// </summary>
        public int Channel { get; set; }
        /// <summary>
        /// The size in pixels of the border around each tile. A tile contains
        /// (tileSize+4)*(tileSize+4)*channels samples.
        /// </summary>
        private int m_tileSize;
        /// <summary>
        /// The size in pixels of the border around each tile. A tile contains
        /// (tileSize+4)*(tileSize+4)*channels samples.
        /// </summary>
        public int Border { get; set; }
        /// <summary>
        /// The maximum level of the stored tiles on disk (inclusive).
        /// </summary>
        private int m_maxLevel;
        /// <summary>
        /// The offsets of each tile on disk, relatively to offset, for each tile id
        /// </summary>
        private long[] m_offsets;

        protected override void Start()
        {
            base.Start();
            CPUTileStorage storage = GetCache().GetStorage(0) as CPUTileStorage;
            if (storage == null)
            {
                throw new InvalidStorageException("Storage Must be CPUTileStorage");
            }
            if (storage.DataType != CPUTileStorage.DATA_TYPE.BYTE)
            {
                throw new InvalidStorageException("Storage date type must be byte");
            }

            byte[] data = new byte[7 * 4];
            using (Stream stream = new FileStream(MediaTypeNames.Application.dataPath + m_fileName, FileMode.Open))
            {
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(data, 0, data.Length);
            }
            m_maxLevel = System.BitConverter.ToInt32(data, 0);
            m_tileSize = System.BitConverter.ToInt32(data, 4);
            Channel = System.BitConverter.ToInt32(data, 8);
            Border = System.BitConverter.ToInt32(data, 12);

            if (storage.Channels != Channel)
            {
                throw new InvalidStorageException("Storage channels must be " + Channel);
            }
            int ntiles = ((1 << (m_maxLevel * 2 + 2)) - 1) / 3;
            m_offsets = new long[ntiles * 2];

            data = new byte[ntiles * 2 * 8];

            using (Stream stream = new FileStream(MediaTypeNames.Application.dataPath + m_fileName, FileMode.Open))
            {
                stream.Seek(7 * 4, SeekOrigin.Begin);
                stream.Read(data, 0, data.Length);
            }

            for (int i = 0; i < ntiles * 2; i++)
            {
                m_offsets[i] = System.BitConverter.ToInt64(data, 8 * i);
            }
        }

        public override bool HasTile(int level, int tx, int ty)
        {
            return level <= m_maxLevel;
        }

        int GetTileId(int level, int tx, int ty)
        {
            return tx + ty * (1 << level) + ((1 << (2 * level) - 1) / 3);
        }

        public override void onCreateTile(int level, int tx, int ty, List<TileStorage.Slot> slots)
        {
            CPUSlot<byte> cpuSlot = slots[0] as CPUSlot<byte>;
            cpuSlot.Clear();

            int tileId = GetTileId(level, tx, ty);
            long fsize = m_offsets[2 * tileId + 1] - m_offsets[2 * tileId];
            if (fsize < (m_tileSize + 2 * Border) * (m_tileSize + 2 * Border) * Channel)
            {
                throw new InvalidParameterException("File size of tile is larger then actual tile size");
            }
            using (Stream stream = new FileStream(MediaTypeNames.Application.dataPath + m_fileName, FileMode.Open))
            {
                stream.Seek(m_offsets[2 * tileId], SeekOrigin.Begin);
                stream.Read(cpuSlot.Data, 0, cpuSlot.Data.Length);
            }
            base.onCreateTile(level, tx, ty, slots);
        }
    }
}