﻿using System.Collections.Generic;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Buffer;
using ReloteRim.Generator.Planet.Core.Storage.Comparers;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Exceptions;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Terrain.Producer
{
    public class PlantProducer : TileProducer
    {
        [SerializeField]
        private ComputeShader m_selectTrees;
        [SerializeField]
        private GameObject m_elevationProducerGO;
        private TileProducer m_elevationProducer;
        [SerializeField]
        private GameObject m_lccProducerGO;
        private TileProducer m_lccProducer;
        [SerializeField]
        private GameObject m_normalProducerGO;
        private TileProducer m_normalProducer;

        private PlantNode node;
        private Dictionary<TileID, Tile> m_drawableTiles;

        public Dictionary<TileID, Tile> DrawableTiles
        {
            get
            {
                return m_drawableTiles;
            }
        }

        protected override void Start()
        {
            base.Start();
            m_drawableTiles = new Dictionary<TileID, Tile>(new EqualityComparerTID());
            node = manager.PlantNode;
            m_elevationProducer = m_elevationProducerGO.GetComponent<TileProducer>();
            m_lccProducer = m_lccProducerGO.GetComponent<TileProducer>();

            CacheBufferStoage s0 = GetCache().GetStorage(0) as CacheBufferStoage;
            CacheBufferStoage s1 = GetCache().GetStorage(1) as CacheBufferStoage;

            if (s0 == null || s1 == null)
            {
                throw new InvalidStorageException("S0 AND S1 Must be CacheBufferStoage");
            }
            if (s0.BufferType != ComputeBufferType.Default || s1.BufferType != ComputeBufferType.Default)
            {
                throw new InvalidStorageException("S0 AND S1 Must be ComputeBufferType Set on Default");
            }
        }

        public override void PutTile(Tile tile)
        {
            if (tile != null)
            {
                TileID id = tile.TileId;
                if (m_drawableTiles.ContainsKey(id))
                {
                    m_drawableTiles.Remove(id);
                }
            }
            base.PutTile(tile);
        }

        public override Tile GetTile(int level, int tx, int ty)
        {
            Tile t = base.GetTile(level, tx, ty);
            if (t != null)
            {
                TileID id = t.TileId;
                if (!m_drawableTiles.ContainsKey(id))
                {
                    m_drawableTiles.Add(id, t);
                }

            }
            return t;
        }

        public override void onCreateTile(int level, int tx, int ty, List<TileStorage.Slot> slots)
        {
            CacheBufferSlot bufferSlot0 = slots[0] as CacheBufferSlot;
            CacheBufferSlot bufferSlot1 = slots[1] as CacheBufferSlot;
            Tile elevationTile = m_elevationProducer.FindTile(level, tx, ty, false, true);
            GPUSlot elevationTileSlot = null;
            if (elevationTile != null)
            {
                elevationTileSlot = elevationTile.Slots[0] as GPUSlot;
            }
            else
            {
                throw new MissingTileException("Find elevation Slot tile failed");
            }
            Tile lccTile = m_lccProducer.FindTile(level, tx, ty, false, true);
            GPUSlot lccSlot = null;

            if (lccTile != null)
            {
                lccSlot = lccTile.Slots[0] as GPUSlot;
            }
            else
            {
                throw new MissingTileException("Find LCC Slot tile failed");
            }

            Tile normalTile = m_normalProducer.FindTile(level, tx, ty, false, true);
            GPUSlot noramlSlot = null;

            if (normalTile != null)
            {
                noramlSlot = normalTile.Slots[0] as GPUSlot;
            }
            else
            {
                throw new MissingTileException("Find Normal Slot tile failed");
            }
            double rootQuadSize = TerrainNode.Root.Length;
            double ox = rootQuadSize * ((double)tx / (1 << level) - 0.5);
            double oy = rootQuadSize * ((double)ty / (1 << level) - 0.5);
            double l = rootQuadSize / (1 << level);

            Matrix4d l2d = TerrainNode.Deformed.LocalToDeformedDifferential(new Vector3d(ox + l / 2.0, oy + l / 2.0, 0.0));
            Matrix4d d2t = TerrainNode.Deformed.DeformedToTangentFrame(l2d * Vector3d.Zero());
            Matrix4d t2l = l2d.Inverse() * d2t.Inverse();
            Vector4d tileDeform = new Vector4d(t2l.Matrix[0, 0], t2l.Matrix[0, 1], t2l.Matrix[1, 0], t2l.Matrix[1, 1]);

            Vector4d tileOffset = new Vector4d(ox, oy, l, 0);

            int patternId = (int)(881.0f * Mathf.Abs(Mathf.Cos((float)(ox * oy)))) % node.PattenCount; //TODO improve this

            ComputeBuffer pattern = node.GetPatten(patternId);
            ComputeBuffer buffer0 = bufferSlot0.Buffer;
            ComputeBuffer buffer1 = bufferSlot1.Buffer;

            m_selectTrees.SetTexture(0, "_Elevation", elevationTileSlot.Texture));
            m_selectTrees.SetInt("_Elevation_Size", m_elevationProducer.GetTileSizeMinBorder(0));
            m_selectTrees.SetInt("_Elevation_Border", m_elevationProducer.GetBorder());

            m_selectTrees.SetTexture(0, "_Lcc", lccSlot.Texture);
            m_selectTrees.SetInt("_Lcc_Size", m_lccProducer.GetTileSizeMinBorder(0));
            m_selectTrees.SetInt("_Lcc_Border", m_lccProducer.GetBorder());

            m_selectTrees.SetTexture(0, "_Normal", noramlSlot.Texture);
            m_selectTrees.SetInt("_Normal_Size", m_normalProducer.GetTileSizeMinBorder(0));
            m_selectTrees.SetInt("_Normal_Border", m_normalProducer.GetBorder());

            m_selectTrees.SetVector("_TileDeform", tileDeform.Vector);
            m_selectTrees.SetVector("_TileOffset", tileOffset.Vector);
            m_selectTrees.SetInt("_PatternCount", pattern.count);
            m_selectTrees.SetBuffer(0, "_Pattern", pattern);
            m_selectTrees.SetBuffer(0, "_Buffer0", buffer0);
            m_selectTrees.SetBuffer(0, "_Buffer1", buffer1);

            m_selectTrees.Dispatch(0, buffer0.count / 8, 1, 1);

            base.onCreateTile(level, tx, ty, slots);
        }
    }
}
