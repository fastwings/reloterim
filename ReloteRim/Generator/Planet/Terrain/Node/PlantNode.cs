﻿using System;
using System.Collections.Generic;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Node;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Core.Utilities;
using ReloteRim.Generator.Planet.Producer;
using ReloteRim.Generator.Planet.Terrain.Producer;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ReloteRim.Generator.Planet.Terrain.Node
{
    public class PlantNode : NodeCore
    {
        public enum DISTRIBUTION
        {
            POISSON,
            RANDOM
        }
        /// <summary>
        /// % of plane covered with disks when generated with algorithm below
        /// </summary>
        static readonly float POISSON_COVERAGE = 0.6826f;
        [SerializeField]
        private Material m_renderer;
        [SerializeField]
        private DISTRIBUTION m_distribution = DISTRIBUTION.POISSON;
        /// <summary>
        /// The density of plants per quad at maxLevel
        /// </summary>
        [SerializeField]
        private int m_density = 8496;
        /// <summary>
        /// Number of unique patterns to create
        /// </summary>
        [SerializeField]
        private int m_patternCount = 6;
        /// <summary>
        /// The distance the the plants will stop being drawn at
        /// </summary>
        [SerializeField]
        private float m_maxDistance = 5000.0f;
        /// <summary>
        /// The quad level the planst are drawn at
        /// </summary>
        [SerializeField]
        private int m_maxLevel = 7;

        private List<ComputeBuffer> m_pattens;
        private PlantProducer[] producers;
        private PlanetGPUUniforms m_uniforms;

        private Vector3d[] m_localCameraPos;
        private Vector3d[] m_tangentCameraPos;
        private Vector3d[] m_tangentSunDir;

        private Matrix4d[] m_localToTangentFrame;
        private Matrix4d[] m_tangentFrameToWorld;
        private Matrix4d[] m_tangentFrameToScreen;
        private Matrix4d[] m_cameraToTangentFrame;

        public int MaxLevel
        {
            get { return m_maxLevel; }
        }
        public int PattenCount
        {
            get { return m_patternCount; }
        }

        public float MaxDistance
        {
            get { return m_maxDistance; }
        }

        public ComputeBuffer GetPattenBuffer(int i)
        {
            return m_pattens[Mathf.Clamp(i, 0, m_pattens.Count - 1)];
        }

        protected override void Start()
        {
            base.Start();
            manager.SkyNode.InitUniforms(m_renderer);

            //Density must be divisible by 8 as a compute shader is used to 
            //generate vaild points (Forests/SelectTree.compute) 
            //and 8 is the number of threads in a thread group used
            if (m_density % 8 != 0)
            {
                while (m_density % 8 != 0)
                    m_density--;
                if (m_density < 8) m_density = 8;
                Debug.Log("ReloteRim::Generator::Planet::Producer::PlantNode - Plant density must be divisible by 8. Changing to " + m_density);
            }
            Random.seed = 0;
            int minVertices = 2 * m_density;
            int maxVertices = 0;
            m_uniforms = new PlanetGPUUniforms();
            m_pattens = new List<ComputeBuffer>();
            //Get all the plant producers that are children of this game object
            producers = GetComponentsInChildren<PlantProducer>();

            //If using 3D tree get the component (optional)
            //m_trees3D = GetComponent<Trees3D>();

            int size = producers.Length;
            m_localCameraPos = new Vector3d[size];
            m_tangentCameraPos = new Vector3d[size];
            m_tangentSunDir = new Vector3d[size];

            m_localToTangentFrame = new Matrix4d[size];
            m_tangentFrameToWorld = new Matrix4d[size];
            m_tangentFrameToScreen = new Matrix4d[size];
            m_cameraToTangentFrame = new Matrix4d[size];

            //Create the patterns
            for (int i = 0; i < m_patternCount; i++)
            {
                List<Vector3d> pattern = new List<Vector3d>();
                float radius = 1.0f / Mathf.Sqrt(Convert.ToSingle(m_density) * Mathf.PI / POISSON_COVERAGE);
                if (m_distribution == DISTRIBUTION.POISSON)
                    GeneratePoissonPattern(pattern, radius);
                else
                    GenerateRandomPattern(pattern, m_density);
                AddPattern(pattern);
                minVertices = Mathf.Min(minVertices, pattern.Count);
                maxVertices = Mathf.Max(maxVertices, pattern.Count);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            foreach (ComputeBuffer buffer in m_pattens)
            {
                buffer.Release();
            }
        }
        /// <summary>
        /// Update the matrices/vectors needed to draw the plants produced on their associated terrain nodes
        /// </summary>
        public void UpdateNode()
        {
            int size = producers.Length;
            for (int i = 0; i < size; i++)
            {
                TerrainNode node = producers[i].TerrainNode;

                m_localCameraPos[i] = node.LocalCamearaPostion;
                Vector3d worldCamera = GetView().WorldCameraPosition;

                Matrix4d A = node.Deformed.LocalToDeformedDifferential(m_localCameraPos[i]);
                Matrix4d B = node.Deformed.DeformedToTangentFrame(worldCamera);
                Matrix4d ltow = node.LocalToWorld;
                Matrix4d ctos = GetView().CameraToScreenMatrix;
                Matrix4d wtoc = GetView().WorldToCameraMatrix;
                Matrix4d ctow = GetView().CameraToWorldMatrix;

                m_localToTangentFrame[i] = B * ltow * A;
                m_tangentFrameToWorld[i] = B.Inverse();
                m_tangentFrameToScreen[i] = ctos * wtoc * m_tangentFrameToWorld[i];
                m_cameraToTangentFrame[i] = B * ctow;
                m_tangentCameraPos[i] = m_cameraToTangentFrame[i] * Vector3d.Zero();
                m_tangentSunDir[i] = ((B * A) * (new Vector4d(manager.SunNode.Direction, 0.0).ToVector3d()));
            }
        }
        /// <summary>
        /// 
        ///
        /// Used if the node has data that nees to be drawn by a camera in the OnPostRender function
        /// See the PostRender.cs script for more info
        ///
        /// </summary>
        public override void PostRender()
        {
            //Bind all uniforms needed
            SetUniforms(m_renderer);
            manager.SetUniforms(m_renderer);
            manager.SunNode.SetUniforms(m_renderer);
            manager.SkyNode.SetUniforms(m_renderer);

            //If a trees3D component attached bind its data to shader

            int size = producers.Length;
            for (int i = 0; i < size; i++)
            {
                m_renderer.SetMatrix(m_uniforms.LocalToTangentFrame, m_localToTangentFrame[i].ToMatrix4x4());
                m_renderer.SetMatrix(m_uniforms.TangentFrameToWorld, m_tangentFrameToWorld[i].ToMatrix4x4());
                m_renderer.SetMatrix(m_uniforms.TangentFrameToScreen, m_tangentFrameToScreen[i].ToMatrix4x4());
                m_renderer.SetVector(m_uniforms.TangentSunDir, m_tangentSunDir[i].Vector);

                Vector4d[] clipPlanes = Frustum.GetPlanes(m_tangentFrameToScreen[i]);
                Matrix4d clips = new Matrix4d();

                clips.SetRow(0, clipPlanes[0] / clipPlanes[0].ToVector3d().SqrMagnitude());
                clips.SetRow(1, clipPlanes[1] / clipPlanes[1].ToVector3d().SqrMagnitude());
                clips.SetRow(2, clipPlanes[2] / clipPlanes[2].ToVector3d().SqrMagnitude());
                clips.SetRow(3, clipPlanes[3] / clipPlanes[3].ToVector3d().SqrMagnitude());

                m_renderer.SetMatrix(m_uniforms.Clip, clips.ToMatrix4x4());
                Vector2d cgDir = (m_cameraToTangentFrame[i] *
                                  (new Vector4d(0, 0, 1, 0))).ToVector3d().ToVector2d().Normalized(1000.0);
                m_renderer.SetVector(m_uniforms.FocusPoint, new Vector3(Convert.ToSingle(cgDir.X), Convert.ToSingle(cgDir.Y), Convert.ToSingle(m_tangentCameraPos[i].Z)));

                double d = (1.0 - GetView().CameraToScreenMatrix.Matrix[2, 2]) / 2.0;
                m_renderer.SetVector(m_uniforms.CameraRefPos, new Vector3(Convert.ToSingle(m_localCameraPos[i].X), Convert.ToSingle(m_localCameraPos[i].Y), Convert.ToSingle(d)));
                //For each terrain quad in the terrain node draw the plants
                DrawQuad(producers[i].TerrainNode.Root, i);
            }
        }

        private void DrawQuad(TerrainQuaternion quad, int idx)
        {
            //If this quad is visible and its level match the level the plants are draw at
            if (quad.IsVisible() && quad.GetSize() == m_maxLevel)
            {
                Dictionary<TileID, Tile> drawable = producers[idx].DrawableTiles;
                TileID id = Tile.GetTileId(quad.Level, quad.TX, quad.TY);
                if (drawable.ContainsKey(id))
                {
                    Tile t = drawable[id];
                    CacheBufferSlot bufferSlot0 = t.GetSlot(0) as CacheBufferSlot;
                    CacheBufferSlot bufferSlot1 = t.GetSlot(1) as CacheBufferSlot;
                    m_renderer.SetPass(0); //must set pass before each call to DrawProcedural
                    m_renderer.SetBuffer("_Plants_Positions", bufferSlot0.Buffer);
                    m_renderer.SetBuffer("_Plants_Params", bufferSlot1.Buffer);
                    //Draw plants
                    Graphics.DrawProcedural(MeshTopology.Points, bufferSlot0.Buffer.count);
                }
            }
            else
            {
                //draws quad in a order based on distance to camera
                int[] order = new int[4];
                double ox = m_localCameraPos[idx].X;
                double oy = m_localCameraPos[idx].Y;
                double cx = quad.OX + quad.Length / 2.0;
                double cy = quad.OY + quad.Length / 2.0;
                if (oy < cy)
                {
                    if (ox < cx)
                    {
                        order[0] = 0;
                        order[1] = 1;
                        order[2] = 2;
                        order[3] = 3;
                    }
                    else
                    {
                        order[0] = 1;
                        order[1] = 0;
                        order[2] = 3;
                        order[3] = 2;
                    }
                }
                else
                {
                    if (ox < cx)
                    {
                        order[0] = 2;
                        order[1] = 0;
                        order[2] = 3;
                        order[3] = 1;
                    }
                    else
                    {
                        order[0] = 3;
                        order[1] = 1;
                        order[2] = 2;
                        order[3] = 0;
                    }
                }
                if (!quad.IsLeaf() && quad.Level < m_maxLevel)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        DrawQuad(quad.GetChild(order[i]), idx);
                    }
                }

            }
        }

        public ComputeBuffer GetPatten(int i)
        {
            return m_pattens[Mathf.Clamp(i, 0, m_pattens.Count - 1)];
        }
        private void AddPattern(List<Vector3d> pattern)
        {
            int n = pattern.Count;
            Vector3[] verts = new Vector3[n];
            for (int i = 0; i < n; i++)
            {
                verts[i] = pattern[i].Vector;
            }
            ComputeBuffer buf = new ComputeBuffer(n, sizeof(float) * 3);
            buf.SetData(verts);
            m_pattens.Add(buf);
        }

        private void GenerateRandomPattern(List<Vector3d> pattern, int n)
        {
            for (int i = 0; i < n; i++)
            {
                float x = Random.value;
                float y = Random.value;
                pattern.Add(new Vector3d(x, y, Random.value));
            }
        }

        private void GeneratePoissonPattern(List<Vector3d> pattern, float radius)
        {
            RangeList ranges = new RangeList();
            PlantsGrid grid = new PlantsGrid(4.0f * radius, 64);
            List<Vector2> candidates = new List<Vector2>();
            Vector2 p = new Vector2(0.5f, 0.5f);
            candidates.Add(p);
            pattern.Add(new Vector3d(p.x, p.y, Random.value));
            grid.AddParticle(p);
            while (candidates.Count != 0)
            {
                // selects a candidate at random
                int c = Random.Range(0, int.MaxValue) % candidates.Count;
                p = candidates[c];
                // removes this candidate from the list
                candidates[c] = candidates[candidates.Count - 1];
                candidates.RemoveAt(candidates.Count - 1);
                ranges.Reset(0.0f, 2.0f * Mathf.PI);
                FindNeighborRanges(p, grid, ranges, radius);
            }
        }

        private void FindNeighborRanges(Vector2 v, PlantsGrid grid, RangeList ranges, float radius)
        {
            Vector2d cell = grid.GetCell(v);
            float rangeSqrD = 16.0f * radius * radius;
            int n = grid.GetCellSize(cell);
            Vector2[] neighbors = grid.GetCellContent(cell);
            for (int i = 0; i < n; i++)
            {
                Vector2 ns = neighbors[n];
                if (ns == v)
                {
                    continue;
                }
                Vector2 vv = ns - v;
                float sqrD = vv.sqrMagnitude;

                if (sqrD < rangeSqrD)
                {
                    float dist = Mathf.Sqrt(sqrD);
                    float angle = Mathf.Atan2(v.y, v.x);
                    float theta = Convert.ToSingle(MathUtility.SafeAcos(0.25f * dist / radius));
                    ranges.Subtract(angle - theta, angle + theta);
                }
            }
        }
        public void SetUniforms(Material mat)
        {
            //Sets uniforms that this or other gameobjects may need
            if (mat == null) return;

            mat.SetFloat(m_uniforms.MaxDist, m_maxDistance);
            mat.SetFloat(m_uniforms.PlantsDensity, 1.0f);
        }
    }
}
