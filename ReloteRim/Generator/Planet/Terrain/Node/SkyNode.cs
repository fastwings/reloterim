﻿using ReloteRim.Common.Config;
using ReloteRim.Common.Utilities.Misc;
using ReloteRim.Generator.Planet.Core.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Terrain.Node
{
    /// <summary>
    /// Loads the tables required for the atmospheric scattering and sets any uniforms for shaders
    /// that need them. If you create new tables using the PreprocessAtmo.cs script and changed some of  
    /// the settings (like the tables dimensions) you need to make sure the settings match here.
    /// You can adjust some of these settings (mieG, betaR) to change the look of the scattering but
    /// as precomputed tables are used there is a limit to how much the scattering will change.
    /// For large changes you will need to create new table with the settings you want.
    /// NOTE - all scenes must contain a skyNode
    /// </summary>
    public class SkyNode : NodeCore
    {
        //The radius of the planet (Rg), radius of the atmosphere (Rt)
        const float Rg = 6360000.0f;
        const float Rt = 6420000.0f;
        const float RL = 6421000.0f;

        //Dimensions of the tables
        const int TRANSMITTANCE_W = 256;
        const int TRANSMITTANCE_H = 64;
        const int SKY_W = 64;
        const int SKY_H = 16;
        const int RES_R = 32;
        const int RES_MU = 128;
        const int RES_MU_S = 32;
        const int RES_NU = 8;

        const float AVERAGE_GROUND_REFLECTANCE = 0.1f;
        //Half heights for the atmosphere air density (HR) and particle density (HM)
        //This is the height in km that half the particles are found below
        const float HR = 8.0f;
        const float HM = 1.2f;
        //scatter coefficient for mie
        readonly Vector3 BETA_MSca = new Vector3(4e-3f, 4e-3f, 4e-3f);
        [SerializeField]
        Material m_skyMaterial;

        [SerializeField]
        Material m_skyMapMaterial;
        //scatter coefficient for rayliegh
        [SerializeField]
        Vector3 m_betaR = new Vector3(5.8e-3f, 1.35e-2f, 3.31e-2f);
        //Asymmetry factor for the mie phase function
        //A higher number meands more light is scattered in the forward direction
        [SerializeField]
        float m_mieG = 0.85f;
        Mesh m_mesh;
        RenderTexture m_transmittance, m_inscatter, m_irradiance, m_skyMap;

        //todo missing everything here
        protected override void Start()
        {
            base.Start();
            m_mesh = MeshFactory.MakePlane(2, 2, MeshFactory.PLANE.XY, false);
            m_mesh.bounds = new Bounds(Vector3.zero, new Vector3(1e8f, 1e8f, 1e8f));
            //we put the sky reflection orb into Center vector potion at center point.
            Vector3 centerPoint = MeshFactory.GetCenterPoint(m_mesh);

            //The sky map is used to create a reflection of the sky for objects that need it (like the ocean)
            m_skyMap = new RenderTexture(512, 512, 0, RenderTextureFormat.ARGBHalf);
            m_skyMap.filterMode = FilterMode.Trilinear;
            m_skyMap.wrapMode = TextureWrapMode.Clamp;
            m_skyMap.anisoLevel = 9;
            m_skyMap.useMipMap = true;
            m_skyMap.Create();

            //
            //Transmittance is responsible for the change in the sun color as it moves
            //The raw file is a 2D array of 32 bit floats with a range of 0 to 1
            m_transmittance = new RenderTexture(TRANSMITTANCE_W, TRANSMITTANCE_H, 0, RenderTextureFormat.ARGBHalf);
            m_transmittance.wrapMode = TextureWrapMode.Clamp;
            m_transmittance.filterMode = FilterMode.Bilinear;
            m_transmittance.enableRandomWrite = true;
            m_transmittance.Create();

            ComputeBuffer buff = new ComputeBuffer(TRANSMITTANCE_W * TRANSMITTANCE_H, sizeof(float) * 3);
            ComputeBufferUtility.WriteIntoRenderTexture(m_transmittance, 3, ConfigSystem.Instance.Config.Files.Transmittance, buff, manager.WriteData);
            buff.Release();

            //Iirradiance is responsible for the change in the sky color as the sun moves
            //The raw file is a 2D array of 32 bit floats with a range of 0 to 1
            m_irradiance = new RenderTexture(SKY_W, SKY_H, 0, RenderTextureFormat.ARGBHalf);
            m_irradiance.wrapMode = TextureWrapMode.Clamp;
            m_irradiance.filterMode = FilterMode.Bilinear;
            m_irradiance.enableRandomWrite = true;
            m_irradiance.Create();
            buff = new ComputeBuffer(TRANSMITTANCE_W * TRANSMITTANCE_H, sizeof(float) * 3);
            ComputeBufferUtility.WriteIntoRenderTexture(m_irradiance, 3, ConfigSystem.Instance.Config.Files.Irradiance, buff, manager.WriteData);
            buff.Release();

            //Inscatter is responsible for the change in the sky color as the sun moves
            //The raw file is a 4D array of 32 bit floats with a range of 0 to 1.589844
            //As there is not such thing as a 4D texture the data is packed into a 3D texture 
            //and the shader manually performs the sample for the 4th dimension
            m_inscatter = new RenderTexture(RES_MU_S * RES_NU, RES_MU, 0, RenderTextureFormat.ARGBHalf);
            m_inscatter.volumeDepth = RES_R;
            m_inscatter.wrapMode = TextureWrapMode.Clamp;
            m_inscatter.filterMode = FilterMode.Bilinear;
            m_inscatter.isVolume = true;
            m_inscatter.enableRandomWrite = true;
            m_inscatter.Create();

            buff = new ComputeBuffer(RES_MU_S * RES_NU * RES_MU * RES_R, sizeof(float) * 4);
            ComputeBufferUtility.WriteIntoRenderTexture(m_inscatter, 4, ConfigSystem.Instance.Config.Files.Inscatter, buff, manager.WriteData);
            buff.Release();

            InitUniforms(m_skyMaterial);
            InitUniforms(m_skyMapMaterial);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            m_transmittance.Release();
            m_irradiance.Release();
            m_inscatter.Release();
            m_skyMap.Release();
        }

        public void UpdateNode()
        {
            SetUniforms(m_skyMaterial);
            SetUniforms(m_skyMapMaterial);


        }
        public void SetUniforms(Material mat)
        {
            //Sets uniforms that this or other gameobjects may need
            if (mat == null) return;

            mat.SetVector("betaR", m_betaR / 1000.0f);
            mat.SetFloat("mieG", Mathf.Clamp(m_mieG, 0.0f, 0.99f));
            mat.SetTexture("_Sky_Transmittance", m_transmittance);
            mat.SetTexture("_Sky_Inscatter", m_inscatter);
            mat.SetTexture("_Sky_Irradiance", m_irradiance);
            mat.SetTexture("_Sky_Map", m_skyMap);

            manager.SunNode.SetUniforms(mat);
        }

        public void InitUniforms(Material mat)
        {
            //Init uniforms that this or other gameobjects may need
            if (mat == null) return;

            mat.SetFloat("scale", Rg / manager.Radius);
            mat.SetFloat("Rg", Rg);
            mat.SetFloat("Rt", Rt);
            mat.SetFloat("RL", RL);
            mat.SetFloat("TRANSMITTANCE_W", TRANSMITTANCE_W);
            mat.SetFloat("TRANSMITTANCE_H", TRANSMITTANCE_H);
            mat.SetFloat("SKY_W", SKY_W);
            mat.SetFloat("SKY_H", SKY_H);
            mat.SetFloat("RES_R", RES_R);
            mat.SetFloat("RES_MU", RES_MU);
            mat.SetFloat("RES_MU_S", RES_MU_S);
            mat.SetFloat("RES_NU", RES_NU);
            mat.SetFloat("AVERAGE_GROUND_REFLECTANCE", AVERAGE_GROUND_REFLECTANCE);
            mat.SetFloat("HR", HR * 1000.0f);
            mat.SetFloat("HM", HM * 1000.0f);
            mat.SetVector("betaMSca", BETA_MSca / 1000.0f);
            mat.SetVector("betaMEx", (BETA_MSca / 1000.0f) / 0.9f);

        }

        private void OnGUI()
        {
            if (ConfigSystem.Instance.GameModeConfig.Debug.Active && ConfigSystem.Instance.GameModeConfig.Debug.DebugLines.Sky)
                GUI.DrawTexture(new Rect(0, 0, 512, 512), m_skyMap);
        }
    }
}
