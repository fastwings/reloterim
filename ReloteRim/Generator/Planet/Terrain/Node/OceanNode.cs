﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Terrain.Node
{
    /// <summary>
    /// An AbstractTask to draw a flat or spherical ocean.
    /// This class provides the functions and data to draw a flat projected grid but nothing else
    /// </summary>
    public abstract class OceanNode : NodeCore
    {
        [SerializeField]
        protected Material m_oceanMaterial;

        [SerializeField]
        protected Color m_oceanUpwellingColor = new Color(0.039f, 0.156f, 0.47f);
        //Sea level in meters
        [SerializeField]
        protected float m_oceanLevel = 5.0f;
        //The maximum altitude at which the ocean must be displayed.
        [SerializeField]
        protected float m_zmin = 20000.0f;
        //Size of each grid in the projected grid. (number of pixels on screen)
        [SerializeField]
        protected int m_resolution = 4;

        Mesh[] screenGrid;
        Matrix4d m_oldLtoo;
        Vector3d m_offset;

        //If the ocean should be draw. To minimize depth fighting the ocean is not draw 
        //when the camera is far away. Instead the terrain shader should render the ocean areas directly on the terrain
        bool m_drawOcean;

        //Concrete classes must provide a function that returns the
        //variance of the waves need for the BRDF rendering of waves
        public abstract float GetMaxSlopeVariance();

        public bool DrawOcean
        {
            get { return m_drawOcean; }
        }


        protected override void Start()
        {
            base.Start();
            manager.SkyNode.InitUniforms(m_oceanMaterial);
            m_oldLtoo = Matrix4d.Identity();
            m_offset = Vector3d.Zero();

            //Create the projected grid. The resolution is the size in pixels
            //of each square in the grid. If the squares are small the size of
            //the mesh will exceed the max verts for a mesh in Unity. In this case 
            //split the mesh up into smaller meshes.

            m_resolution = Mathf.Max(1, m_resolution);
            int nx = Screen.width / m_resolution;
            int ny = Screen.height / m_resolution;
            int numGrids = 1;
            const int MAX_VERTS = 65000;
            if (nx * ny > MAX_VERTS)
            {
                numGrids += (nx * ny) / MAX_VERTS;
            }
            screenGrid = new Mesh[numGrids];
            //Make the meshes. The end product will be a grid of verts that cover 
            //the screen on the x and y axis with the z depth at 0. This grid is then
            //projected as the ocean by the shader
            for (int i = 0; i < numGrids; i++)
            {
                ny = Screen.height / numGrids / m_resolution;
                screenGrid[i] = MakePlane(nx, ny, Convert.ToSingle(i) / Convert.ToSingle(numGrids), 1.0f / Convert.ToSingle(numGrids));

            }


        }


        private Mesh MakePlane(int w, int h, float offset, float scale)
        {
            Vector3[] vertices = new Vector3[w * h];
            Vector2[] texcoord = new Vector2[w * h];
            Vector3[] normals = new Vector3[w * h];
            int[] indices = new int[w * h * 6];
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    Vector2 uv = new Vector2(
                        Convert.ToSingle((float)x / (float)(w - 1)),
                        Convert.ToSingle((float)y / (float)(h - 1))
                        );
                    uv.y *= scale;
                    uv.y += offset;

                    Vector2 p = new Vector2();
                    p.x = (uv.x - 0.5f) * 2.0f;
                    p.y = (uv.y - 0.5f) * 2.0f;

                    Vector3 pos = new Vector3(p.x, p.y, 0.0f);
                    Vector3 norm = new Vector3(0.0f, 0.0f, 1.0f);
                    texcoord[x + y * w] = uv;
                    vertices[x + y * w] = pos;
                    normals[x + y * w] = norm;
                }
            }
            int num = 0;
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    indices[num++] = x + y * w;
                    indices[num++] = x + (y + 1) * w;
                    indices[num++] = (x + 1) + y * w;

                    indices[num++] = x + (y + 1) * w;
                    indices[num++] = (x + 1) + (y + 1) * w;
                    indices[num++] = (x + 1) + y * w;
                }
            }

            Mesh mesh = new Mesh();

            mesh.vertices = vertices;
            mesh.uv = texcoord;
            mesh.triangles = indices;
            mesh.normals = normals;

            return mesh;
        }

        public virtual void UpdateNode()
        {
            //Calculates the required data for the projected grid

            // compute ltoo = localToOcean transform, where ocean frame = tangent space at
            // camera projection on sphere radius in local space
            Matrix4d ctol = GetView().CameraToWorldMatrix;
            Vector3d cl = ctol * Vector3d.Zero();
            float radius = manager.IsDeform ? manager.Radius : 0.0f;
            if ((radius == 0.0 && cl.Z > m_zmin) ||
                (radius > 0.0 && cl.Magnitude() > radius + m_zmin) ||
                (radius < 0.0 && (new Vector2d(cl.Y, cl.Z)).Magnitude() < -radius - m_zmin))
            {
                m_oldLtoo = Matrix4d.Identity();
                m_offset = Vector3d.Zero();
                m_drawOcean = false;
                return;
            }
            m_drawOcean = true;
            Vector3d ux, uy, uz, oo;
            if (radius == 0.0f)
            {
                ux = Vector3d.UnitX();
                uy = Vector3d.UnitY();
                uz = Vector3d.UnitZ();
                oo = new Vector3d(cl.X, cl.Y, 0.0f);
            }
            else
            {
                uz = cl.Normalized();
                if (m_oldLtoo != Matrix4d.Identity())
                {
                    ux =
                        (new Vector3d(m_oldLtoo.Matrix[1, 0], m_oldLtoo.Matrix[1, 1], m_oldLtoo.Matrix[1, 2])).Cross(uz)
                            .Normalized();
                }
                else
                {
                    ux = Vector3d.UnitZ().Cross(uz).Normalized();
                }
                uy = uz.Cross(ux);// unit y vector
                oo = uz * radius;// origin of ocean frame, in local space

            }
            // compute ctoo = cameraToOcean transform
            Matrix4d ltoo = new Matrix4d(
                ux.X, ux.Y, ux.Z, -ux.Dot(oo),
                uy.X, uy.Y, uy.Z, -uy.Dot(oo),
                uz.X, uz.Y, uz.Z, -uz.Dot(oo),
                0.0, 0.0, 0.0, 1.0
                );
            Matrix4d ctoo = ltoo * ctol;
            if (m_oldLtoo != Matrix4d.Identity())
            {
                Vector3d delta = ltoo * (m_oldLtoo.Inverse() * Vector3d.Zero());
                m_offset += delta;
            }
            m_oldLtoo = ltoo;
            Matrix4d stoc = GetView().ScreenToCameraMatrix;
            Vector3d oc = ctoo * Vector3d.Zero();
            double h = oc.Z;
            Vector4d stoc_w = (stoc * Vector4d.UnitW()).ToVector4WZero();
            Vector4d stoc_x = (stoc * Vector4d.UnitX()).ToVector4WZero();
            Vector4d stoc_y = (stoc * Vector4d.UnitY()).ToVector4WZero();
            Vector3d A0 = (ctoo * stoc_w).ToVector3d();
            Vector3d dA = (ctoo * stoc_x).ToVector3d();
            Vector3d B = (ctoo * stoc_y).ToVector3d();
            Vector3d horizon1, horizon2;
            Vector3d offset = new Vector3d(-m_offset.X, -m_offset.Y, oc.Z);
            if (radius == 0.0)
            {
                //Terrain ocean
                horizon1 = new Vector3d(-(h * 1e-6 + A0.Z) / B.Z, -dA.Z / B.Z, 0.0);
                horizon2 = Vector3d.Zero();
            }
            else
            {
                //planet ocean
                double h1 = h * h(h + 2.0 * radius);
                double h2 = (h + radius) * (h + radius);
                double alpha = B.Dot(B) * h1 - B.Z * B.Z * h2;
                double beta0 = (A0.Dot(B) * h1 - B.Z * A0.Z * h2) / alpha;
                double beta1 = (A0.Dot(B) * h1 - B.Z * dA.Z * h2) / alpha;
                double gamma0 = (A0.Dot(A0) * h1 - A0.Z * A0.Z * h2) / alpha;
                double gamma1 = (A0.Dot(dA) * h1 - A0.Z * dA.Z * h2) / alpha;
                double gamma2 = (dA.Dot(dA) * h1 - dA.Z * dA.Z * h2) / alpha;
                horizon1 = new Vector3d(-beta0, -beta1, 0.0);
                horizon2 = new Vector3d(beta0 * beta0 * gamma0, 2.0 * (beta0 * beta1 - gamma1), beta1 * beta1 - gamma2);
            }
            Vector3d sunDirection = new Vector3d(manager.SunNode.Direction);
            Vector3d oceanSunDirection = ltoo.ToMatrix3x3d() * sunDirection;
            m_oceanMaterial.SetVector("_Ocean_Horizon1", horizon1.Vector);
            m_oceanMaterial.SetVector("_Ocean_Horizon2", horizon2.Vector);
            m_oceanMaterial.SetMatrix("_Ocean_CameraToOcean", ctoo.ToMatrix4x4());
            m_oceanMaterial.SetMatrix("_Ocean_OceanToCamera", ctoo.Inverse().ToMatrix4x4());
            m_oceanMaterial.SetVector("_Ocean_CameraPos", offset.Vector);
            m_oceanMaterial.SetVector("_Ocean_Color", m_oceanUpwellingColor * 0.1f);
            m_oceanMaterial.SetVector("_Ocean_ScreenGridSize", new Vector2((float)m_resolution / (float)Screen.width, (float)m_resolution / (float)Screen.height));
            m_oceanMaterial.SetFloat("_Ocean_Radius", radius);

            manager.SkyNode.SetUniforms(m_oceanMaterial);
            manager.SunNode.SetUniforms(m_oceanMaterial);
            manager.SetUniforms(m_oceanMaterial);

            //Draw each mesh that makes up the projected grid
            foreach (Mesh mesh in screenGrid)
                Graphics.DrawMesh(mesh, Matrix4x4.identity, m_oceanMaterial, 0, Camera.main);
        }

        public void SetUniforms(Material mat)
        {
            //Sets uniforms that this or other gameobjects may need
            if (mat == null) return;

            mat.SetFloat("_Ocean_Sigma", GetMaxSlopeVariance());
            mat.SetVector("_Ocean_Color", m_oceanUpwellingColor * 0.1f);
            mat.SetFloat("_Ocean_DrawBRDF", (m_drawOcean) ? 0.0f : 1.0f);
            mat.SetFloat("_Ocean_Level", m_oceanLevel);
        }
    }
}
