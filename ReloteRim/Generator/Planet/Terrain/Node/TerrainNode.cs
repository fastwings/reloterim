﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Node;
using ReloteRim.Generator.Planet.Core.Utilities;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Terrain.Node
{
    /// <summary>
    /// 
    /// Provides a framework to draw and update view-dependent, TerrainQuaternion based terrains.
    /// This framework provides classes to represent the terrain TerrainQuaternion, classes to
    /// associate data produced by a TileProducer to the quads of this
    /// TerrainQuaternion, as well as classes to update and draw such terrains (which can be
    /// deformed to get spherical).
    ///
    /// A view dependent, TerrainQuaternion based terrain. This class provides access to the
    /// terrain TerrainQuaternion, defines the terrain deformation (can be used to get planet
    /// sized terrains), and defines how the terrain TerrainQuaternion must be subdivided based
    /// on the viewer position. This class does not give any direct or indirect access
    /// to the terrain data (elevations, normals, texture, etc). The terrain data must
    /// be managed by TileProducer, and stored in TileStorage. 
    /// The link between with the terrain TerrainQuaternion is provided by the TileSampler class.
    /// </summary>
    public class TerrainNode : NodeCore
    {
        private static readonly int HORIZON_SIZE = 256;

        /// <summary>
        /// material that used in the terrain
        /// </summary>
        [SerializeField]
        private Material terrianMat;

        /// <summary>
        /// Describes how the terrain TerrainQuaternion must be subdivided based on the viewer
        /// distance. For a field of view of 80 degrees, and a viewport width of 1024
        /// pixels, a quad of size L will be subdivided into subquads if the viewer
        /// distance is less than splitFactor * L. For a smaller field of view and/or
        /// a larger viewport, the quad will be subdivided at a larger distance, so
        /// that its size in pixels stays more or less the same. This number must be
        /// strictly larger than 1.
        /// </summary>
        [SerializeField]
        private float m_splitFactor = 2.0f;

        /// <summary>
        /// The maximum level at which the terrain TerrainQuaternion must be subdivided (inclusive).
        /// The terrain TerrainQuaternion will never be subdivided beyond this level, even if the
        /// viewer comes very close to the terrain.
        /// </summary>
        [SerializeField]
        private int m_maxLevel = 16;

        /// <summary>
        /// The terrain quad half size (only use on start up)
        /// </summary>
        [SerializeField]
        private float m_size = 50000.0f;

        /// <summary>
        /// The terrain quad zmin (only use on start up)
        /// </summary>

        [SerializeField]
        private float m_zmin = -5000.0f;

        /// <summary>
        /// The terrain quad zmax (only use on start up)
        /// </summary>
        [SerializeField]
        private float m_zmax = 5000.0f;

        /// <summary>
        /// Which face of the cube this terrain is for planets, of 0 for terrains
        /// </summary>
        [SerializeField]
        private int m_face = 0;

        /// <summary>
        /// True to perform horizon occlusion culling tests.
        /// </summary>
        private bool horizon_culling = true;

        /// <summary>
        /// True to subdivide invisible quads based on distance, like visible ones.
        /// </summary>
        private bool split_invisible_quads = true;

        /// <summary>
        /// The deformation of this terrain. In the terrain local space the
        /// terrain sea level surface is flat. In the terrain deformed space
        /// the sea level surface can be spherical (or flat if the
        /// identity deformation is used).
        /// </summary>
        private Deformation deform;

        /// <summary>
        /// The root of the terrain TerrainQuaternion. This TerrainQuaternion is subdivided based on the
        /// current viewer position by the update method.
        /// </summary>
        private TerrainQuaternion root;

        /// <summary>
        /// The current viewer position in the deformed terrain space.
        /// </summary>
        private Vector3d m_deformedCameraPostion;

        /// <summary>
        /// The current viewer frustum planes in the deformed terrain space.
        /// </summary>
        private Vector4d[] m_deformedFrustumPlanes;

        /// <summary>
        /// The current viewer position in the local terrain space.
        /// </summary>
        private Vector3d m_localCameraPostion;

        /// <summary>
        /// The viewer distance at which a quad is subdivided, relatively to the quad size.
        /// </summary>
        private float m_splitDistance = 1.1f;

        /// <summary>
        /// The ratio between local and deformed lengths at localCameraPos.
        /// </summary>
        private float m_distanceFactor;

        /// <summary>
        /// Pasteurized horizon elevation angle for each azimuth angle.
        /// </summary>
        private float[] m_horizion = new float[HORIZON_SIZE];

        /// <summary>
        /// Local reference frame used to compute horizon occlusion culling.
        /// </summary>
        private Matrix2d m_localCameraDirection;

        /// <summary>
        /// double precision local to world matrix
        /// </summary>
        private Matrix4d m_localToWorld;

        /// <summary>
        /// The rotation of the face to object space
        /// </summary>
        private Matrix4d m_faceToLocal;
        /// <summary>
        /// The current viewer position in the deformed terrain space.
        /// </summary>
        public Vector3d DeformedCameraPostion
        {
            get
            {
                return m_deformedCameraPostion;
            }
        }
        /// <summary>
        /// The deformation of this terrain. In the terrain local space the
        /// terrain sea level surface is flat. In the terrain deformed space
        /// the sea level surface can be spherical (or flat if the
        /// identity deformation is used).
        /// </summary>
        public Deformation Deformed
        {
            get
            {
                return deform;
            }
        }

        /// <summary>
        /// True to subdivide invisible quads based on distance, like visible ones.
        /// </summary>
        public bool SplitInvisibleQuads
        {
            get
            {
                return split_invisible_quads;
            }
        }

        /// <summary>
        /// The viewer distance at which a quad is subdivided, relatively to the quad size.
        /// </summary>
        public float SplitDistance
        {
            get
            {
                return m_splitDistance;
            }
        }
        /// <summary>
        /// The ratio between local and deformed lengths at localCameraPos.
        /// </summary>
        public float DistanceFactor
        {
            get
            {
                return m_distanceFactor;
            }
        }
        /// <summary>
        /// The current viewer frustum planes in the deformed terrain space.
        /// </summary>
        public Vector4d[] DeformedFrustumPlanes
        {
            get
            {
                return m_deformedFrustumPlanes;
            }
        }
        /// <summary>
        /// The maximum level at which the terrain TerrainQuaternion must be subdivided (inclusive).
        /// The terrain TerrainQuaternion will never be subdivided beyond this level, even if the
        /// viewer comes very close to the terrain.
        /// </summary>
        public int MaxLevel
        {
            get
            {
                return m_maxLevel;
            }
        }
        /// <summary>
        /// The current viewer position in the local terrain space.
        /// </summary>
        public Vector3d LocalCamearaPostion
        {
            get
            {
                return m_localCameraPostion;
            }
        }
        /// <summary>
        /// The root of the terrain TerrainQuaternion. This TerrainQuaternion is subdivided based on the
        /// current viewer position by the update method.
        /// </summary>
        public TerrainQuaternion Root
        {
            get
            {
                return root;
            }
        }
        /// <summary>
        /// double precision local to world matrix
        /// </summary>
        public Matrix4d LocalToWorld
        {
            get
            {
                return m_localToWorld;
            }
        }
        /// <summary>
        /// Which face of the cube this terrain is for planets, of 0 for terrains
        /// </summary>
        public int Face
        {
            get
            {
                return m_face;
            }
        }
        /// <summary>
        /// The rotation of the face to object space
        /// </summary>
        public Matrix4d FaceToLocal
        {
            get
            {
                return m_faceToLocal;
            }
        }
        /// <summary>
        /// material that used in the terrain
        /// </summary>
        public Material Material
        {
            get
            {
                return terrianMat;
            }
        }
        #region Unity Events
        protected override void Start()
        {
            base.Start();
            manager.SkyNode.InitUniforms(terrianMat);
            Vector3d[] faces = new Vector3d[]
            {
                new Vector3d(0, 0, 0), new Vector3d(90, 0, 0), new Vector3d(90, 90, 0),
                new Vector3d(90, 180, 0), new Vector3d(90, 270, 0), new Vector3d(0, 180, 180)
            };
            m_faceToLocal = Matrix4d.Identity();
            //If this terrain is deformed into a sphere the face matrix is the rotation of the 
            //terrain needed to make up the spherical planet. In this case there should be 6 terrains, each with a unique face number
            if (m_face - 1 >= 0 && m_face - 1 < 6)
            {
                m_faceToLocal = Matrix4d.Rotate(faces[m_face - 1]);
            }
            m_localToWorld = m_faceToLocal;
            float size = m_size;
            if (manager.IsDeform)
            {
                size = manager.Radius;
                deform = new SphericalDeformation(size);
            }
            else
            {
                deform = new Deformation();
            }
            root = new TerrainQuaternion(this, null, 0, 0, -size, -size, 2.0 * size, m_zmin, m_zmax);

        }

        public void UpdateNode()
        {
            m_localToWorld = m_faceToLocal;
            Matrix4d localToCamera = GetView().WorldToCameraMatrix * m_localToWorld;
            Matrix4d localToScreen = GetView().CameraToScreenMatrix * localToCamera;
            Matrix4d invLocalToCamera = localToCamera.Inverse();

            m_deformedCameraPostion = invLocalToCamera * Vector3d.Zero();
            m_deformedFrustumPlanes = Frustum.GetPlanes(localToScreen);
            m_localCameraPostion = deform.DeformedToLocal(m_deformedCameraPostion);

            Matrix4d m = deform.LocalToDeformedDifferential(m_localCameraPostion, true);
            m_distanceFactor =
                Convert.ToSingle(Math.Max((new Vector3d(m.Matrix[0, 0], m.Matrix[1, 0], m.Matrix[2, 0])).Magnitude(),
                    (new Vector3d(m.Matrix[0, 1], m.Matrix[1, 1], m.Matrix[2, 1])).Magnitude()));
            Vector3d l = m_deformedFrustumPlanes[0].ToVector3d().Normalized();
            Vector3d r = m_deformedFrustumPlanes[1].ToVector3d().Normalized();

            float fov = Convert.ToSingle(MathUtility.SafeAcos(-l.Dot(r)));
            float width = Convert.ToSingle(Screen.width);
            m_splitDistance = m_splitFactor * width / 1024.0f * Mathf.Tan(40.0f * Mathf.Deg2Rad) / Mathf.Tan(fov / 2.0f);
            if (m_splitDistance < 1.1f || !MathUtility.IsFinite(m_splitDistance))
            {
                m_splitDistance = 1.1f;
            }

            // initializes data structures for horizon occlusion culling
            if (horizon_culling && m_localCameraPostion.Z <= root.ZMax)
            {
                Vector3d deformDir = invLocalToCamera * Vector3d.UnitZ();
                Vector2d localDir = (deform.DeformedToLocal(deformDir) - m_localCameraPostion).ToVector2d().Normalized();
                m_localCameraDirection = new Matrix2d(localDir.Y, -localDir.X, -localDir.X, -localDir.Y);
                for (int i = 0; i < HORIZON_SIZE; ++i)
                {
                    m_horizion[i] = float.NegativeInfinity;
                }
            }
            root.Update();

            manager.SkyNode.SetUniforms(terrianMat);
            manager.SunNode.SetUniforms(terrianMat);
            manager.SetUniforms(terrianMat);
            deform.SetUniforms(this, terrianMat);
            if (manager.OceanNode != null)
            {
                manager.OceanNode.SetUniforms(terrianMat);
            }
            else
            {
                terrianMat.SetFloat("_Ocean_DrawBRDF", 0.0f);
            }
            //@todo here we will update and create the block entity element

        }
        #endregion
        #region Methods
        public Frustum.VISIBILTY GetVisiblity(Box3d localBox)
        {
            return deform.GetVisibility(this, localBox);
        }
        public void SetPerQuadUniforms(TerrainQuaternion quad, MaterialPropertyBlock matPropertyBlock)
        {
            deform.SetUniforms(this, quad, matPropertyBlock);
        }
        public void SetSplitInvisibleQuads(bool b)
        {
            split_invisible_quads = b;
        }
        /// <summary>
        /// Adds the given bounding box as an occluder. The bounding boxes must
        /// be added in front to back order.
        ///
        /// param occluder a bounding box in local (i.e. non deformed) coordinates.
        /// return true is the given bounding box is occluded by the bounding boxes
        /// previously added as occluders by this method.
        /// </summary>
        /// <param name="occluder">Box3d</param>
        /// <returns>bool</returns>
        public bool AddOccluder(Box3d occluder)
        {
            if (!horizon_culling && m_localCameraPostion.Z > root.ZMax)
            {
                return false;
            }
            Vector2d[] corners = new Vector2d[4];
            Vector3d[] bounds = new Vector3d[4];
            Vector2d xy = m_localCameraPostion.Vector2D;
            //now we calc the cornurs
            corners[0] = m_localCameraDirection * (new Vector2d(occluder.XMin, occluder.YMin) - xy);
            corners[1] = m_localCameraDirection * (new Vector2d(occluder.XMin, occluder.YMax) - xy);
            corners[2] = m_localCameraDirection * (new Vector2d(occluder.XMax, occluder.YMin) - xy);
            corners[3] = m_localCameraDirection * (new Vector2d(occluder.XMax, occluder.YMax) - xy);

            //we check that axis y not equal or below 0.0f if so we stop here
            if (corners[0].Y <= 0.0 || corners[1].Y <= 0.0 || corners[2].Y <= 0.0 || corners[3].Y <= 0.0)
                return false;
            double dZMin = occluder.ZMin - m_localCameraPostion.Z;
            double dZMax = occluder.ZMax - m_localCameraPostion.Z;
            bounds[0] = new Vector3d(corners[0].X, dZMin, dZMax) / corners[0].Y;
            bounds[1] = new Vector3d(corners[1].X, dZMin, dZMax) / corners[1].Y;
            bounds[2] = new Vector3d(corners[2].X, dZMin, dZMax) / corners[2].Y;
            bounds[3] = new Vector3d(corners[3].X, dZMin, dZMax) / corners[3].Y;
            double xmin = Math.Max(Math.Min(bounds[0].X, bounds[1].X), Math.Min(bounds[2].X, bounds[3].X)) * 0.83;
            double xmax = Math.Max(Math.Max(bounds[0].X, bounds[1].X), Math.Max(bounds[2].X, bounds[3].X)) * 0.83;
            double zmin = Math.Max(Math.Max(bounds[0].Y, bounds[1].Y), Math.Max(bounds[2].Y, bounds[3].Y));
            double zmax = Math.Max(Math.Max(bounds[0].Z, bounds[1].Z), Math.Max(bounds[2].Z, bounds[3].Z));
            int iMin = Math.Max((int)Math.Floor(xmin * HORIZON_SIZE), 0);
            int iMax = Math.Min((int)Math.Ceiling(xmax * HORIZON_SIZE), HORIZON_SIZE - 1);
            // first checks if the bounding box projection is below the current horizon line
            bool occluded = (iMax >= iMin);
            for (int i = iMin; i <= iMax; i++)
            {
                if (zmax > m_horizion[i])
                {
                    occluded = false;
                    break;
                }
            }
            if (!occluded)
            {
                iMin = Math.Max((int)Math.Ceiling(xmin * HORIZON_SIZE), 0);
                iMax = Math.Min((int)Math.Floor(xmax * HORIZON_SIZE), HORIZON_SIZE - 1);
                for (int i = iMin; i <= iMax; i++)
                {
                    m_horizion[i] = (float)Math.Max(m_horizion[i], zmin);
                }
            }
            return occluded;
        }
        /// <summary>
        /// 
        /// Returns true if the given bounding box is occluded by the bounding boxes
        /// previously added by AddOccluder().
        ///
        /// param box a bounding box in local (i.e. non deformed) coordinates.
        /// return true is the given bounding box is occluded by the bounding boxes
        /// previously added as occluders by AddOccluder.
        /// </summary>
        /// <param name="box">Box3d</param>
        /// <returns>bool</returns>
        public bool IsOccluded(Box3d box)
        {
            if (!horizon_culling && m_localCameraPostion.Z > root.ZMax)
            {
                return false;
            }
            Vector2d[] corners = new Vector2d[4];
            Vector2d xy = m_localCameraPostion.Vector2D;
            //now we calc the cornurs
            corners[0] = m_localCameraDirection * (new Vector2d(box.XMin, box.YMin) - xy);
            corners[1] = m_localCameraDirection * (new Vector2d(box.XMin, box.YMax) - xy);
            corners[2] = m_localCameraDirection * (new Vector2d(box.XMax, box.YMin) - xy);
            corners[3] = m_localCameraDirection * (new Vector2d(box.XMax, box.YMax) - xy);

            //we check that axis y not equal or below 0.0f if so we stop here
            if (corners[0].Y <= 0.0 || corners[1].Y <= 0.0 || corners[2].Y <= 0.0 || corners[3].Y <= 0.0)
                return false;
            double dz = box.ZMax - m_localCameraPostion.Z;

            corners[0] = new Vector2d(corners[0].X, dz) / corners[0].Y;
            corners[1] = new Vector2d(corners[1].X, dz) / corners[1].Y;
            corners[2] = new Vector2d(corners[2].X, dz) / corners[2].Y;
            corners[3] = new Vector2d(corners[3].X, dz) / corners[3].Y;

            double xmin = Math.Max(Math.Min(corners[0].X, corners[1].X), Math.Min(corners[2].X, corners[3].X)) * 0.83;
            double xmax = Math.Max(Math.Max(corners[0].X, corners[1].X), Math.Max(corners[2].X, corners[3].X)) * 0.83;
            double zmax = Math.Max(Math.Max(corners[0].Y, corners[1].Y), Math.Max(corners[2].Y, corners[3].Y));
            int iMin = Math.Max((int)Math.Floor(xmin * HORIZON_SIZE), 0);
            int iMax = Math.Min((int)Math.Ceiling(xmax * HORIZON_SIZE), HORIZON_SIZE - 1);
            for (int i = iMin; i <= iMax; i++)
            {
                if (zmax > m_horizion[i])
                {
                    return false;
                }
            }
            return (iMax >= iMin);
        }
        /// <summary>
        /// 
        /// Returns the distance between the current viewer position and the
        /// given bounding box. This distance is measured in the local terrain
        /// space (with Deformation::DistanceFactor), with altitudes divided by
        /// DistanceFactor to take deformations into account.
        /// </summary>
        /// <param name="localBox">Box3d</param>
        /// <returns>double</returns>
        public double CameraDistance(Box3d localBox)
        {
            return Math.Max(Math.Abs(m_localCameraPostion.Z - localBox.ZMax) / DistanceFactor,
                Math.Max(
                    Math.Min(Math.Abs(m_localCameraPostion.X - localBox.XMin),
                        Math.Abs(m_localCameraPostion.X - localBox.XMax)),
                    Math.Min(Math.Abs(m_localCameraPostion.Y - localBox.YMin),
                        Math.Abs(m_localCameraPostion.Y - localBox.YMax))));

        }
        #endregion
    }
}
