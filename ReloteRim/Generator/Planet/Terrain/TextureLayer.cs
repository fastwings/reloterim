﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Buffer;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Exceptions;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Terrain
{
    class TextureLayer : TileLayer
    {
        [SerializeField]
        private GameObject m_sourceProducerGo;
        private TileProducer m_sourceProducer;
        [SerializeField]
        private Material m_material;
        private RenderTexture m_texture;
        private TileProducer m_targetProducer;

        protected override void Start()
        {
            base.Start();
            m_targetProducer = GetComponent<TileProducer>();
            m_sourceProducer = m_sourceProducerGo.GetComponent<TileProducer>();

            int targetSize = m_targetProducer.GetCache().GetStorage(0).TileSize;
            int sourceSize = m_sourceProducer.GetCache().GetStorage(0).TileSize;

            if (targetSize != sourceSize && targetSize != sourceSize - 1)
            {
                throw new InvalidParameterException("Target Tile Must be equal to the source tile");
            }
            if (m_targetProducer.GetBorder() != m_sourceProducer.GetBorder())
            {
                throw new InvalidParameterException("Target border size Must be equal to the source border size");
            }
            GPUTileStorage storage = m_targetProducer.GetCache().GetStorage(0) as GPUTileStorage;
            if (storage == null)
            {
                throw new InvalidStorageException("Target Storage must be GPUTileStorage Type");
            }
            storage = m_sourceProducer.GetCache().GetStorage(0) as GPUTileStorage;
            if (storage == null)
            {
                throw new InvalidStorageException("Source Storage must be GPUTileStorage Type");
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (m_texture != null)
            {
                m_texture.Release();
            }
        }

        public override void CreateTile(int level, int tx, int ty, List<TileStorage.Slot> slot)
        {
            GPUSlot gpuSlot = slot[0] as GPUSlot;
            RenderTexture target = gpuSlot.Texture;
            if (m_texture == null)
            {
                m_texture = RenderTexture.Instantiate(target) as RenderTexture;
            }
            Tile sourceTile = m_sourceProducer.FindTile(level, tx, ty, false, true);
            GPUSlot sourceGpuSlot = null;
            if (sourceTile != null)
            {
                sourceGpuSlot = sourceTile.GetSlot(0) as GPUSlot;
            }
            else
            {
                throw new MissingTileException("Find source producer tile failed");
            }
            Vector3 coord = new Vector3(0, 0, 1);
            int targetSize = m_targetProducer.GetCache().GetStorage(0).TileSize;
            int sourceSize = m_sourceProducer.GetCache().GetStorage(0).TileSize;
            if (targetSize == sourceSize - 1)
            {
                coord.x = 1.0f / (float)sourceSize;
                coord.y = 1.0f / (float)sourceSize;
                coord.z = 1.0f - coord.x;
            }
            Graphics.Blit(target, m_texture);


            m_material.SetTexture("_Target", m_texture);
            m_material.SetTexture("_Source", sourceGpuSlot.Texture);
            m_material.SetVector("_Coords", coord);
            Graphics.Blit(null, target, m_material);
        }
    }
}
