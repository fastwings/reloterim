﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Generator.Planet.Exceptions
{
    public class NodeManagerNotFoundException : Exception
    {
        public NodeManagerNotFoundException()
        {

        }

        public NodeManagerNotFoundException(string message)
            : base(message)
        {

        }

        public NodeManagerNotFoundException(string message, Exception inner)
            : base(message, inner)
        {

        }

    }
}
