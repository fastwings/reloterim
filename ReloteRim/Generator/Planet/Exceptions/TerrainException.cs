﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Generator.Planet.Exceptions
{
    public class TerrainException : Exception
    {
        public TerrainException()
        {

        }

        public TerrainException(string message)
            : base(message)
        {

        }

        public TerrainException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
