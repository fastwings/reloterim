﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Generator.Planet.Exceptions
{
    public class InvalidStorageException : TerrainException
    {
        public InvalidStorageException()
        {

        }

        public InvalidStorageException(string message)
            : base(message)
        {

        }

        public InvalidStorageException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
