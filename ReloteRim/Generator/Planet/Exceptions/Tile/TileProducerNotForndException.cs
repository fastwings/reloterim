﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Exceptions.Tile
{
    class TileProducerNotForndException : TileException
    {
        public TileProducerNotForndException()
        {
            Debug.LogError("ReloteRim::Generator::Planet::Node::Tile::TileCache::GetTile Error!!");
            printLog("Producer id not been inserted into cache");
        }
    }
}
