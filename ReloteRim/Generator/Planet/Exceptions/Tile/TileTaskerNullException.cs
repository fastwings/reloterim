﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Exceptions.Tile
{
    public class TileTaskerNullException : TileException
    {
        public TileTaskerNullException()
        {
            Debug.LogError("ReloteRim::Generator::Planet::NodeCore::Tile::Ctor Error!!");
            printLog("task can not be null");
        }
    }
}
