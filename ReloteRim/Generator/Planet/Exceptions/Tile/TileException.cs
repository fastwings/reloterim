﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Exceptions.Tile
{
    public abstract class TileException : Exception
    {
        protected void printLog(string message)
        {
            Debug.LogError("===============================");
            Debug.LogError(String.Format("Error Message: {0},Exception Source: {1} , Exception Data: {2}  ", message, this.Source, this.StackTrace));
            Debug.LogError("===============================");
            if (this.InnerException != null)
            {
                printLog(this.InnerException);
            }
        }
        private void printLog(Exception inner)
        {
            Debug.LogError("===============================");
            Debug.LogError("===========Inner Exception============");
            Debug.LogError("===============================");
            Debug.LogError(String.Format("Exception Source: {1} , Exception Data: {2}  ", this.Source, this.StackTrace));
            Debug.LogError("===============================");
            Debug.LogError("===============================");
        }
    }
}
