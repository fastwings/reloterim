﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Exceptions.Tile
{
    class InsertProducerExistedException : TileException
    {
        public InsertProducerExistedException(int idx)
        {
            Debug.LogError("ReloteRim::Generator::Planet::NodeCore::TileCache::InsertProducer Error!!");
            printLog("Producer id (" + idx + ") already inserted");
        }
    }
}
