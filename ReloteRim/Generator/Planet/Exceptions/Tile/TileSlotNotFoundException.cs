﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Exceptions.Tile
{
    public class TileSlotNotFoundException : TileException
    {
        public TileSlotNotFoundException(int idx)
        {
            Debug.LogError("ReloteRim::Generator::Planet::NodeCore::Tile::GetSlot Error!!");
            printLog("slot at location " + idx + " does not exist");
        }
    }
}
