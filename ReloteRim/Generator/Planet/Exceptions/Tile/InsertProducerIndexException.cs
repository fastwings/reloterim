﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Exceptions.Tile
{
    class InsertProducerIndexException : TileException
    {
        public InsertProducerIndexException(int idx)
        {
            Debug.LogError("ReloteRim::Generator::Planet::NodeCore::TileCache::InsertProducer Error!!");
            printLog("tile storage at location " + idx + " does not exist");
        }
    }
}
