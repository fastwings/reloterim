﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ReloteRim.Generator.Planet.Exceptions
{
    public class InvalidParameterException : TerrainException
    {
        public InvalidParameterException()
        {

        }

        public InvalidParameterException(string message)
            : base(message)
        {

        }

        public InvalidParameterException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
