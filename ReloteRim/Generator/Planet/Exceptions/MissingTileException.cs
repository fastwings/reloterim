﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReloteRim.Generator.Planet.Exceptions;

namespace ReloteRim.Generator.Planet.Exceptions
{
    public class MissingTileException : TerrainException
    {
        public MissingTileException()
        {

        }

        public MissingTileException(string message)
            : base(message)
        {

        }

        public MissingTileException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
