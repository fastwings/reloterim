﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReloteRim.Generator.Planet.Exceptions;

namespace ReloteRim.Generator.Planet.Exceptions
{
    public class CacheCapacityException : TerrainException
    {
        public CacheCapacityException()
        {

        }

        public CacheCapacityException(string message)
            : base(message)
        {

        }

        public CacheCapacityException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
