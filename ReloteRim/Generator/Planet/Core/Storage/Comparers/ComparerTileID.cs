﻿using System.Collections.Generic;
using ReloteRim.Generator.Planet.Core.Storage.Data;

namespace ReloteRim.Generator.Planet.Core.Storage.Comparers
{
    /// <summary>
    /// A TileID is sorted based as its level. Sorts from lowest level to highest
    /// </summary>
    public class ComparerTileID : IComparer<TileID>
    {
        public int Compare(TileID a, TileID b)
        {
            return a.Compare(b);
        }
    }
    //A Id is compared based on its level, tx and ty
    public class EqualityComparerID : IEqualityComparer<TileID>
    {
        public bool Equals(TileID t1, TileID t2)
        {
            return t1.Equals(t2);
        }

        public int GetHashCode(TileID t)
        {
            return t.GetHashCode();
        }
    }

    //A Tid is compared based on its producer, level, tx and ty
    public class EqualityComparerTID : IEqualityComparer<TileID>
    {
        public bool Equals(TileID t1, TileID t2)
        {
            return t1.Equals(t2);
        }

        public int GetHashCode(TileID t)
        {
            return t.GetHashCode();
        }
    }

}
