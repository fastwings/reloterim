﻿using System.Collections.Generic;
using ReloteRim.Generator.Planet.Core.Storage.Data;

namespace ReloteRim.Generator.Planet.Core.Storage.Comparers
{
    /// <summary>
    /// A TileID is compared based on its producer, level, tx and ty
    /// </summary>
    public class EqualityComparerTileID : IEqualityComparer<TileID>
    {
        public bool Equals(TileID t1, TileID t2)
        {
            return t1.Equals(t2);
        }

        public int GetHashCode(TileID t)
        {
            return t.GetHashCode();
        }
    }
}
