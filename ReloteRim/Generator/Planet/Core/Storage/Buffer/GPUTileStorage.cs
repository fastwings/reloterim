﻿using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Storage.Buffer
{
    public class GPUTileStorage : TileStorage
    {

        public GPUTileStorage()
        {
            InternalFormat = RenderTextureFormat.ARGB32;
            FilterMode = FilterMode.Point;
            WrapMode = TextureWrapMode.Clamp;
        }
        [SerializeField]
        public RenderTextureFormat InternalFormat { get; set; }

        [SerializeField]
        public TextureWrapMode WrapMode { get; set; }

        [SerializeField]
        public FilterMode FilterMode { get; set; }

        [SerializeField]
        public RenderTextureReadWrite ReadWrite { get; set; }

        [SerializeField]
        public bool Mipmaps { get; set; }

        [SerializeField]
        public bool EnableRandomWrite { get; set; }

        [SerializeField]
        public int Ansio { get; set; }

        protected override void Awake()
        {
            base.Awake();

            int tileSize = TileSize;
            int capacity = Capacity;
            for (int i = 0; i < capacity; i++)
            {
                RenderTexture t = new RenderTexture(tileSize, tileSize, 0, InternalFormat, ReadWrite);
                t.filterMode = FilterMode;
                t.wrapMode = WrapMode;
                t.useMipMap = Mipmaps;
                t.anisoLevel = Ansio;
                t.enableRandomWrite = EnableRandomWrite;
                GPUSlot slot = new GPUSlot(this, t);
                AddSlot(i, slot);
            }
        }
    }
}