﻿using System.Runtime.InteropServices;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Storage.Buffer
{
    public class CPUTileStorage : TileStorage
    {
        public CPUTileStorage()
        {
            DataType = DATA_TYPE.FLOAT;
            Channels = 0;
        }
        public enum DATA_TYPE { FLOAT, INT, SHORT, BYTE };

        //what type of data is held in the buffer. ie float, int, etc
        [SerializeField]
        public DATA_TYPE DataType { get; set; }
        //How many channels has the data, ie a float1, float2, etc
        [SerializeField]
        public int Channels { get; set; }

        protected override void Awake()
        {
            base.Awake();
            int tileSize = TileSize;
            int capacity = Capacity;

            //Note size is sqaured as the array is 2D (but stored as a 1D array)
            int size = tileSize * tileSize * Channels;
            for (int i = 0; i < capacity; i++)
            {
                switch ((int)DataType)
                {
                    case (int)DATA_TYPE.FLOAT:
                        AddSlot(i, new CPUSlot<float>(this, size));
                        break;
                    case (int)DATA_TYPE.INT:
                        AddSlot(i, new CPUSlot<int>(this, size));
                        break;
                    case (int)DATA_TYPE.BYTE:
                        AddSlot(i, new CPUSlot<byte>(this, size));
                        break;
                    case (int)DATA_TYPE.SHORT:
                        AddSlot(i, new CPUSlot<short>(this, size));
                        break;
                }
            }
        }
    }
}