﻿using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Storage.Buffer
{
    public class CacheBufferStoage : TileStorage
    {
        public CacheBufferStoage()
        {
            m_data_type = DATA_TYPE.FLOAT;
            BufferType = ComputeBufferType.Default;
        }
        public enum DATA_TYPE { FLOAT, INT, BYTE };

        //what type of data is held in the buffer. ie float, int, etc
        [SerializeField]
        private DATA_TYPE m_data_type = DATA_TYPE.FLOAT;

        //How many channels has the data, ie a float1, float2, etc
        [SerializeField]
        private int m_channels = 1;

        [SerializeField]
        public ComputeBufferType BufferType { get; set; }

        protected override void Awake()
        {
            ComputeBuffer buffer;
            int tileSize = TileSize;
            int capacity = Capacity;
            for (int i = 0; i < capacity; i++)
            {

                switch ((int)m_data_type)
                {
                    case (int)DATA_TYPE.FLOAT:
                        buffer = new ComputeBuffer(tileSize, sizeof(float) * m_channels, BufferType);
                        break;
                    case (int)DATA_TYPE.INT:
                        buffer = new ComputeBuffer(tileSize, sizeof(int) * m_channels, BufferType);
                        break;
                    case (int)DATA_TYPE.BYTE:
                        buffer = new ComputeBuffer(tileSize, sizeof(byte) * m_channels, BufferType);
                        break;
                    default:
                        buffer = new ComputeBuffer(tileSize, sizeof(float) * m_channels, BufferType);
                        break;
                }
                CacheBufferSlot slot = new CacheBufferSlot(this, buffer);
                AddSlot(i, slot);
            }
        }
    }
}