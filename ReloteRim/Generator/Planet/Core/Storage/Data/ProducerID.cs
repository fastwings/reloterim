﻿using System;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    /**
    * A tile identifier. Contains a producer id and
    * tile coordinates level,tx,ty.
    */
    public class ProducerID
    {
        public int ID { get; private set; }
        public TileID TileID { get; private set; }

        public ProducerID(int producerId, int level, int tx, int ty)
        {
            this.ID = producerId;
            this.TileID = new TileID(level, tx, ty);
        }
        public bool Equals(ProducerID Producer)
        {
            return (ID == Producer.ID && TileID.Equals(Producer.TileID));
        }
        public override int GetHashCode()
        {
            int code = ID ^ TileID.GetHashCode();
            return code.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("[ ProducerID: {0} , Tile: {1} ]", ID, TileID.ToString());
        }
    }
}
