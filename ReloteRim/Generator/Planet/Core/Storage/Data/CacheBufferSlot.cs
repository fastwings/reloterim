﻿using ReloteRim.Generator.Planet.Core.Node.Tile;
using UnityEngine;
using System.Collections.Generic;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    public class CacheBufferSlot : TileStorage.Slot
    {
        public ComputeBuffer Buffer { get; private set; }
        public override void Release()
        {
            if (Buffe != null)
            {
                Buffer.Release();
            }
        }

        public CBSlot(TileStorage owner, ComputeBuffer buffer)
            : base(owner)
        {
            Buffer = buffer;
        }
    }
}