﻿using ReloteRim.Generator.Planet.Core.Node.Tile;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    public class GPUSlot : TileStorage.Slot
    {
        public RenderTexture Texture { get; private set; }
        public GPUSlot(TileStorage owner, RenderTexture texture)
            : base(owner)
        {
            Texture = texture;
        }

        public override void Release()
        {
            if (Texture != null)
            {
                Texture.Release();
            }
        }
    }
}