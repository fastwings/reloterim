﻿using System;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    [Serializable]
    public class Position
    {
        /// <summary>
        /// The x and y coordinate of the point the camera is looking at on the ground.
        /// For a planet view these are the longitudes and latitudes
        /// </summary>
        public double X { get; set; }
        public double Y { get; set; }
        /// <summary>
        /// The zenith angle of the vector between the "look at" point and the camera.
        /// </summary>
        public double Theta { get; set; }
        /// <summary>
        /// The azimuth angle of the vector between the "look at" point and the camera.
        /// </summary>
        public double Phi { get; set; }
        /// <summary>
        /// The distance between the "look at" point and the camera.
        /// </summary>
        public double Distance { get; set; }

        public override string ToString()
        {
            return "Pos(x,y) = " + X + "," + Y + " Theta = " + Theta + " Phi = " + Phi + " distance = " + Distance;
        }
    }
}
