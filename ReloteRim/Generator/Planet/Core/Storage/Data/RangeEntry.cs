﻿namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    public class RangeEntry
    {
        public float Max = 0.0f;
        public float Min = 0.0f;

        public RangeEntry()
        {

        }

        public RangeEntry(RangeEntry r)
        {
            Min = r.Min;
            Max = r.Max;
        }
    }
}