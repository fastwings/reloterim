﻿using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    public class PlanetGPUUniforms
    {
        public int LocalToTangentFrame { get; set; }
        public int TangentFrameToWorld { get; set; }
        public int TangentFrameToScreen { get; set; }
        public int FocusPoint { get; set; }
        public int CameraRefPos { get; set; }
        public int MaxDist { get; set; }
        public int Clip { get; set; }
        public int PlantsRadius { get; set; }
        public int PlantsDensity { get; set; }
        public int TangentSunDir { get; set; }


        public PlanetGPUUniforms()
        {
            LocalToTangentFrame = Shader.PropertyToID("_Plants_LocalToTangentFrame");
            TangentFrameToWorld = Shader.PropertyToID("_Plants_TangentFrameToWorld");
            TangentFrameToScreen = Shader.PropertyToID("_Plants_TangentFrameToScreen");
            FocusPoint = Shader.PropertyToID("_Plants_FocalPos");
            CameraRefPos = Shader.PropertyToID("_Plants_CameraRefPos");
            MaxDist = Shader.PropertyToID("_Plants_MaxDist");
            Clip = Shader.PropertyToID("_Plants_Clip");
            PlantsRadius = Shader.PropertyToID("_Plants_Radius");
            PlantsDensity = Shader.PropertyToID("_Plants_Density");
            TangentSunDir = Shader.PropertyToID("_Plants_TangentSunDir");
        }

    }
}
