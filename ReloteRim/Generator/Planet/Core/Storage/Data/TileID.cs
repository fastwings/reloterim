﻿using System;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    /**
    * A tile identifier for a given producer. Contains the tile coordinates
    * level, tx, ty.
    */
    public class TileID
    {
        private int level, tx, ty;
        public int Level
        {
            get
            {
                return level;
            }
        }
        public int TX
        {
            get
            {
                return tx;
            }
        }
        public int TY
        {
            get
            {
                return ty;
            }
        }
        public TileID(int level, int tx, int ty)
        {
            this.level = level;
            this.tx = tx;
            this.ty = ty;
        }

        public int Compare(TileID ID)
        {
            return level.CompareTo(ID.Level);
        }

        public bool Equals(TileID ID)
        {
            return (level == ID.Level && tx == ID.TX && ty == ID.TY);
        }
        public override string ToString()
        {
            return String.Format("[ Level: {0} , TX: {1} , TY{2} ]", level, tx, ty);
        }
    }
}
