﻿using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    public class GPUUniforms
    {
        public int Tile { get; set; }
        public int Size { get; set; }
        public int Coords { get; set; }
        public GPUUniforms(string name)
        {
            Tile = Shader.PropertyToID("_" + name + "_Tile");
            Size = Shader.PropertyToID("_" + name + "_TileSize");
            Coords = Shader.PropertyToID("_" + name + "_TileCoords");

        }

    }
}
