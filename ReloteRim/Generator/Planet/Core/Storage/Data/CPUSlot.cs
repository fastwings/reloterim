﻿using ReloteRim.Generator.Planet.Core.Node.Tile;

namespace ReloteRim.Generator.Planet.Core.Storage.Data
{
    public class CPUSlot<T> : TileStorage.Slot
    {
        public T[] Data { get; set; }
        public int Size { get; private set; }

        public void Clear()
        {
            Data = new T[Size];
        }

        public CPUSlot(TileStorage owner, int size)
            : base(owner)
        {
            Data = new T[size];
            Size = size;
        }
    }
}