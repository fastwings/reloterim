﻿using ReloteRim.Generator.Planet.Core.Utilities;
using ReloteRim.Generator.Planet.Exceptions;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Node
{
    /// <summary>
    /// Provides a common interface for nodes (ie terrain node, ocean node etc)
    /// Also for tile samplers and producers. Provides access to the manager so
    /// common data can be shared.
    /// </summary>
    public abstract class NodeCore : MonoBehaviour
    {
        protected Manager manager;
        public TerrainView GetView()
        {
            return manager.GetController().GetView();
        }

        protected virtual void Awake()
        {
            FindManager();
        }

        protected virtual void Start()
        {
            if (manager == null)
                FindManager();
        }

        protected virtual void OnDestroy() { }

        /// <summary>
        /// Used if the node has data that nees to be drawn by a camera in the OnPostRender function 
        /// See the PostRender.cs script for more info
        /// </summary>
        public virtual void PostRender()
        {

        }
        private void FindManager()
        {
            Transform t = transform;
            while (t != null)
            {
                Manager m = t.GetComponent<Manager>();
                if (m != null)
                {
                    manager = m;
                    break;
                }
                t = t.parent;
            }
            if (manager == null)
            {
                Debug.LogError("ReloteRim::Generator::Planet::NodeCore - Could not find manager!!!!. This gameObject must be a child of the manager");
                throw new NodeManagerNotFoundException();
            }
        }
    }
}
