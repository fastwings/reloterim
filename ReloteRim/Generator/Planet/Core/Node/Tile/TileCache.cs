﻿using System.Collections.Generic;
using ReloteRim.Common.Utilities.Containers;
using ReloteRim.Generator.Planet.Core.Storage.Comparers;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Core.Tasks;
using ReloteRim.Generator.Planet.Exceptions.Tile;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    /// <summary>
    /// A cache of tiles to avoid recompiling recently produced tiles. A tile cache keeps track of which tiles (identified by their level,tx,ty coordinates) are
    ///  currently stored in an associated TileStorage. It also keeps track of which
    ///  tiles are in use, and which are not. Unused tiles are kept in the TileStorage
    ///  as long as possible, in order to avoid re creating them if they become needed
    ///  again. But the storage associated with unused tiles can be reused to store
    ///  other tiles at any moment (in this case we say that a tile is evicted from
    ///  the cache of unused tiles).
    ///  Conversely, the storage associated with tiles currently in use cannot be
    ///  reaffixed until these tiles become unused. A tile is in use when it is
    ///  returned by GetTile, and becomes unused when PutTile is called (more
    ///  precisely when the number of users of this tile becomes 0, this number being
    ///  incremented and decremented by GetTile and PutTile, respectively). The
    ///  tiles that are needed to render the current frame should be declared in use,
    ///  so that they are not evicted between their creation and their actual
    ///  rendering.
    ///  A cache can have multiple TileStorages attached to it and the slot created is made up
    ///  of a slot from each of the TileStorages. This is so producer can generate tiles that contain 
    ///  multiple types of data associated with the same tile. For example the PlantsProducer uses a cache with 2 CBTileStorages,
    ///  one slot for the plants position and one for the plants other parameters.
    /// </summary>
    public class TileCache : MonoBehaviour
    {
        /// <summary>
        /// Next local identifier to be used for a TileProducer using this cache.
        /// </summary>
        static int next_producer_id = 0;
        /// <summary>
        /// The total number of slots managed by the TileStorage attached to the cache.
        /// </summary>
        [SerializeField]
        int m_capacity;
        /// <summary>
        /// The storage to store the tiles data.
        /// </summary>
        TileStorage[] m_tileStorage;
        /// <summary>
        /// The tiles currently in use. These tiles cannot be evicted from the cache
        /// and from the TileStorage, until they become unused. Maps tile identifiers
        /// to actual tiles.
        /// </summary>
        Dictionary<TileID, Tile> m_usedTiles;
        /// <summary>
        /// The unused tiles. These tiles can be evicted from the cache at any moment.
        /// Uses a custom container (DictionaryQueue) that can store tiles by there ProducerID for fast look up
        /// and also keeps track of the order the tiles were inserted so it can also act as a queue
        /// </summary>
        DictionaryQueue<TileID, Tile> m_unusedTiles;

        /// <summary>
        /// The producers that use this TileCache. Maps local producer identifiers to actual producers
        /// </summary>
        Dictionary<int, TileProducer> producers;

        int maxUseTiles = 0;

        void Awake()
        {
            m_tileStorage = GetComponents<TileStorage>();
            producers = new Dictionary<int, TileProducer>();
            m_usedTiles = new Dictionary<TileID, Tile>(new EqualityComparerTID());
            m_unusedTiles = new DictionaryQueue<TileID, Tile>(new EqualityComparerTID());
        }

        public int NextProducerId()
        {
            return next_producer_id++;
        }

        public void InsertProducer(int id, TileProducer producer)
        {
            if (producers.ContainsKey(id))
            {
                throw new InsertProducerExistedException(id);
            }
            else
            {
                producers.Add(id, producer);
            }
        }
        /// <summary>
        /// Returns the storage used to store the actual tiles data.
        /// </summary>
        /// <param name="idx">int</param>
        /// <returns>TileStorage</returns>
        public TileStorage GetStorage(int idx)
        {
            if (idx >= m_tileStorage.Length)
            {
                throw new InsertProducerIndexException(idx);
            }
            return m_tileStorage[idx];
        }
        /// <summary>
        /// The total number of slots managed by the TileStorage attached to the cache.
        /// </summary>
        public int Capacity
        {
            get
            {
                return m_capacity;
            }
        }
        /// <summary>
        /// get the total tiles in the storage 
        /// </summary>
        public int TileStorageCount
        {
            get
            {
                return m_tileStorage.Length;
            }
        }
        /// <summary>
        /// Returns the number of tiles currently in use in this cache.
        /// </summary>
        public int UsedTilesCount
        {
            get
            {
                return m_usedTiles.Count;
            }
        }
        /// <summary>
        /// Returns the number of tiles currently unused in this cache.
        /// </summary>
        public int UnusedTilesCount
        {
            get
            {
                return m_unusedTiles.Count();
            }
        }
        public int MaxUsedTiles
        {
            get
            {
                return maxUseTiles;
            }
        }

        #region Methods
        /// <summary>
        /// Call this when a tile is no longer needed.
        /// If the number of users of the tile is 0 then the tile will be moved from the used to the unused cache
        /// </summary>
        /// <param name="tile">Tile</param>
        public void PutTile(Tile tile)
        {
            if (tile == null) return;
            tile.DecrementUsers();
            //if there are no more users of this tile move the tile from the used cahce to the unused cache
            if (tile.Users <= 0)
            {
                TileID id = tile.TileId;
                if (m_unusedTiles.ContainsKey(id))
                {
                    m_usedTiles.Remove(id);
                }
                if (!m_unusedTiles.ContainsKey(id))
                {
                    m_unusedTiles.AddLast(id, tile);
                }
            }
        }
        /// <summary>
        /// Call this if a tile is needed. Will move the tile from the unused to the used cache if its is found there.
        /// If the tile is not found then a new tile will be created with a new slot. If there are no more free
        /// slots then the cache capacity has not been set to a high enough value and the program must abort.
        /// </summary>
        /// <param name="producerId">int</param>
        /// <param name="level">int</param>
        /// <param name="tx">int</param>
        /// <param name="ty">int</param>
        /// <returns>Tile</returns>
        public Tile GetTile(int producerId, int level, int tx, int ty)
        {
            if (!producers.ContainsKey(producerId))
            {
                throw new TileProducerNotForndException();
            }

            TileID id = Tile.GetTileId(level, tx, ty);
            Tile tile = null;

            //check if tile not in cache
            if (!m_usedTiles.ContainsKey(id))
            {
                //check if tile not unused storage of the cache
                if (!m_unusedTiles.ContainsKey(id))
                {
                    List<TileStorage.Slot> slot = NewSlot();
                    //if there no more free slots then we reuse free s;pts from unused in cache
                    if (slot == null && !m_unusedTiles.Empty())
                    {
                        //Remove the tile and recylce its slot
                        slot = m_unusedTiles.RemoveFirst().Slots;
                    }
                    //if  slot is create a new tile task
                    if (slot != null)
                    {
                        CreateTileTask task = producers[producerId].CreateTile(level, tx, ty, slot);
                        tile = new Tile(producerId, level, tx, ty, task);
                    }

                }
                else
                {
                    //we found such tile been in que as unused slot
                    tile = m_unusedTiles.Remove(id);
                }
            }
            else
            {
                tile = m_usedTiles[id];
            }

            // case if tile is null 
            if (tile == null)
            {
                throw new System.ArgumentNullException("Tile Can't be null");
            }
            //we keep trace of the number of tiles for debug used
            if (m_usedTiles.Count > maxUseTiles)
                maxUseTiles = m_usedTiles.Count;

            //up number of users on tile
            tile.IncrementUsers();
            return tile;
        }

        /// <summary>
        /// fine tile on cache que storage on the system
        /// </summary>
        /// <param name="producerId">int</param>
        /// <param name="level">int</param>
        /// <param name="tx">int</param>
        /// <param name="ty">int</param>
        /// <param name="includeUnusedCache">bool</param>
        /// <returns>Tile</returns>
        public Tile FindTile(int producerId, int level, int tx, int ty, bool includeUnusedCache)
        {
            TileID id = Tile.GetTileId(level, tx, ty);
            Tile tile = null;
            //look up in used cache que

            if (m_usedTiles.ContainsKey(id))
            {
                tile = m_usedTiles[id];
            }
            //ad flag state we look in unused tiles  que
            if (tile == null && includeUnusedCache)
            {
                if (m_unusedTiles.ContainsKey(id))
                    tile = m_unusedTiles.Get(id);
            }
            return tile;
        }
        /// <summary>
        /// Creates a new slot for a tile. A slot is made up of a slot from
        /// each of the TileStorages attached to the TileCache.
        /// If anyone of the storages runs out of slots then null will be returned and the program 
        /// should abort if this happens.
        /// </summary>
        /// <returns>List<TileStorage.Slot></returns>
        private List<TileStorage.Slot> NewSlot()
        {
            List<TileStorage.Slot> slots = new List<TileStorage.Slot>();
            foreach (TileStorage item in m_tileStorage)
            {
                TileStorage.Slot slot = item.NewSlot();
                if (slot == null) return null;
                slots.Add(slot);
            }
            return slots;
        }


        #endregion
    }
}
