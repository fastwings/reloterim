﻿using System.Collections.Generic;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    public abstract class TileLayer : NodeCore
    {
        protected override void Start()
        {
            base.Start();
        }

        public abstract void CreateTile(int level, int tx, int ty, List<TileStorage.Slot> slot);
    }
}
