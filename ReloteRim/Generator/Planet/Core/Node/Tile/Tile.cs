﻿using System.Collections.Generic;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Core.Tasks;
using ReloteRim.Generator.Planet.Exceptions.Tile;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    /// <summary>
    /// a Tite represent the squre of a chunk of the part of the world.
    /// its has level ,tx ,ty.
    /// A Tile
    ///  describes where the tile is stored in the TileStorage, how its data can
    ///  be produced, and how many users currently use it.
    ///  Contains the keys (Id, Tid) commonly used to store the tiles in data structures like dictionaries
    /// </summary>
    public class Tile
    {
        /// <summary>
        /// producer Id that manages this tile.
        /// </summary>
        public int ProduserId { get; private set; }
        /// <summary>
        /// number of users that use this tile
        /// </summary>
        public int Users { get; private set; }
        /// <summary>
        /// the QuaternionTree level of the tile
        /// </summary>
        public int Level { get; private set; }
        /// <summary>
        /// The QuaternionTree  Axis X coordinate of this tile at level level.
        /// Varies between 0 and 2^level - 1.
        /// </summary>
        public int TX { get; private set; }
        /// The QuaternionTree Axis Y coordinate of this tile at level level.
        /// Varies between 0 and 2^level - 1.
        public int TY { get; private set; }
        /// <summary>
        /// The task that produces or produced the actual tile data.
        /// </summary>
        public CreateTileTask Tasker { get; set; }
        public Tile(int producerId, int level, int tx, int ty, CreateTileTask tasker)
        {
            ProduserId = producerId;
            Level = level;
            TX = tx;
            TY = ty;
            Tasker = tasker;
            if (Tasker == null)
            {
                throw new TileTaskerNullException();
            }
        }
        public List<TileStorage.Slot> Slots
        {
            get
            {
                return Tasker.Slots;
            }
        }
        public TileStorage.Slot GetSlot(int idx)
        {
            if (idx >= Tasker.Slots.Count)
            {
                throw new TileSlotNotFoundException(idx);
            }
            return Tasker.Slots[idx];
        }

        public void IncrementUsers()
        {
            Users++;
        }

        public void DecrementUsers()
        {
            Users--;
        }
        public TileID TileId
        {
            get
            {
                return new TileID(Level, TX, TY);
            }
        }
        public static TileID GetTileId(int level, int tx, int ty)
        {
            return new TileID(level, tx, ty);
        }
    }
}
