﻿using System;
using System.Collections.Generic;
using ReloteRim.Generator.Planet.Core.Tasks;
using ReloteRim.Generator.Planet.Exceptions.Tile;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    /// <summary>
    /// An abstract producer of tiles. A TileProducer must be inherited from and overide the DoCreateTile
    /// function to create the tiles data.
    /// Note that several TileProducer can share the same TileCache, and hence the
    /// same TileStorage.
    /// </summary>
    [RequireComponent(typeof(TileSampler))]
    public abstract class TileProducer : NodeCore
    {
        /// <summary>
        /// The name of the uniforms this producers data will be bound if used in a shader
        /// </summary>
        [SerializeField]
        GameObject cacheObject;
        TileCache cache;
        /// <summary>
        /// The name of the uniforms this producers data will be bound if used in a shader
        /// </summary>
        [SerializeField]
        String name;

        /// <summary>
        /// Does this producer use the gpu 
        /// </summary>
        [SerializeField]
        bool isActiveGPU = true;
        /// <summary>
        /// layers that may modify the tile created by this producer and are optional
        /// </summary>
        TileLayer[] layers;
        /// <summary>
        /// The tile sampler associated with this producer
        /// </summary>
        TileSampler sampler;
        /// <summary>
        /// The id of this producer. This id is local to the TileCache used by this
        /// producer, and is used to distinguish all the producers that use this cache.
        /// </summary>
        int m_id;

        /// <summary>
        /// Returns the TileCache that stores the tiles produced by this producer.
        /// </summary>
        /// <returns>TileCache</returns>
        public TileCache GetCache()
        {
            InitCache();
            return cache;
        }
        public bool IsProduserActiveGPU
        {
            get
            {
                return isActiveGPU;
            }
        }
        public int ProduserId
        {
            get
            {
                return m_id;
            }
        }
        public string ProduserName
        {
            get
            {
                return name;
            }
        }
        public TileSampler Sampler
        {
            get
            {
                return sampler;
            }
        }
        public TerrainNode TerrainNode
        {
            get
            {
                return sampler.TerrainNode;
            }
        }
        #region Unity Events
        protected override void Start()
        {
            base.Start();
            InitCache();
            layers = GetComponents<TileLayer>();
            sampler = GetComponent<TileSampler>();
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();
        }

        #endregion
        #region Methods

        void InitCache()
        {
            if (cache == null)
            {
                cache = cacheObject.GetComponent<TileCache>();
                m_id = cache.NextProducerId();
            }
        }
        /// <summary>
        /// Returns the size in pixels of the border of each tile. Tiles made of
        /// raster data may have a border that contains the value of the neighboring
        /// pixels of the tile. For instance if the tile size (returned by
        /// TileStorage.GetTileSize) is 196, and if the tile border is 2, this means
        /// that the actual tile data is 192x192 pixels, with a 2 pixel border that
        /// contains the value of the neighboring pixels. Using a border introduces
        /// data redundancy but is usefull to get the value of the neighboring pixels
        /// of a tile without needing to load the neighboring tiles.
        /// </summary>
        /// <returns></returns>
        public virtual int GetBorder()
        {
            return 0;
        }
        public int GetTileSize(int i)
        {
            int tileSize = 0;
            try
            {
                tileSize = GetCache().GetStorage(i).TileSize;
            }
            catch (InsertProducerIndexException e) { }
            return tileSize;
        }
        public int GetTileSizeMinBorder(int i)
        {
            int tileSize = 0;
            try
            {
                tileSize = GetCache().GetStorage(i).TileSize;
            }
            catch (InsertProducerIndexException e) { }
            return tileSize - GetBorder() * 2;
        }
        /// <summary>
        /// Returns true if this producer can produce the given tile.
        ///  param level the tile's QuaternionTree level.
        /// param tx the tile's QuaternionTree x coordinate.
        /// param ty the tile's QuaternionTree y coordinate.
        /// </summary>
        /// <param name="level">int</param>
        /// <param name="tx">int</param>
        /// <param name="ty">int</param>
        /// <returns>bool</returns>
        public virtual bool HasTile(int level, int tx, int ty)
        {
            return true;
        }
        /// <summary>
        /// Returns true if this producer can produce the children of the given tile.
        /// param level the tile's QuaternionTree level.
        /// param tx the tile's QuaternionTree x coordinate.
        /// param ty the tile's QuaternionTree y coordinate.
        /// </summary>
        /// <param name="level">int</param>
        /// <param name="tx">int</param>
        /// <param name="ty">int</param>
        /// <returns>bool</returns>
        public virtual bool HasChildren(int level, int tx, int ty)
        {
            return HasTile(level + 1, 2 * tx, 2 * ty);
        }
        /// <summary>
        /// Decrements the number of users of this tile by one. If this number
        /// becomes 0 the tile is marked as unused, and so can be evicted from the
        /// cache at any moment.
        /// param tile a tile currently in use.
        /// </summary>
        /// <param name="tile">Tile</param>
        public virtual void PutTile(Tile tile)
        {
            cache.PutTile(tile);
        }
        public virtual Tile GetTile(int level, int tx, int ty)
        {
            return cache.GetTile(m_id, level, tx, ty);
        }
        /// <summary>
        /// 
        /// Looks for a tile in the TileCache of this TileProducer.
        ///
        /// param level the tile's quadtree level.
        /// param tx the tile's quadtree x coordinate.
        /// param ty the tile's quadtree y coordinate.
        /// param includeUnusedCache true to include both used and unused tiles in the
        /// search, false to include only the used tiles.
        /// param done true to check that the tile's creation task is done.
        /// 
        /// return the requested tile, or NULL if it is not in the TileCache or
        /// if 'done' is true, if it is not ready. This method does not change the
        /// number of users of the returned tile.
        /// </summary>
        /// <param name="level">int</param>
        /// <param name="tx">int</param>
        /// <param name="ty">int</param>
        /// <param name="includeUnusedCache">bool</param>
        /// <param name="done">bool</param>
        /// <returns>Tile</returns>
        public virtual Tile FindTile(int level, int tx, int ty, bool includeUnusedCache, bool done)
        {
            Tile tile = cache.FindTile(m_id, level, tx, ty, includeUnusedCache);
            if (done && tile != null && !tile.Tasker.IsDone())
            {
                tile = null;
            }
            return tile;
        }
        public virtual void onCreateTile(int level, int tx, int ty, List<TileStorage.Slot> slots)
        {
            if (layers == null) return;
            foreach (TileLayer item in layers)
            {
                item.CreateTile(level, tx, ty, slots);
            }
        }
        public virtual CreateTileTask CreateTile(int level, int tx, int ty, List<TileStorage.Slot> slot)
        {
            return new CreateTileTask(this, level, tx, ty, slot);
        }

        //todo missing methods i think need double check
        #endregion
    }
}
