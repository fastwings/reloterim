﻿using System.Collections.Generic;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    /// <summary>
    /// A shared storage to store tiles of the same kind. This abstract class defines
    ///  the behavior of tile storages but does not provide any storage itself. The
    ///  slots managed by a tile storage can be used to store any tile identified by
    ///  its (level,tx,ty) coordinates. This means that a TileStorage::Slot can store
    ///  the data of some tile at some moment, and then be reused to store the data of
    ///  tile some time later. The mapping between tiles and TileStorage::Slot is not
    ///  managed by the TileStorage itself, but by a TileCache. A TileStorage just
    ///  keeps track of which slots in the pool are currently associated with a
    ///  tile (i.e., store the data of a tile), and which are not. The first ones are
    /// </summary>
    [RequireComponent(typeof(TileCache))]
    public abstract class TileStorage : MonoBehaviour
    {
        /// <summary>
        ///  A slot managed by a TileStorage. Concrete sub classes of this class must provide a reference to the actual tile data.
        /// </summary>
        public abstract class Slot
        {
            public TileStorage Owner { get; private set; }
            public Slot(TileStorage owner)
            {
                Owner = owner;
            }
            /// <summary>
            /// override this if the slot needs to release data on destroy
            /// </summary>
            public virtual void Release() { }
        }

        /// <summary>
        /// The size of each tile. For tiles made of raster data, this size is the
        /// tile width in pixels (the tile height is supposed equal to the tile width).
        /// </summary>
        [SerializeField]
        int m_tileSize;

        /// <summary>
        /// queue of the slots
        /// </summary>
        Slot[] m_allSlots;
        /// <summary>
        /// the current free slots
        /// </summary>
        LinkedList<Slot> m_freeSlots;
        /// <summary>
        /// The total number of slots managed by this TileStorage. This includes both unused and used tiles.
        /// </summary>
        int m_capacity;
        /// <summary>
        /// on Awake will generate the storage space
        /// </summary>
        protected virtual void Awake()
        {
            m_capacity = GetComponent<TileCache>().Capacity;
            m_freeSlots = new LinkedList<Slot>();
            m_allSlots = new Slot[m_capacity];
        }
        /// <summary>
        /// on event OnDestroy will Release all the slots in space
        /// </summary>
        public void OnDestroy()
        {
            foreach (var item in m_allSlots)
            {
                item.Release();
            }
        }

        protected void AddSlot(int i, Slot slot)
        {
            m_allSlots[i] = slot;
            m_freeSlots.AddLast(slot);
        }
        /// <summary>
        /// Returns a free slot in the pool of slots managed by this TileStorage.
        /// return a free slot, or NULL if all tiles are currently allocated. 
        /// The returned slot is then considered to be allocated, until it is released with deleteSlot.
        /// </summary>
        /// <returns>Slot</returns>
        public Slot NewSlot()
        {
            if (m_freeSlots.Count != 0)
            {
                Slot s = m_freeSlots.First.Value;
                m_freeSlots.RemoveFirst();
                return s;
            }
            return null;
        }
        /// <summary>
        /// Notifies this storage that the given slot is free.
        /// The given slot can then be allocated to store a new tile, i.e., it can be returned by a subsequent call to newSlot.
        /// </summary>
        /// <param name="s">Slot</param>
        public void ReleaseSlot(Slot s)
        {
            m_freeSlots.AddLast(s);
        }
        /// <summary>
        /// Returns the size of each tile. 
        /// For tiles made of raster data, this size is the tile width in pixels (the tile height is supposed equal to the tile width).
        /// </summary>
        public int TileSize
        {
            get
            {
                return m_tileSize;
            }
        }
        /// <summary>
        /// Returns the total number of slots managed by this TileStorage. This includes both unused and used tiles.
        /// </summary>
        public int Capacity
        {
            get
            {
                return m_capacity;
            }
        }
        /// <summary>
        /// Returns the number of slots in this TileStorage that are currently unused.
        /// </summary>
        public int FreeSlots
        {
            get
            {
                return m_freeSlots.Count;
            }
        }
    }
}
