﻿using System.Collections.Generic;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using ReloteRim.Generator.Planet.Core.Utilities;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    public class TileSampler : NodeCore
    {
        /// <summary>
        /// class used to sort a TileSampler based on its priority
        /// </summary>
        public class Sort : IComparer<TileSampler>
        {
            int IComparer<TileSampler>.Compare(TileSampler a, TileSampler b)
            {
                if (a.Priority > b.Priority)
                    return 1;
                if (a.Priority < b.Priority)
                    return -1;
                else
                    return 0;
            }
        }
        /// <summary>
        /// The terrain node associated with this sampler
        /// </summary>
        [SerializeField]
        GameObject m_terrainNodeObject;
        TerrainNode m_terrinNode;


        /// <summary>
        /// True to store texture tiles for leaf quads.
        /// </summary>
        [SerializeField]
        bool m_storeLeaf;

        /// <summary>
        /// True to store texture tiles for non leaf quads.
        /// </summary>
        [SerializeField]
        bool m_storeParent;

        /// <summary>
        /// True to store texture tiles for invisible quads.
        /// </summary>
        [SerializeField]
        bool m_storeInvisible = false;

        /// <summary>
        /// The order in which to update samplers
        /// </summary>
        [SerializeField]
        int m_priority = -1;

        GPUUniforms m_uniforms;
        QuaternionTree root = null;
        /// <summary>
        /// The producer to be used to create texture tiles for newly created quads.
        /// </summary>
        TileProducer producer;

        TileFilter[] filters;

        #region Unity Events
        protected override void Start()
        {
            base.Start();
            producer = GetComponent<TileProducer>();
            filters = GetComponents<TileFilter>();
            m_terrinNode = m_terrainNodeObject.GetComponent<TerrainNode>();
            m_uniforms = new GPUUniforms(producer.ProduserName);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
        }
        #endregion
        public TileProducer Producer
        {
            get
            {
                return producer;
            }
        }

        public bool StoreLeaf
        {
            get
            {
                return m_storeLeaf;
            }
        }
        public TerrainNode TerrainNode
        {
            get
            {
                return m_terrinNode;
            }
        }
        public int Priority
        {
            get
            {
                return m_priority;
            }
        }
        #region Methods
        public virtual void UpdateSampler()
        {
            if (m_storeInvisible)
                m_terrinNode.SetSplitInvisibleQuads(true);

            //todo build the terrin node class;
        }
        /// <summary>
        /// Returns true if a tile is needed for the given terrain quad.
        /// </summary>
        /// <param name="quad">TerrainQuaternion</param>
        /// <returns>bool</returns>
        protected virtual bool NeedTile(TerrainQuaternion quad)
        {
            bool needTile = m_storeLeaf;
            //if the quad is not a leaf and producer has children 
            //and if have been asked not to store parent then dont need tile
            if (!m_storeParent && !quad.IsLeaf() && producer.HasChildren(quad.Level, quad.TX, quad.TY))
            {
                needTile = false;
            }
            //Check if any of the filters have determined that this tile is not needed
            foreach (TileFilter filter in filters)
            {
                if (filter.DiscardTile(quad))
                {
                    needTile = false;
                    break;
                }
            }
            //if this quad is not visilbe and have not been asked to store invisilbe quads dont need tile
            if (!m_storeInvisible && !quad.IsVisible())
            {
                needTile = false;
            }
            return needTile;
        }
        /// <summary>
        /// 
        /// Updates the internal QuaternionTree to make it identical to the given terrain
        /// TerrainQuaternion. This method releases the texture tiles corresponding to
        /// deleted quads.
        /// </summary>
        /// <param name="tree">QuaternionTree</param>
        /// <param name="quad">TerrainQuaternion</param>

        protected virtual void PutTile(QuaternionTree tree, TerrainQuaternion quad)
        {
            if (tree == null) return;
            //Check if this tile is needed, if not put tile.
            tree.NeedTile = NeedTile(quad);

            if (!tree.NeedTile && tree.Tile != null)
            {
                producer.PutTile(tree.Tile);
                tree.Tile = null;
            }
            //If this QuaternionTree is a leaf then all children of the tree are not needed
            if (quad.IsLeaf() && !tree.IsLeaf())
            {
                tree.RecursiveDeleteChildren(this);
            }
            else
            {
                if (producer.HasChildren(quad.Level, quad.TX, quad.TY))
                {
                    for (int i = 0; i < 4; i++)
                    {
                        PutTile(tree.Children[i], quad.GetChild(i));
                    }
                }
            }
        }
        /// <summary>
        /// Updates the internal quadtree to make it identical to the given terrain
        /// quadtree. Collects the tasks necessary to create the missing texture
        /// tiles, corresponding to newly created quads.
        /// </summary>
        /// <param name="parant">QuaternionTree</param>
        /// <param name="tree">QuaternionTree</param>
        /// <param name="quad">TerrainQuaternion</param>
        protected virtual void GetTiles(QuaternionTree parant, ref QuaternionTree tree, TerrainQuaternion quad)
        {
            //if tree not created, create a new tree and check if its tile is needed
            if (tree == null)
            {
                tree = new QuaternionTree(parant);
                tree.NeedTile = NeedTile(quad);
            }
            //If this trees tile is needed get a tile and add its task to the schedular if the task is not already done
            if (tree.NeedTile && tree.Tile == null)
            {
                tree.Tile = producer.GetTile(quad.Level, quad.TX, quad.TY);
                if (!tree.Tile.Tasker.IsDone())
                {
                    //if task not done schedule task
                    manager.Schedular.Add(tree.Tile.Tasker);
                }
            }
            if (!quad.IsLeaf() && producer.HasChildren(quad.Level, quad.TX, quad.TY))
            {
                for (int i = 0; i < 4; i++)
                {
                    GetTiles(tree, ref tree.Children[i], quad.GetChild(i));
                }
            }
        }

        public void SetTile(MaterialPropertyBlock matPropertyBlock, int level, int tx, int ty)
        {
            if (!producer.IsProduserActiveGPU) return;
            RenderTexture tex = null;
            Vector3 coords = Vector3.zero, size = Vector3.zero;
            SetTile(ref tex, ref coords, ref size, level, tx, ty);

            matPropertyBlock.SetTexture(m_uniforms.Tile, tex);
            matPropertyBlock.SetVector(m_uniforms.Coords, coords);
            matPropertyBlock.SetVector(m_uniforms.Size, size);
        }

        public void SetTile(Material mat, int level, int tx, int ty)
        {
            if (!producer.IsProduserActiveGPU) return;
            RenderTexture tex = null;
            Vector3 coords = Vector3.zero, size = Vector3.zero;
            SetTile(ref tex, ref coords, ref size, level, tx, ty);

            mat.SetTexture(m_uniforms.Tile, tex);
            mat.SetVector(m_uniforms.Coords, coords);
            mat.SetVector(m_uniforms.Size, size);
        }

        /// <summary>
        /// Sets the uniforms necessary to access the texture tile for
        /// the given quad. The samplers producer must be using a GPUTileStorage at the first slot
        /// for this function to work
        /// </summary>
        /// <param name="tex"></param>
        /// <param name="coord"></param>
        /// <param name="size"></param>
        /// <param name="level"></param>
        /// <param name="tx"></param>
        /// <param name="ty"></param>
        private void SetTile(ref RenderTexture tex, ref Vector3 coord, ref Vector3 size, int level, int tx, int ty)
        {
            if (!producer.IsProduserActiveGPU) return;
            Tile t = null;
            int b = producer.GetBorder();
            int s = producer.GetCache().GetStorage(0).TileSize;
            float dx = 0;
            float dy = 0;
            float dd = 1;
            float ds0 = (s / 2) * 2.0f - 2.0f * b;
            float ds = ds0;
            while (!producer.HasTile(level, tx, ty))
            {
                dx += (tx % 2) * dd;
                dy += (ty % 2) * dd;
                dd *= 2;
                ds /= 2;
                level -= 1;
                tx /= 2;
                ty /= 2;

                if (level < 0)
                {
                    Debug.Log("ReloteRim::Generator::Planet::Core::Node::TileSampler::SetTile - invalid level");
                    return;
                }
            }
            QuaternionTree tt = root;
            QuaternionTree tc;
            int tl = 0;

            while (tl != level &&
                   (tc = tt.Children[((tx >> (level - tl - 1)) & 1) | ((ty >> (level - tl - 1)) & 1) << 1]) != null)
            {
                tl += 1;
                tt = tc;
            }

            while (level > tl)
            {
                dx += (tx % 2) * dd;
                dy += (ty % 2) * dd;
                dd *= 2;
                ds /= 2;
                level -= 1;
                tx /= 2;
                ty /= 2;
            }
            t = tt.Tile;
            while (t == null)
            {
                dx += (tx % 2) * dd;
                dy += (ty % 2) * dd;
                dd *= 2;
                ds /= 2;
                level -= 1;
                tx /= 2;
                ty /= 2;
                tt = tt.Parent;

                if (tt == null)
                {
                    Debug.Log("ReloteRim::Generator::Planet::Core::Node::TileSampler::SetTile - null tile");
                    return;
                }

                t = tt.Tile;
            }
            dx = dx * ((s / 2) * 2 - 2 * b) / dd;
            dy = dy * ((s / 2) * 2 - 2 * b) / dd;

            GPUSlot gpuSlot = t.GetSlot(0) as GPUSlot;
            if (gpuSlot == null)
            {
                Debug.Log("ReloteRim::Generator::Planet::Core::Node::TileSampler::SetTile - gpuSlot is null");
                return;
            }
            float w = gpuSlot.Texture.width;
            float h = gpuSlot.Texture.height;
            Vector4 coords;
            if (s % 2 == 0)
            {
                coords = new Vector4((dx + b) / w, (dy + b) / h, 0.0f, ds / w);
            }
            else
            {
                coords = new Vector4((dx + b + 0.5f) / w, (dy + b + 0.5f) / h, 0.0f, ds / w);
            }


            tex = gpuSlot.Texture;
            coord = new Vector3(coords.x, coords.y, coords.z);
            size = new Vector3(coords.w, coords.w, (s / 2) * 2.0f - 2.0f * b);

        }

        #endregion

    }
}
