﻿using ReloteRim.Generator.Planet.Core.Utilities;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Node.Tile
{
    /// <summary>
    /// A filter to decide whether a texture tile must be produced or not for a given quad.
    /// </summary>
    public abstract class TileFilter : MonoBehaviour
    {
        public abstract bool DiscardTile(TerrainQuaternion q);
    }
}
