﻿using System;
using System.Collections.Generic;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Common.Utilities.Misc;
using ReloteRim.Common.Utilities.Tasks;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using ReloteRim.Generator.Planet.Core.Utilities;
using ReloteRim.Generator.Planet.Core.Views;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core
{
    public enum DEFORM { PLANE, SPHERE };

    /// <summary>
    ///  A manger to organize what order update functions are called, the running of tasks and the drawing of the terrain.
    ///  Provides a location for common settings and allows the nodes to access each other.
    ///  Also sets uniforms that are considered global.
    ///  Must have a scheduler script attached to the same game object
    /// </summary>
    public class Manager : MonoBehaviour
    {
        [SerializeField]
        private ComputeShader m_writeData;
        [SerializeField]
        private ComputeShader m_readData;
        [SerializeField]
        private int m_gridResolution = 25;
        [SerializeField]
        private float m_HDRExposure = 0.2f;
        //If the world is a flat plane or a sphere
        [SerializeField]
        private DEFORM m_deformType = DEFORM.PLANE;
        [SerializeField]
        private float m_radius = 6360000.0f;

        private Schedular m_schedular;
        private Controller m_controller;
        private Mesh m_quadMesh;
        private MaterialPropertyBlock m_propertyBlock;
        private Vector3 m_origin;
        private List<TileSampler> m_samplers;


        #region Nodes

        private TerrainNode[] m_terrainNodes;
        private SkyNode m_skyNode;
        private SunNode m_sunNode;
        private OceanNode m_oceanNode;
        private PlantNode m_plantNode;

        public SunNode SunNode
        {
            get { return m_sunNode; }
        }

        public SkyNode SkyNode
        {
            get { return m_skyNode; }
        }

        public OceanNode OceanNode
        {
            get { return m_oceanNode; }
        }

        public PlantNode PlantNode
        {
            get { return m_plantNode; }
        }

        #endregion

        public Controller GetController()
        {
            return m_controller;
        }

        public bool IsDeform
        {
            get { return (m_deformType == DEFORM.SPHERE); }
        }

        public float Radius
        {
            get { return m_radius; }
        }

        public ComputeShader WriteData
        {
            get { return m_writeData; }
        }

        public ComputeShader ReadData
        {
            get { return m_readData; }
        }

        public Schedular Schedular
        {
            get { return m_schedular; }
        }
        /// <summary>
        /// Use this for initialization
        /// </summary>
        private void Awake()
        {
            if (IsDeform)
            {
                m_origin = Vector3.zero;
            }
            else
            {
                m_origin = new Vector3(0.0f, 0.0f, m_radius);
            }
            m_schedular = GetComponent<Schedular>();
            m_controller = GetComponentInChildren<Controller>();

            //if planet view is being use set the radius
            if (m_controller.GetView() is PlanetView)
                ((PlanetView)m_controller.GetView()).SetRadius(m_radius);

            m_oceanNode = GetComponentInChildren<OceanNode>();
            m_skyNode = GetComponentInChildren<SkyNode>();
            m_sunNode = GetComponentInChildren<SunNode>();
            m_terrainNodes = GetComponentsInChildren<TerrainNode>();
            m_samplers = new List<TileSampler>(GetComponentsInChildren<TileSampler>());
            m_samplers.Sort(new TileSampler.Sort());
            m_propertyBlock = new MaterialPropertyBlock();
            //make the mesh used to draw the terrain quads
            m_quadMesh = MeshFactory.MakePlane(m_gridResolution, m_gridResolution);
            m_quadMesh.bounds = new Bounds(Vector3.zero, new Vector3(1e8f, 1e8f, 1e8f));


        }

        private void Update()
        {
            //Update the sky, sun and controller. These node are presumed to always be present
            m_controller.UpdateController();
            m_sunNode.UpdateNode();
            m_skyNode.UpdateNode();

            //Uppdate ocean if used
            if (m_oceanNode != null)
            {
                m_oceanNode.UpdateNode();
            }
            //Update all the terrain nodes used and active
            foreach (TerrainNode node in m_terrainNodes)
            {
                if (node.gameObject.activeInHierarchy)
                {
                    node.UpdateNode();
                }
            }


            //Update the plant node if used
            if (m_plantNode != null)
            {
                m_plantNode.UpdateNode();
            }

            //Update all the samplers used and active
            foreach (TileSampler sampler in m_samplers)
            {
                if (sampler.gameObject.activeInHierarchy)
                {
                    sampler.UpdateSampler();
                }
            }
            m_schedular.Run(); //Run any tasks generated by updating the samplers

            //Draw the terrain quads of each terrain node if active
            foreach (TerrainNode node in m_terrainNodes)
            {
                if (node.gameObject.activeInHierarchy)
                {
                    DrawTerrain(node);
                }
            }

        }

        private void DrawTerrain(TerrainNode node)
        {
            //Get all the samplers attached to the terrain node. The samples contain the data need to draw the quad
            TileSampler[] allSamplers = node.transform.GetComponentsInChildren<TileSampler>();
            List<TileSampler> samplers = new List<TileSampler>();
            //check if sampler is enabled
            foreach (TileSampler sampler in allSamplers)
            {
                if (sampler.enabled && sampler.StoreLeaf)
                {
                    samplers.Add(sampler);
                }
            }
            if (samplers.Count == 0) return;
            //Find all the quads in the terrain node that need to be drawn
            FindDrawableQuads(node.Root, samplers);
            //The draw them
            DrawQuad(node, node.Root, samplers);
        }

        private void DrawQuad(TerrainNode node, TerrainQuaternion quad, List<TileSampler> samplers)
        {
            if (!quad.IsVisible())
                return;
            if (!quad.Drawable)
                return;
            if (!quad.IsLeaf())
            {
                m_propertyBlock.Clear();
                for (int i = 0; i < samplers.Count; ++i)
                {
                    samplers[i].SetTile(m_propertyBlock, quad.Level, quad.TX, quad.TY);
                }
                node.SetPerQuadUniforms(quad, m_propertyBlock);
                Graphics.DrawMesh(m_quadMesh, Matrix4x4.identity, node.Material, 0, Camera.main, 0, m_propertyBlock);
            }
            else
            {
                //draw quads in a order based on distance to camera
                int[] order = new int[4];
                double ox = node.LocalCamearaPostion.X;
                double oy = node.LocalCamearaPostion.Y;
                double cx = quad.OX + quad.Level / 2.0;
                double cy = quad.OY + quad.Level / 2.0;

                if (oy < cy)
                {
                    if (ox < cx)
                    {
                        order[0] = 0;
                        order[1] = 1;
                        order[2] = 2;
                        order[3] = 3;
                    }
                    else
                    {
                        order[0] = 1;
                        order[1] = 0;
                        order[2] = 3;
                        order[3] = 2;
                    }
                }
                else
                {
                    if (ox < cx)
                    {
                        order[0] = 2;
                        order[1] = 0;
                        order[2] = 3;
                        order[3] = 1;
                    }
                    else
                    {
                        order[0] = 3;
                        order[1] = 1;
                        order[2] = 2;
                        order[3] = 0;
                    }
                }
                int done = 0;
                for (int i = 0; i < 4; i++)
                {
                    if (quad.GetChild(order[i]).Visible == Frustum.VISIBILTY.INVISIBLE)
                    {
                        done |= (1 << order[i]);
                    }
                    else if (quad.GetChild(order[i]).Drawable)
                    {
                        DrawQuad(node, quad.GetChild(order[i]), samplers);
                        done |= (1 << order[i]);
                    }
                }
                if (done < 15)
                {
                    //If the a leaf quad needs to be drawn but its tiles are not ready then this 
                    //will draw the next parent tile instead that is ready.
                    //Because of the current set up all tiles always have there tasks run on the frame they are generated
                    //so this section of code is never reached
                    m_propertyBlock.Clear();
                    for (int i = 0; i < samplers.Count; ++i)
                    {
                        samplers[i].SetTile(m_propertyBlock, quad.Level, quad.TX, quad.TY);
                    }

                    //Set the uniforms unique to each quad
                    node.SetPerQuadUniforms(quad, m_propertyBlock);
                    //TODO - use mesh of appropriate resolution for non-leaf quads
                    Graphics.DrawMesh(m_quadMesh, Matrix4x4.identity, node.Material, 0, Camera.main, 0, m_propertyBlock);
                }
            }
        }


        private void FindDrawableQuads(TerrainQuaternion quad, List<TileSampler> samplers)
        {
            quad.Drawable = false;
            if (!quad.IsVisible())
            {
                quad.Drawable = true;
                return;
            }
            if (quad.IsLeaf())
            {
                for (int i = 0; i < samplers.Count; ++i)
                {
                    TileProducer p = samplers[i].Producer;
                    int l = quad.Level;
                    int tx = quad.TX;
                    int ty = quad.TY;
                    if (p.HasTile(l, tx, ty) && p.FindTile(l, tx, ty, false, true) == null)
                    {
                        return;
                    }
                }
            }
            else
            {
                int nDrawable = 0;
                for (int i = 0; i < 4; i++)
                {
                    FindDrawableQuads(quad.GetChild(i), samplers);
                    if (quad.GetChild(i).Drawable)
                    {
                        ++nDrawable;
                    }
                }
                if (nDrawable < 4)
                {
                    for (int i = 0; i < samplers.Count; ++i)
                    {
                        TileProducer p = samplers[i].Producer;

                        int l = quad.Level;
                        int tx = quad.TX;
                        int ty = quad.TY;
                        if (p.HasTile(l, tx, ty) && p.FindTile(l, tx, ty, false, true) == null)
                        {
                            return;
                        }
                    }
                }
            }

            quad.Drawable = true;
        }

        public void SetUniforms(Material mat)
        {
            //Sets uniforms that this or other gameobjects may need
            if (mat == null) return;

            mat.SetMatrix("_Globals_WorldToCamera", m_controller.GetView().WorldToCameraMatrix.ToMatrix4x4());
            mat.SetMatrix("_Globals_CameraToWorld", m_controller.GetView().CameraToWorldMatrix.ToMatrix4x4());
            mat.SetMatrix("_Globals_CameraToScreen", m_controller.GetView().CameraToScreenMatrix.ToMatrix4x4());
            mat.SetMatrix("_Globals_ScreenToCamera", m_controller.GetView().ScreenToCameraMatrix.ToMatrix4x4());
            mat.SetVector("_Globals_WorldCameraPos", m_controller.GetView().WorldCameraPosition.Vector);
            mat.SetVector("_Globals_Origin", m_origin);
            mat.SetFloat("_Exposure", m_HDRExposure);

        }
    }
}
