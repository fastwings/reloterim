﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Utilities;

namespace ReloteRim.Generator.Planet.Core.Views
{
    public class PlanetView : TerrainView
    {
        //The radius of the planet at sea level.This is the size value of the terrain nodes
        double m_radius = 6360000.0;

        public void SetRadius(double radius)
        {
            m_radius = radius;
        }

        public override double GetHeight()
        {
            return m_worldPos.Magnitude() - m_radius; ;
        }
        /// <summary>
        /// returns the position the camera is currently looking at
        /// </summary>
        /// <returns>Vector3d</returns>
        public override Vector3d GetLookAtPosition()
        {
            double co = Math.Cos(m_position.X); // X => longitude
            double so = Math.Sin(m_position.X);
            double ca = Math.Cos(m_position.Y); // Y => latitude
            double sa = Math.Sin(m_position.Y);
            return new Vector3d(co * ca, so * sa, sa) * m_radius;
        }
        /// <summary>
        /// Any contraints you need on the position are applied here
        /// </summary>
        public override void Constrain()
        {
            m_position.Y = Math.Max(-Math.PI / 2.0, Math.Min(Math.PI / 2.0, m_position.Y));
            m_position.Theta = Math.Max(0.1, Math.Max(Math.PI, m_position.Theta));
            m_position.Distance = Math.Max(0.1, m_position.Distance);
        }

        protected override void Start()
        {
            base.Start();
            Constrain();
        }

        protected override void SetWorldToCameraMatrix()
        {
            double co = Math.Cos(m_position.X); // X => longitude
            double so = Math.Sin(m_position.X);
            double ca = Math.Cos(m_position.Y); // Y => latitude
            double sa = Math.Sin(m_position.Y);
            Vector3d po = GetLookAtPosition();
            Vector3d px = new Vector3d(-so, co, 0.0);
            Vector3d py = new Vector3d(-co * sa, -so * sa, ca);
            Vector3d pz = new Vector3d(co * ca, so * ca, sa);


            double ct = Math.Cos(m_position.Theta);
            double st = Math.Sin(m_position.Theta);
            double cp = Math.Cos(m_position.Phi);
            double sp = Math.Sin(m_position.Phi);

            Vector3d cx = px * cp + py * sp;
            Vector3d cy = (px * -1.0) * sp * ct + py * cp * ct + pz * st;
            Vector3d cz = px * sp * st + py * cp * st + pz * ct;
            m_worldPos = po + cz * m_position.Distance;

            if (m_worldPos.Magnitude() < m_radius + 10 + GroundHeight)
            {
                m_worldPos = m_worldPos.Normalized(m_radius + 10 + GroundHeight);
            }

            Matrix4d view = new Matrix4d(cx.X, cx.Y, cx.Z, 0.0,
                                            cy.X, cy.Y, cy.Z, 0.0,
                                            cz.X, cz.Y, cz.Z, 0.0,
                                            0.0, 0.0, 0.0, 1.0);

            CameraToWorldMatrix = view * Matrix4d.Translate(m_worldPos * -1.0);
            //Flip first row to match Unitys winding order.
            CameraToWorldMatrix.Matrix[0, 0] *= -1.0;
            CameraToWorldMatrix.Matrix[0, 1] *= -1.0;
            CameraToWorldMatrix.Matrix[0, 2] *= -1.0;
            CameraToWorldMatrix.Matrix[0, 3] *= -1.0;
            CameraToWorldMatrix = CameraToWorldMatrix.Inverse();
            ActiveCamera.worldToCameraMatrix = CameraToWorldMatrix.ToMatrix4x4();
            ActiveCamera.transform.position = m_worldPos.Vector;
        }

        public override void Move(Vector3d old_position, Vector3d position, double speed)
        {
            Vector3d oldpos = old_position.Normalized();
            Vector3d pos = position.Normalized();
            double oldLat = MathUtility.SafeASin(oldpos.Z);
            double oldLon = Math.Atan2(oldpos.Y, oldpos.X);
            double lat = MathUtility.SafeASin(pos.Z);
            double lon = Math.Atan2(pos.Y, pos.X);

            m_position.X -= (lon - oldLon) * speed * Math.Max(1.0, GetHeight());
            m_position.Y -= (lat - oldLat) * speed * Math.Max(1.0, GetHeight());
        }

        public override void MoveForward(double distance)
        {
            double co = Math.Cos(m_position.X); // X => longitude
            double so = Math.Sin(m_position.X);
            double ca = Math.Cos(m_position.Y); // Y => latitude
            double sa = Math.Sin(m_position.Y);
            Vector3d po = GetLookAtPosition();
            Vector3d px = new Vector3d(-so, co, 0.0);
            Vector3d py = new Vector3d(-co * sa, -so * sa, ca);
            Vector3d pz = new Vector3d(co * ca, so * ca, sa);
            Vector3d pd =
                (po - px * Math.Sin(m_position.Phi) * distance + py * Math.Cos(m_position.Phi) * distance).Normalized();
            m_position.X = Math.Atan2(pd.Y, pd.X);
            m_position.Y = MathUtility.SafeASin(pd.Z);
        }

        public override void Turn(double angle)
        {
            m_position.Phi += angle;
        }

        public override double Interpolate(double sx0, double sy0, double stheta, double sphi, double sd, double dx0, double dy0, double dtheta,
            double dphi, double dd, double t)
        {
            Vector3d s = new Vector3d(Math.Cos(sx0) * Math.Cos(sy0), Math.Sin(sx0) * Math.Cos(sy0), Math.Sin(sy0));
            Vector3d e = new Vector3d(Math.Cos(dx0) * Math.Cos(dy0), Math.Sin(dx0) * Math.Cos(dy0), Math.Sin(dy0));
            double dist = Math.Max(MathUtility.SafeAcos(s.Dot(e)) * m_radius, 1e-3);
            t = Math.Min(t + Math.Min(0.1, 5000.0 / dist), 1.0);
            double T = 0.5 * Math.Atan(4.0 * (t - 0.5)) / Math.Atan(4.0 * 0.5) + 0.5;
            var lon = m_position.X;
            var lat = m_position.Y;
            var theta = m_position.Theta;
            var phi = m_position.Phi;
            InterpolateDirection(sx0, sy0, dx0, dy0, T, ref lon, ref  lat);
            InterpolateDirection(sphi, stheta, dphi, dtheta, T, ref phi, ref theta);
            double w = 10.0;

            m_position.Distance = sd * (1.0 - t) + dd * t + dist * (Math.Exp(-w * (t - 0.5) * (t - 0.5)) - Math.Exp(-w * 0.25));
            return t;
        }

        public override void InterpolatePosition(double sx0, double sy0, double dx0, double dy0, double t, ref double x0, ref double y0)
        {
            InterpolateDirection(sx0, sy0, dx0, dy0, t, ref x0, ref y0);
        }
    }
}
