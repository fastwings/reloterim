﻿using System.Collections.Generic;
using ReloteRim.Common.Utilities.Tasks;
using ReloteRim.Generator.Planet.Core.Node.Tile;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Tasks
{
    /// <summary>
    /// The task that creates the tiles. The task calls the producers DoCreateTile function
    /// and the data created is stored in the slot.
    /// </summary>
    public class CreateTileTask : Task
    {
        /// <summary>
        /// The TileProducer that created this task.
        /// </summary>
        TileProducer owner;
        /// <summary>
        /// The level of the tile to create.
        /// </summary>
        int level;
        /// <summary>
        /// QuaternionTree axis x coordinate of the tile to create.
        /// </summary>
        int tx;
        /// <summary>
        /// QuaternionTree axis y coordinate of the tile to create.
        /// </summary>
        int ty;
        /// <summary>
        /// Where the created tile data must be stored
        /// </summary>
        List<TileStorage.Slot> slots;
        public CreateTileTask(TileProducer owner, int level, int tx, int ty, List<TileStorage.Slot> slots)
        {
            this.owner = owner;
            this.level = level;
            this.tx = tx;
            this.ty = ty;
            this.slots = slots;
        }
        public List<TileStorage.Slot> Slots
        {
            get
            {
                return slots;
            }
        }
        public override void Run()
        {
            if (IsDone())
            {
                Debug.Log("ReloteRim::Generator::Planet::Producer::Core::Tasks::CreateTileTask::Run - task has already been run, task not run");
                return;
            }
            owner.CreateTile(level, tx, ty, slots);
            SetDone(true);
        }

        public override string ToString()
        {
            return "[CreateTileTask, name: " + owner.name + " ,level: " + level + ", tx: " + tx + ", ty: " + ty + "]";
        }
    }
}
