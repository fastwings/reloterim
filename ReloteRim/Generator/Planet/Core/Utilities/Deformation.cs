﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    /// <summary>
    /// 
    /// A deformation of space. Such a deformation maps a 3D source point to a 3D
    /// destination point. The source space is called the local space, while
    /// the destination space is called the deformed space. Source and
    /// destination points are defined with their x,y,z coordinates in an orthonormal
    /// reference frame. A Deformation is also responsible to set the shader uniforms
    /// that are necessary to project a TerrainQuad on screen, taking the deformation
    /// into account. The default implementation of this class implements the
    /// identity deformation, i.e. the deformed point is equal to the local one.
    /// </summary>
    public class Deformation
    {
        public class Uniforms
        {
            public int Blending { get; set; }
            public int LocalToWorld { get; set; }
            public int LocalToScreen { get; set; }
            public int Offset { get; set; }
            public int Camera { get; set; }
            public int ScreenQuadCorners { get; set; }
            public int ScreenQuadVerticals { get; set; }
            public int Raduis { get; set; }
            public int ScreemQuasCornerNorms { get; set; }
            public int TangentFrameToWorld { get; set; }
            public int TileToTangent { get; set; }
            public Uniforms()
            {

                Blending = Shader.PropertyToID("_Deform_Blending");
                LocalToWorld = Shader.PropertyToID("_Deform_LocalToWorld");
                LocalToScreen = Shader.PropertyToID("_Deform_LocalToScreen");
                Offset = Shader.PropertyToID("_Deform_Offset");
                Camera = Shader.PropertyToID("_Deform_Camera");
                ScreenQuadCorners = Shader.PropertyToID("_Deform_ScreenQuadCorners");
                ScreenQuadVerticals = Shader.PropertyToID("_Deform_ScreenQuadVerticals");
                Raduis = Shader.PropertyToID("_Deform_Radius");
                ScreemQuasCornerNorms = Shader.PropertyToID("_Deform_ScreenQuadCornerNorms");
                TangentFrameToWorld = Shader.PropertyToID("_Deform_TangentFrameToWorld");
                TileToTangent = Shader.PropertyToID("_Deform_TileToTangent");
            }
        }

        public Uniforms m_uniforms;
        public Matrix4d m_localToCamera;
        public Matrix4d m_localToScreen;
        public Matrix3d m_localToTangent;
        public Deformation()
        {
            m_uniforms = new Uniforms();
            m_localToCamera = new Matrix4d();
            m_localToScreen = new Matrix4d();
            m_localToTangent = new Matrix3d();
        }
        public virtual Vector3d LocalToDeformed(Vector3d localPt)
        {
            return localPt;
        }
        public virtual Matrix4d LocalToDeformedDifferential(Vector3d locatPt, bool clamp = false)
        {
            return Matrix4d.Translate(new Vector3d(locatPt.X, locatPt.Y, 0.0));
        }
        public virtual Vector3d DeformedToLocal(Vector3d deformedPt)
        {
            return deformedPt;
        }
        /// <summary>
        /// 
        /// Returns the local bounding box corresponding to the given source disk.
        ///
        /// param deformedPt the source disk center in deformed space.
        /// param deformedRadius the source disk radius in deformed space.
        /// return the local bounding box corresponding to the given source disk.
        /// </summary>
        /// <param name="deformedCenter">Vector3d</param>
        /// <param name="deformedRadius">double</param>
        /// <returns>Box2d</returns>
        public virtual Box2d DeformedToLocalBounds(Vector3d deformedCenter, double deformedRadius)
        {
            return new Box2d(deformedCenter.X - deformedRadius, deformedCenter.X + deformedRadius,
                             deformedCenter.Y - deformedRadius, deformedCenter.Y + deformedRadius);
        }
        /// <summary>
        ///  Returns an orthonormal reference frame of the tangent space at the given
        /// deformed point. This reference frame is such that its xy plane is the
        /// tangent plane, at deformedPt, to the deformed surface corresponding to
        /// the local plane z=0. Note that this orthonormal reference frame does
        /// not give the differential of the inverse deformation funtion,
        /// which in general is not an orthonormal transformation. If p is a deformed
        /// point, then deformedToLocalFrame(deformedPt) * p gives the coordinates of
        /// p in the orthonormal reference frame defined above.
        ///
        /// param deformedPt a point in the deformed (i.e., destination) space.
        /// return the orthonormal reference frame at deformedPt defined above.
        /// </summary>
        /// <param name="deformedPt">Vector3d</param>
        /// <returns>Matrix4d</returns>
        public virtual Matrix4d DeformedToTangentFrame(Vector3d deformedPt)
        {
            return Matrix4d.Translate(new Vector3d(-deformedPt.X, -deformedPt.Y, 0.0d));
        }
        /// <summary>
        /// Returns the distance in local (i.e., source) space between a point and a bounding box.
        /// </summary>
        /// <param name="localPt"></param>
        /// <param name="localBox"></param>
        /// <returns></returns>
        public virtual double GetLocalDist(Vector3d localPt, Box3d localBox)
        {
            return Math.Max(Math.Abs(localPt.Z - localBox.ZMax),
                Math.Max(Math.Min(Math.Abs(localPt.X - localBox.XMin), Math.Abs(localPt.X - localBox.XMax)),
                Math.Min(Math.Abs(localPt.Y - localBox.YMin), Math.Abs(localPt.Y - localBox.YMax))));
        }
        /// <summary>
        /// 
        /// Returns the visibility of a bounding box in local space, in a view
        /// frustum defined in deformed space.
        ///
        /// param node a TerrainNode. This is node is used to get the camera position
        /// in local and deformed space with TerrainNode::GetLocalCamera and
        /// TerrainNode::GetDeformedCamera, as well as the view frustum planes
        /// in deformed space with TerrainNode::GetDeformedFrustumPlanes.
        /// param localBox a bounding box in local space.
        /// return the visibility of the bounding box in the view frustum.
        /// </summary>
        /// <param name="node">TerrainNode</param>
        /// <param name="localBox">Box3d</param>
        /// <returns>Frustum.VISIBILTY</returns>
        public virtual Frustum.VISIBILTY GetVisibility(TerrainNode node, Box3d localBox)
        {
            return Frustum.GetVisibility(node.DeformedFrustumPlanes, localBox);
        }
        /// <summary>
        /// 
        /// Sets the shader uniforms that are necessary to project on screen the
        /// TerrainQuad of the given TerrainNode. This method can set the uniforms
        /// that are common to all the quads of the given terrain.
        /// </summary>
        /// <param name="node">TerrainNode</param>
        /// <param name="mat">Material</param>
        public virtual void SetUniforms(TerrainNode node, Material mat)
        {
            if (mat == null || node == null)
            {
                //we cant do any both need with something
                return;
            }
            float d1 = node.SplitDistance + 1.0f;
            float d2 = 2.0f + node.SplitDistance;
            mat.SetVector(m_uniforms.Blending, new Vector2(d1, d2 - d1));
            m_localToCamera = node.GetView().WorldToCameraMatrix * node.LocalToWorld;
            m_localToScreen = node.GetView().CameraToScreenMatrix * m_localToCamera;
            Vector3d localCameraPostion = node.LocalCamearaPostion;
            Vector3d worldCamera = node.GetView().WorldCameraPosition;

            Matrix4d a = LocalToDeformedDifferential(localCameraPostion);
            Matrix4d b = DeformedToTangentFrame(worldCamera);

            Matrix4d l = b * node.LocalToWorld * a;
            m_localToTangent = new Matrix3d(l.Matrix[0, 0], l.Matrix[0, 1], l.Matrix[0, 3],
                                            l.Matrix[1, 0], l.Matrix[1, 1], l.Matrix[1, 3],
                                            l.Matrix[3, 0], l.Matrix[3, 1], l.Matrix[3, 3]);
            mat.SetMatrix(m_uniforms.LocalToScreen, m_localToScreen.ToMatrix4x4());
            mat.SetMatrix(m_uniforms.LocalToWorld, node.LocalToWorld.ToMatrix4x4());
        }
        /// <summary>
        /// 
        /// Sets the shader uniforms that are necessary to project on screen the
        /// TerrainQuad of the given TerrainNode. This method can set the uniforms
        /// that are common to all the quads of the given terrain.
        /// </summary>
        /// <param name="node">TerrainNode</param>
        /// <param name="quad">TerrainQuaternion</param>
        /// <param name="matPropertyBlock">MaterialPropertyBlock</param>
        public virtual void SetUniforms(TerrainNode node, TerrainQuaternion quad, MaterialPropertyBlock matPropertyBlock)
        {
            if (matPropertyBlock == null || node == null || quad == null)
            {
                //we cant do any both need with something
                return;
            }
            //todo Finsh the File Deformation Class
            double ox = quad.OX;
            double oy = quad.OY;
            double length = quad.Length;
            double distanceFactor = (double)node.DistanceFactor;
            int level = quad.Level;
            matPropertyBlock.AddVector(m_uniforms.Offset, new Vector4((float)ox, (float)oy, (float)length, (float)level));
            Vector3d cameraPos = node.LocalCamearaPostion;
            matPropertyBlock.AddVector(m_uniforms.Camera, new Vector4((float)((cameraPos.X - ox) / length), (float)((cameraPos.Y - oy) / length), (float)((cameraPos.Z - node.GetView().GroundHeight) / (length * distanceFactor))));

            cameraPos = node.LocalCamearaPostion;//we update cameraPos p
            Matrix3d m = m_localToTangent * (new Matrix3d(length, 0.0, ox - cameraPos.X,
                                                            0.0, length, oy - cameraPos.Y,
                                                            0.0, 0.0, 1.0));
            matPropertyBlock.AddMatrix(m_uniforms.TileToTangent, m.ToMatrix4x4());

            SetScreenUniforms(node, quad, matPropertyBlock);
        }
        protected virtual void SetScreenUniforms(TerrainNode node, TerrainQuaternion quad, MaterialPropertyBlock matPropertyBlock)
        {
            double ox = quad.OX;
            double oy = quad.OY;
            double length = quad.Length;



            Vector3d p0 = new Vector3d(ox, oy, 0.0);
            Vector3d p1 = new Vector3d(ox + length, oy, 0.0);
            Vector3d p2 = new Vector3d(ox, oy + length, 0.0);
            Vector3d p3 = new Vector3d(ox + length, oy + length, 0.0);

            Matrix4d corners = new Matrix4d(p0.X, p1.X, p2.X, p3.X,
                                                p0.Y, p1.Y, p2.Y, p3.Y,
                                                p0.Z, p1.Z, p2.Z, p3.Z,
                                                1.0, 1.0, 1.0, 1.0);

            matPropertyBlock.AddMatrix(m_uniforms.ScreenQuadCorners, (m_localToScreen * corners).ToMatrix4x4());

            Matrix4d verticals = new Matrix4d(0.0, 0.0, 0.0, 0.0,
                                                    0.0, 0.0, 0.0, 0.0,
                                                    1.0, 1.0, 1.0, 1.0,
                                                    0.0, 0.0, 0.0, 0.0);

            matPropertyBlock.AddMatrix(m_uniforms.ScreenQuadVerticals, (m_localToScreen * verticals).ToMatrix4x4());
        }

    }
}
