﻿using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    /// <summary>
    ///  A quad in a terrain QuaternionTree. The QuaternionTree is subdivided based only
    ///  on the current viewer position. All quads are subdivided if they
    ///  meet the subdivision criterion, even if they are outside the view
    ///  frustum. The quad visibility is stored in #visible. It can be used
    ///  in TileSampler to decide whether or not data must be produced
    ///  for invisible tiles (we recall that the terrain QuaternionTree itself
    ///  does not store any terrain data).
    /// </summary>
    public class TerrainQuaternion
    {
        /// <summary>
        /// The TerrainNode to which this terrain Quaternion belongs.
        /// </summary>
        TerrainNode m_owner;
        /// <summary>
        /// The parent quad of this Quaternion.
        /// </summary>
        TerrainQuaternion m_parent;
        /// <summary>
        /// The level of this quad in the QuaternionTree (0 for the root).
        /// </summary>
        int m_level;
        /// <summary>
        /// The logical x,y coordinate of this quad (between 0 and 2^level).
        /// </summary>
        int m_tx, m_ty;
        /// <summary>
        /// The physical x,y coordinate of the lower left corner of this quad (in local space).
        /// </summary>
        double m_ox, m_oy;
        /// <summary>
        /// The physical size of this quad (in local space).
        /// </summary>
        double m_length;
        /// <summary>
        /// local bounding box
        /// </summary>
        Box3d m_localBox;
        /// <summary>
        /// Should the quad be drawn
        /// </summary>
        bool m_drawable;
        /// <summary>
        /// The minimum/maximum terrain elevation inside this quad. This field must
        /// be updated manually by users (the TileSampler class can
        /// do this for you).
        /// </summary>
        float m_zmin, m_zmax;
        /// <summary>
        /// 
        /// The visibility of the bounding box of this quad from the current
        /// viewer position. The bounding box is computed using zmin and
        /// zmax, which must therefore be up to date to get a correct culling
        /// of quads out of the view frustum. This visibility only takes frustum
        /// culling into account.
        /// </summary>
        Frustum.VISIBILTY m_visible;
        /// <summary>
        /// The four subquads of this quad. If this quad is not subdivided,
        /// the four values are NULL. The subquads are stored in the
        /// following order: bottomleft, bottomright, topleft, topright.
        /// </summary>
        TerrainQuaternion[] children = new TerrainQuaternion[4];
        /// <summary>
        /// True if the bounding box of this quad is occluded by the bounding
        /// boxes of the quads in front of it.
        /// </summary>
        bool m_occluded = false;
        /// <summary>
        /// Creates a new TerrainQuaternion.
        /// param owner the TerrainNode to which the terrain TerrainQuaternion belongs.
        /// param parent the parent quad of this quad.
        /// param tx the logical x coordinate of this quad.
        /// param ty the logical y coordinate of this quad.
        /// param ox the physical x coordinate of the lower left corner of this quad.
        /// param oy the physical y coordinate of the lower left corner of this quad.
        /// param length the physical size of this quad.
        /// param zmin the minimum %terrain elevation inside this quad.
        /// param zmax the maximum %terrain elevation inside this quad.
        /// </summary>
        /// <param name="owner">TerrainNode</param>
        /// <param name="parent">TerrainQuaternion</param>
        /// <param name="tx">int</param>
        /// <param name="ty">int</param>
        /// <param name="ox">double</param>
        /// <param name="oy">double</param>
        /// <param name="length">double</param>
        /// <param name="zmin">float</param>
        /// <param name="zmax">float</param>
        public TerrainQuaternion(TerrainNode owner, TerrainQuaternion parent, int tx, int ty, double ox, double oy, double length, float zmin, float zmax)
        {
            m_owner = owner;
            m_parent = parent;
            m_tx = tx;
            m_ty = ty;
            m_ox = ox;
            m_oy = oy;
            m_zmax = zmax;
            m_zmin = zmin;
            m_length = length;
            m_localBox = new Box3d(m_ox, m_ox + m_level, m_oy, m_oy + m_length, m_zmin, m_zmax);
        }
        public Frustum.VISIBILTY Visible
        {
            get
            {
                return m_visible;
            }
        }
        public int Level
        {
            get
            {
                return m_level;
            }
        }
        public float ZMax
        {
            get
            {
                return m_zmax;
            }
            set
            {
                m_zmax = value;
            }
        }
        public float ZMin
        {
            get
            {
                return m_zmin;
            }
            set
            {
                m_zmin = value;
            }
        }
        public bool Occluded
        {
            get
            {
                return m_occluded;
            }
        }
        public int TX
        {
            get
            {
                return m_tx;
            }
        }
        public int TY
        {
            get
            {
                return m_ty;
            }
        }
        public double OX
        {
            get
            {
                return m_ox;
            }
        }
        public double OY
        {
            get
            {
                return m_oy;
            }
        }
        public double Length
        {
            get
            {
                return m_length;
            }
        }
        public bool Drawable
        {
            get
            {
                return m_drawable;
            }
            set
            {
                m_drawable = value;
            }
        }
        public TerrainNode Owner
        {
            get
            {
                return m_owner;
            }
        }

        #region Methods
        public TerrainQuaternion GetChild(int i)
        {
            return children[i];
        }
        public bool IsVisible()
        {
            return (m_visible != Frustum.VISIBILTY.INVISIBLE);
        }
        /// <summary>
        /// Returns true if this quad is not subdivided.
        /// </summary>
        /// <returns>bool</returns>
        public bool IsLeaf()
        {
            return (children[0] == null);
        }
        /// <summary>
        /// Returns the number of quads in the tree below this quad.
        /// </summary>
        /// <returns>int</returns>
        public int GetSize()
        {
            int size = 1;
            if (IsLeaf())
            {
                return size;
            }
            else
            {
                return size + children[0].GetSize() + children[1].GetSize() + children[2].GetSize() + children[3].GetSize();
            }
        }
        /// <summary>
        /// Returns the depth of the tree below this quad.
        /// </summary>
        /// <returns>int</returns>
        public int GetDepth()
        {

            if (IsLeaf())
            {
                return m_level;
            }
            else
            {
                return Mathf.Max(Mathf.Max(children[0].GetDepth(), children[1].GetDepth()),
                                Mathf.Max(children[2].GetDepth(), children[3].GetDepth()));
            }
        }

        private void Release()
        {
            for (int i = 0; i < 4; i++)
            {
                if (children[i] != null)
                {
                    children[i].Release();
                    children[i] = null;
                }
            }
        }
        public void Update()
        {
            Frustum.VISIBILTY v = (m_parent == null) ? Frustum.VISIBILTY.PARTIALLY : m_parent.Visible;
            if (v == Frustum.VISIBILTY.PARTIALLY)
            {
                m_visible = m_owner.GetVisiblity(m_localBox);
            }
            else
            {
                m_visible = v;
            }
            // here we reuse the occlusion test from the previous frame:
            // if the quad was found unoccluded in the previous frame, we suppose it is
            // still unoccluded at this frame. If it was found occluded, we perform
            // an occlusion test to check if it is still occluded.
            if (m_visible != Frustum.VISIBILTY.INVISIBLE && m_occluded)
            {
                m_occluded = Owner.IsOccluded(m_localBox);
                if (m_occluded)
                {
                    m_visible = Frustum.VISIBILTY.INVISIBLE;
                }
            }
            double ground = m_owner.GetView().GroundHeight;
            //todo need finsh up tarreinnode class
            //double distance = m_owner.Cam

        }
        #endregion
    }
}
