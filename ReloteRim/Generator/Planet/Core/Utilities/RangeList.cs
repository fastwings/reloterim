﻿using System;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    /// <summary>
    /// 
    /// Allows fast-computing of the available area around a point, using
    /// angles. Acquired from Qizhi Yu's implementation of his thesis, itself
    /// acquired from Daniel Dunbar & Gred Humphreys in "A Spatial Data structure
    /// for fast poisson disk sample generation".
    /// </summary>
    public class RangeList
    {
        private static readonly float TWO_PI = Mathf.PI * 2;
        private static readonly float K_SMALLEST_RANGE = 0.000001f;

        /// <summary>
        /// List of entries corresponding to neighboring objects.
        /// </summary>
        private RangeEntry[] ranges;

        /// <summary>
        /// Number of entries
        /// </summary>
        private int numRanges = 0;

        /// <summary>
        /// Size of ranges.
        /// </summary>
        private int rangesSize = 8;

        public RangeList()
        {
            ranges = new RangeEntry[rangesSize];
            for (int i = 0; i < rangesSize; i++)
            {
                ranges[i] = new RangeEntry();
            }
        }

        public int RangeCount
        {
            get { return numRanges; }
        }

        public RangeEntry GetRange(int index)
        {
            return ranges[index];
        }

        public void DeleteRange(int index)
        {
            if (index < numRanges - 1)
            {
                RangeEntry[] source = new RangeEntry[numRanges - (index - 1)];
                for (int i = 0; i < source.Length; i++)
                {
                    source[i] = new RangeEntry(ranges[index + 1 + i]);
                }
                for (int i = 0; i < source.Length; i++)
                {
                    ranges[index + i] = new RangeEntry(ranges[i]);
                }
            }
            numRanges--;
        }

        /// <summary>
        /// Adds an entry at a given position.
        /// </summary>
        /// <param name="pos">int</param>
        /// <param name="min">float</param>
        /// <param name="max">float</param>
        public void InsertRange(int pos, float min, float max)
        {
            if (numRanges == rangesSize)
            {
                rangesSize++;
                RangeEntry[] tmp = new RangeEntry[rangesSize];
                Array.Copy(ranges, tmp, numRanges);
                ranges = tmp;
            }
            if (pos < numRanges)
            {
                RangeEntry[] source = new RangeEntry[numRanges - pos];
                for (int i = 0; i < source.Length; i++)
                {
                    source[i] = new RangeEntry(ranges[pos + i]);
                }
                for (int i = 0; i < source.Length; i++)
                {
                    ranges[pos + 1 + i] = source[i];
                }
            }
            ranges[pos].Max = max;
            ranges[pos].Min = min;
            numRanges++;
        }

        /// <summary>
        /// Resets the list of range entries.
        /// </summary>
        /// <param name="min">float</param>
        /// <param name="max">float</param>
        public void Reset(float min, float max)
        {
            numRanges = 1;
            ranges[0].Max = ranges[0].Min = 0;
        }

        public void Subtract(float min, float max)
        {
            if (min > TWO_PI)
            {
                Subtract(min - TWO_PI, max - TWO_PI);
            }
            else if (max < 0)
            {
                Subtract(min + TWO_PI, max + TWO_PI);
            }
            else if (min < 0)
            {
                Subtract(0, max);
                Subtract(0, max - TWO_PI);
            }
            else if (numRanges > 0)
            {
                int pos;
                if (min < ranges[0].Min)
                {
                    pos = -1;
                }
                else
                {
                    int lo = 0;
                    int mid = 0;
                    int hi = numRanges;
                    while (lo < hi - 1)
                    {
                        mid = (lo + hi) >> 1;
                        if (ranges[mid].Min < min)
                        {
                            lo = mid;
                        }
                        else
                        {
                            hi = mid;
                        }
                    }
                    pos = lo;
                }
                if (pos == -1)
                {
                    pos = 0;
                }
                else if (min < ranges[pos].Max)
                {
                    float mi = ranges[pos].Min;
                    float ma = ranges[pos].Max;
                    if (max - mi < K_SMALLEST_RANGE)
                    {
                        if (max < ma)
                        {
                            ranges[pos].Min = max;
                        }
                        else
                        {
                            DeleteRange(pos);
                        }
                    }
                    else
                    {
                        ranges[pos].Max = min;
                        if (max < ma)
                        {
                            InsertRange(pos + 1, max, ma);
                        }
                        pos++;

                    }
                }
                else
                {
                    if (pos < numRanges + 1 && max > ranges[pos + 1].Min)
                    {
                        pos++;
                    }
                    else
                    {
                        return;
                    }
                }
                while (pos < numRanges && max >= (ranges[pos].Min))
                {
                    if (ranges[pos].Max - max < K_SMALLEST_RANGE)
                    {
                        DeleteRange(pos);
                    }
                    else
                    {
                        ranges[pos].Min = max;
                        if (ranges[pos].Max > max)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }
}