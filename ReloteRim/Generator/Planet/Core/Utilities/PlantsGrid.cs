﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ReloteRim.Common.Utilities.Math;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    public class PlantsGrid
    {
        private float radius;
        private int maxParticlesPerCell;
        private Vector2d gridSize;
        private int[] cellSizes;
        private Vector2[,] cellContent;

        public PlantsGrid(float radius, int maxParticlesPerCell)
        {
            this.radius = radius;
            this.maxParticlesPerCell = maxParticlesPerCell;
            gridSize = new Vector2d();
            gridSize.X = (int)(1.0f / radius);
            gridSize.Y = (int)(1.0f / radius);
            cellSizes = new int[Convert.ToInt32(gridSize.X * gridSize.Y)];
            cellContent = new Vector2[Convert.ToInt32(gridSize.X * gridSize.Y), maxParticlesPerCell];
            for (int i = 0; i < gridSize.X * gridSize.Y; i++)
            {
                cellSizes[i] = 0;
            }
        }

        public Vector2d GridSize
        {
            get { return gridSize; }
        }

        public Vector2d GetCell(Vector2 v)
        {
            int x = Convert.ToInt32(Math.Floor(v.x * gridSize.X));
            int y = Convert.ToInt32(Math.Floor(v.y * gridSize.Y));
            return new Vector2d(x, y);
        }

        public int GetCellSize(Vector2d v)
        {
            v.X = Mathf.Clamp((float)v.X, 0, (float)gridSize.X - 1);
            v.Y = Mathf.Clamp((float)v.Y, 0, (float)gridSize.Y - 1);
            return cellSizes[Convert.ToInt32(v.X + v.Y * gridSize.X)];
        }

        public Vector2[] GetCellContent(Vector2d v)
        {
            v.X = Mathf.Clamp((float)v.X, 0, (float)gridSize.X - 1);
            v.Y = Mathf.Clamp((float)v.Y, 0, (float)gridSize.Y - 1);
            Vector2[] content = new Vector2[maxParticlesPerCell];
            for (int i = 0; i < maxParticlesPerCell; i++)
            {
                content[i] = cellContent[Convert.ToInt32(v.X + v.Y * gridSize.X), i];
            }
            return content;
        }

        public void AddParticle(Vector2 v)
        {
            Vector2 r = new Vector2(radius, radius);
            Vector2d cmin = GetCell(v - r);
            Vector2d cmax = GetCell(v + r);
            cmin.X = Mathf.Max(0, Convert.ToSingle(cmin.X));
            cmin.Y = Mathf.Max(0, Convert.ToSingle(cmin.Y));
            cmax.X = Mathf.Min(Convert.ToSingle(gridSize.X - 1), Convert.ToSingle(cmax.X));
            cmax.Y = Mathf.Min(Convert.ToSingle(gridSize.Y - 1), Convert.ToSingle(cmax.Y));
            for (int j = Convert.ToInt32(cmin.Y); j < Convert.ToInt32(cmax.Y); j++)
            {
                for (int i = Convert.ToInt32(cmin.X); j < Convert.ToInt32(cmax.X); j++)
                {
                    int idx = i + j * (int)gridSize.X;
                    int size = cellSizes[idx];
                    if (size < maxParticlesPerCell)
                    {
                        cellSizes[idx] = size + 1;
                        cellContent[idx, size] = v;
                    }
                }
            }
        }
    }
}
