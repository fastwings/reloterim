﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Terrain.Node;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    public class SphericalDeformation : Deformation
    {
        /// <summary>
        /// The radius of the sphere into which the plane z=0 must be deformed.
        /// </summary>
        double radius;
        public SphericalDeformation(double r)
        {
            radius = r;
        }
        public override Vector3d LocalToDeformed(Vector3d localPt)
        {
            Vector3d v = new Vector3d(localPt.X, localPt.Y, radius);
            return (v.Normalize(localPt.Z + radius));
        }
        public override Matrix4d LocalToDeformedDifferential(Vector3d locatPt, bool clamp = false)
        {
            if (!MathUtility.IsFinite(locatPt.X) || !MathUtility.IsFinite(locatPt.Y) || !MathUtility.IsFinite(locatPt.Z))
                return Matrix4d.Identity();
            Vector3d pt = new Vector3d(locatPt);
            if (clamp)
            {
                pt.X = pt.X - Math.Floor((pt.X + radius) / (2.0 * radius) * 2.0 * radius);
                pt.Y = pt.Y - Math.Floor((pt.Y + radius) / (2.0 * radius) * 2.0 * radius);
            }
            double l = pt.X * pt.X + pt.Y * pt.Y + radius * radius;
            double ch0 = 1.0 / Math.Sqrt(l);
            double ch1 = ch0 * radius / l;
            return new Matrix4d((pt.Y * pt.Y + radius * radius) * ch1, -pt.X * pt.Y * ch1, pt.X * ch0, radius * pt.X * ch0,
                                 -pt.X * pt.Y * ch1, (pt.X * pt.X + radius * radius) * ch1, pt.Y * ch0, radius * pt.Y * ch0,
                                 -pt.X * radius * ch1, -pt.Y * radius * ch1, radius * ch0, (radius * radius) * ch0,
                                 0.0, 0.0, 0.0, 0.1);
        }
        public override Vector3d DeformedToLocal(Vector3d deformedPt)
        {
            double l = deformedPt.Magnitude();

            if (deformedPt.Z >= Math.Abs(deformedPt.X) && deformedPt.Z >= Math.Abs(deformedPt.Y))
            {
                return new Vector3d(deformedPt.X / deformedPt.Z * radius, deformedPt.Y / deformedPt.Z * radius, l - radius);
            }
            if (deformedPt.Z <= -Math.Abs(deformedPt.X) && deformedPt.Z <= -Math.Abs(deformedPt.Z))
            {
                return new Vector3d(double.PositiveInfinity, double.PositiveInfinity, double.PositiveInfinity);
            }
            if (deformedPt.Y >= Math.Abs(deformedPt.X) && deformedPt.Y >= Math.Abs(deformedPt.Z))
            {
                return new Vector3d(deformedPt.X / deformedPt.Y * radius, (2.0 - deformedPt.Z / deformedPt.Y) * radius, l - radius);
            }
            if (deformedPt.Y <= -Math.Abs(deformedPt.X) && deformedPt.Y <= -Math.Abs(deformedPt.Z))
            {
                return new Vector3d(-deformedPt.X / deformedPt.Y * radius, (-2.0 - deformedPt.Z / deformedPt.Y) * radius, l - radius);
            }
            if (deformedPt.X >= Math.Abs(deformedPt.Y) && deformedPt.X >= Math.Abs(deformedPt.Z))
            {
                return new Vector3d((2.0 - deformedPt.Z / deformedPt.X * radius), deformedPt.Y / deformedPt.X * radius, l - radius);
            }
            if (deformedPt.X <= -Math.Abs(deformedPt.Y) && deformedPt.X <= -Math.Abs(deformedPt.Z))
            {
                return new Vector3d((-2.0 - deformedPt.Z / deformedPt.X) * radius - deformedPt.Y / deformedPt.X * radius, l - radius);
            }
            //should never reach here
            Debug.LogWarning("ReloteRim::Generator::Planet::Producer::Core::Terrain::SpericalDeformation::DeformToLocal fail!!");
            return new Vector3d();
        }
        public override Box2d DeformedToLocalBounds(Vector3d deformedCenter, double deformedRadius)
        {
            Vector3d p = DeformedToLocal(deformedCenter);
            double r = deformedRadius;
            if (double.IsInfinity(p.X) || double.IsInfinity(p.Y))
                return new Box2d();
            double k = (1.0 - r * r / (2.0 * radius * radius)) * (new Vector3d(p.X, p.Y, radius).Magnitude());
            double a = k * k - p.X * p.X;
            double b = k * k - p.Y * p.Y;
            double c = -2.0 * p.X * p.Y;
            double d = -2.0 * radius * radius * p.X;
            double e = -2.0 * radius * radius * p.Y;
            double f = radius * radius * (k * k - radius * radius);

            double A = c * c - 4.0 * a * b;
            double B = 2.0 * c * e - 4.0 * b * d;
            double C = e * e - 4.0 * b * f;
            double D = Math.Sqrt(B * B - 4.0 * A * C);
            //we get x axis
            double x1 = (-B - D) / (2.0 * A);
            double x2 = (-B + D) / (2.0 * A);
            //now we change formals
            B = 2.0 * c * d - 4.0 * a * e;
            C = d * d - 4.0 * a * f;
            D = Math.Sqrt(B * B - 4.0 * A * C);
            //we get y axis
            double y1 = (-B - D) / (2.0 * A);
            double y2 = (-B + D) / (2.0 * A);
            return new Box2d(new Vector2d(x1, y1), new Vector2d(x2, y2));
        }
        public override Matrix4d DeformedToTangentFrame(Vector3d deformedPt)
        {
            Vector3d UZ = deformedPt.Normalized();
            Vector3d UX = (new Vector3d(0, 1, 0)).Cross(UZ).Normalized();
            Vector3d UY = UZ.Cross(UX);
            return new Matrix4d(UX.X, UX.Y, UX.Z, 0.0,
                                UY.X, UY.Y, UY.Z, 0.0,
                                UZ.X, UZ.Y, UZ.Z, -radius,
                                0.0, 0.0, 0.0, 1.0);
        }
        public override Frustum.VISIBILTY GetVisibility(TerrainNode node, Box3d localBox)
        {
            Vector3d[] deformedBox = new Vector3d[4];
            deformedBox[0] = LocalToDeformed(new Vector3d(localBox.XMin, localBox.YMin, localBox.ZMin));
            deformedBox[1] = LocalToDeformed(new Vector3d(localBox.XMax, localBox.YMin, localBox.ZMin));
            deformedBox[2] = LocalToDeformed(new Vector3d(localBox.XMax, localBox.YMax, localBox.ZMin));
            deformedBox[3] = LocalToDeformed(new Vector3d(localBox.XMin, localBox.YMax, localBox.ZMin));
            double a = (localBox.ZMax + radius) / (localBox.ZMin + radius);
            double x = (localBox.XMax - localBox.XMin) / 2 * a;
            double y = (localBox.YMax - localBox.YMin) / 2 * a;
            double z = localBox.XMax + radius;
            double factor = Math.Sqrt(x * x + y * y + z * z) / (localBox.ZMin + radius);
            Vector4d[] deformFrustumPlanes = node.DeformedFrustumPlanes;
            Frustum.VISIBILTY v1 = GetVisibility(deformFrustumPlanes[0], deformedBox, factor);
            Frustum.VISIBILTY v2 = GetVisibility(deformFrustumPlanes[1], deformedBox, factor);
            Frustum.VISIBILTY v3 = GetVisibility(deformFrustumPlanes[2], deformedBox, factor);
            Frustum.VISIBILTY v4 = GetVisibility(deformFrustumPlanes[3], deformedBox, factor);
            if (v1 == Frustum.VISIBILTY.INVISIBLE || v2 == Frustum.VISIBILTY.INVISIBLE || v3 == Frustum.VISIBILTY.INVISIBLE || v4 == Frustum.VISIBILTY.INVISIBLE)
            {
                return Frustum.VISIBILTY.INVISIBLE;
            }

            Vector3d v = node.DeformedCameraPostion;
            double lSq = v.SqrMagnitude();
            double raduisMin = radius + Math.Min(0.0, localBox.ZMin);
            double raduisMax = radius + localBox.ZMax;
            double raduisSqMin = raduisMin * raduisMin;
            double raduisSqMax = raduisMax * raduisMax;
            Vector4d farPlane = new Vector4d(v.X, v.Y, v.Z, Math.Sqrt((lSq - raduisSqMin) * (raduisSqMax - raduisSqMin)) - raduisSqMin);
            Frustum.VISIBILTY plane = GetVisibility(farPlane, deformedBox, factor);
            if (plane == Frustum.VISIBILTY.INVISIBLE)
            {
                return Frustum.VISIBILTY.INVISIBLE;
            }

            if (v1 == Frustum.VISIBILTY.FULLY &&
                v2 == Frustum.VISIBILTY.FULLY &&
                v3 == Frustum.VISIBILTY.FULLY &&
                v4 == Frustum.VISIBILTY.FULLY &&
                plane == Frustum.VISIBILTY.FULLY)
                return Frustum.VISIBILTY.FULLY;
            return Frustum.VISIBILTY.PARTIALLY;
        }

        public static Frustum.VISIBILTY GetVisibility(Vector4d clip, Vector3d[] planes, double factor)
        {
            Frustum.VISIBILTY result = Frustum.VISIBILTY.PARTIALLY;
            double o = planes[0].X * clip.X + planes[0].Y * clip.Y + planes[0].Z * clip.Z;
            bool partially = o + clip.W > 0.0;
            if ((o * factor + clip.W > 0.0) == partially)
            {
                o = planes[1].X * clip.X + planes[1].Y * clip.Y + planes[1].Z * clip.Z;
                if ((o + clip.W > 0.0) == partially && (o * factor + clip.W > 0.0) == partially)
                {
                    o = planes[2].X * clip.X + planes[2].Y * clip.Y + planes[2].Z * clip.Z;
                    if ((o + clip.W > 0.0) == partially && (o * factor + clip.W > 0.0) == partially)
                    {
                        o = planes[3].X * clip.X + planes[3].Y * clip.Y + planes[3].Z * clip.Z;
                        result = ((o + clip.W > 0.0) == partially && (o * factor + clip.W > 0.0) == partially) ? (partially ? Frustum.VISIBILTY.FULLY : Frustum.VISIBILTY.INVISIBLE) : Frustum.VISIBILTY.PARTIALLY;
                    }
                }
            }
            return result;
        }

        public override void SetUniforms(TerrainNode node, Material mat)
        {
            if (node == null || mat == null)
                return;
            base.SetUniforms(node, mat);
            mat.SetFloat(m_uniforms.Raduis, (float)radius);
        }

        protected override void SetScreenUniforms(TerrainNode node, TerrainQuaternion quad, MaterialPropertyBlock matPropertyBlock)
        {
            double ox = quad.OX;
            double oy = quad.OY;
            double length = quad.Length;

            Vector3d v1 = new Vector3d(ox, oy, radius);
            Vector3d v2 = new Vector3d(ox + length, oy, radius);
            Vector3d v3 = new Vector3d(ox, oy + length, radius);
            Vector3d v4 = new Vector3d(ox + length, oy + length, radius);
            Vector3d vc = (v1 + v4) * 0.5;
            double l0 = 0.0, l1 = 0.0, l2 = 0.0, l3 = 0.0;
            Vector3d vv1 = v1.Normalized(ref l0);
            Vector3d vv2 = v2.Normalized(ref l1);
            Vector3d vv3 = v3.Normalized(ref l2);
            Vector3d vv4 = v4.Normalized(ref l3);

            Matrix4d deformedCorners = new Matrix4d(vv1.X * radius, vv2.X * radius, vv3.X * radius, vv4.X * radius,
                                                        vv1.Y * radius, vv2.Y * radius, vv3.Y * radius, vv4.Y * radius,
                                                        vv1.Z * radius, vv2.Z * radius, vv3.Z * radius, vv4.Z * radius,
                                                        1.0, 1.0, 1.0, 1.0);
            matPropertyBlock.AddMatrix(m_uniforms.ScreenQuadCorners, (m_localToScreen * deformedCorners).ToMatrix4x4());

            Matrix4d deformedVerts = new Matrix4d(vv1.X, vv2.X, vv3.X, vv4.X,
                                                    vv1.Y, vv2.Y, vv3.Y, vv4.Y,
                                                    vv1.Z, vv2.Z, vv3.Z, vv4.Z,
                                                    0.0, 0.0, 0.0, 0.0);
            matPropertyBlock.AddMatrix(m_uniforms.ScreenQuadVerticals, deformedVerts.ToMatrix4x4());
            matPropertyBlock.AddVector(m_uniforms.ScreemQuasCornerNorms, new Vector4((float)l0, (float)l1, (float)l2, (float)l3));

            Vector3d uz = vc.Normalized();
            Vector3d ux = (new Vector3d(0, 1, 0)).Cross(uz).Normalized();
            Vector3d uy = uz.Cross(ux);

            Matrix4d ltw = node.LocalToWorld;
            Matrix3d m = new Matrix3d(ux.X, uy.X, uz.X,
                                                        ux.Y, uy.Y, uz.Y,
                                                        ux.Z, uy.Z, uz.Z);

            Matrix3d tangentFrameToWorld = new Matrix3d(ltw.Matrix[0, 0], ltw.Matrix[0, 1], ltw.Matrix[0, 2],
                                        ltw.Matrix[1, 0], ltw.Matrix[1, 1], ltw.Matrix[1, 2],
                                        ltw.Matrix[2, 0], ltw.Matrix[2, 1], ltw.Matrix[2, 2]);

            matPropertyBlock.AddMatrix(m_uniforms.TangentFrameToWorld, (tangentFrameToWorld * m).ToMatrix4x4());
        }
    }
}
