﻿using ReloteRim.Generator.Planet.Core.Node.Tile;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    /// <summary>
    /// An internal QuaternionTree to store the texture tile associated with each terrain quad.
    /// </summary>
    public class QuaternionTree
    {
        /// <summary>
        /// if a tile is needed for this quad
        /// </summary>
        public bool NeedTile { get; set; }
        /// <summary>
        /// if a tile is needed for this quad
        /// </summary>
        public bool IsTileActive { get; set; }
        /// <summary>
        /// The parent quad of this quad.
        /// </summary>
        public QuaternionTree Parent { get; private set; }
        /// <summary>
        /// The texture tile associated with this quad.
        /// </summary>
        public Tile Tile { get; set; }
        /// <summary>
        /// The sub quaterniontrees of this quad.
        /// </summary>
        public QuaternionTree[] Children { get; set; }
        public QuaternionTree(QuaternionTree parant)
        {
            Parent = parant;
            Children = new QuaternionTree[4];
        }

        public bool IsLeaf()
        {
            return (Children[0] == null);
        }
        /// <summary>
        /// Deletes All trees subelements. Releases all the corresponding texture tiles.
        /// </summary>
        /// <param name="owner">TileSamplerNode</param>
        public void RecursiveDeleteChildren(TileSampler owner)
        {
            if (Children[0] != null)
            {
                for (int i = 0; i < 4; i++)
                {
                    Children[i].RecursiveDelete(owner);
                    Children[i] = null;
                }
            }
        }

        /// <summary>
        /// Deletes All trees subelements. Releases all the corresponding texture tiles.
        /// </summary>
        /// <param name="owner">TileSamplerNode</param>
        public void RecursiveDelete(TileSampler owner)
        {
            if (Tile != null && owner != null)
            {

            }
        }


    }
}
