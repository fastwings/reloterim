﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    /// <summary>
    /// Controller used to collect user input and move the view (TerrainView or PlanetView)
    /// Provides smooth interpolation from the views current to new position
    /// </summary>
    public class Controller : MonoBehaviour
    {
        //Speed settings for the different typs of movement
        [SerializeField]
        public double m_moveSpeed = 1e-3;

        [SerializeField]
        public double m_turnSpeed = 5e-3;

        [SerializeField]
        public double m_zoomSpeed = 1.0;

        [SerializeField]
        public double m_rotateSpeed = 0.1;

        [SerializeField]
        public double m_dragSpeed = 0.01;

        //True to use exponential damping to go to target positions, false to go to target positions directly.
        [SerializeField]
        public bool m_smooth = true;
        //True if the PAGE_DOWN key is currently pressed.
        bool m_near;
        //True if the PAGE_UP key is currently pressed.
        bool m_far;
        //True if the UP key is currently pressed.
        bool m_forward;
        //True if the DOWN key is currently pressed.
        bool m_backward;
        //True if the LEFT key is currently pressed.
        bool m_left;
        //True if the RIGHT key is currently pressed.
        bool m_right;
        //True if the target position target is initialized.
        bool m_initialized;
        Position m_target;
        Position m_start;
        Position m_end;
        TerrainView m_view;
        Vector3d m_previousMousePos;
        /**
        * Animation status. Negative values mean no animation.
        * 0 corresponds to the start position, 1 to the end position,
        * and values between 0 and 1 to intermediate positions between
        * the start and end positions.
        */
        double m_animation = -1.0;
        public TerrainView GetView()
        {
            return m_view;
        }// Use this for initialization
        public void Start()
        {
            m_view = GetComponent<TerrainView>();

            m_target = new Position();
            m_start = new Position();
            m_end = new Position();
            m_previousMousePos = new Vector3d(Input.mousePosition);
        }
        /// <summary>
        /// Update is called once per frame
        /// </summary>
        public void UpdateController()
        {
            double dt = Time.deltaTime * 1000.0;

            if (!m_initialized)
            {
                m_target = GetPosition(m_target);
                m_initialized = true;
            }
            //lets check the inputs 
            KeyDown();
            MouseWheel();
            MouseMotion();

            //now we handle the animations
            if (m_animation >= 0.0)
            {
                m_animation = m_view.Interpolate(m_start.X, m_start.Y, m_start.Theta, m_start.Phi, m_start.Distance,
                                                m_end.X, m_end.Y, m_end.Theta, m_end.Phi, m_end.Distance, m_animation);
                if (m_animation == 1.0)
                {
                    m_target = GetPosition(m_target);
                    m_animation = -1.0;
                }
            }
            else
            {
                UpdateController(dt);
            }
            m_view.UpdateView();
        }
        private void UpdateController(double dt)
        {
            double dzFactor = Math.Pow(1.02, Math.Min(dt, 1.0));
            double speed = Math.Max(m_view.GetHeight(), 1.0);
            if (m_near)
            {
                m_target.Distance = m_target.Distance / (dzFactor * m_zoomSpeed);
            }
            else if (m_far)
            {
                m_target.Distance = m_target.Distance * (dzFactor * m_zoomSpeed);
            }
            Position pos = new Position();
            pos = GetPosition(pos);
            SetPosition(m_target);

            //movement logic that handle  by the speed movement
            if (m_forward)
            {
                m_view.MoveForward(speed * dt * m_moveSpeed);
            }
            else if (m_backward)
            {
                m_view.MoveForward(-speed * dt * m_moveSpeed);
            }
            if (m_left)
            {
                m_view.Turn(dt * m_turnSpeed);
            }
            else if (m_right)
            {
                m_view.Turn(-dt * m_turnSpeed);
            }
            //update the position
            m_target = GetPosition(m_target);

            //we need smooth(if enabled) the movement
            if (m_smooth)
            {
                double lerp = 1.0 - Math.Exp(-dt * 2.301e-3);
                double x = 0.0;
                double y = 0.0;
                m_view.InterpolatePosition(pos.X, pos.Y, m_target.X, m_target.Y, lerp, ref x, ref y);
                pos.X = x;
                pos.Y = y;
                pos.Theta = Mix(pos.Theta, m_target.Theta, lerp);
                pos.Phi = Mix(pos.Phi, m_target.Phi, lerp);
                pos.Distance = Mix(pos.Distance, m_target.Distance, lerp);
                SetPosition(pos);
            }
            else
            {
                SetPosition(m_target);
            }
        }
        /// <summary>
        /// will update the current position requested object
        /// </summary>
        /// <param name="pos">Position</param>
        /// <returns>Position</returns>
        private Position GetPosition(Position pos)
        {
            pos.Distance = m_view.Position.Distance;
            pos.X = m_view.Position.X;
            pos.Y = m_view.Position.Y;
            pos.Theta = m_view.Position.Theta;
            pos.Phi = m_view.Position.Phi;
            return pos;
        }
        private void SetPosition(Position pos)
        {
            m_view.Position.X = pos.X;
            m_view.Position.Y = pos.Y;
            m_view.Position.Distance = pos.Distance;
            m_view.Position.Phi = pos.Phi;
            m_view.Position.Theta = pos.Theta;
            m_animation = -1.0;
        }
        /// <summary>
        /// if interpolation between point x and y more then the max point of x,y of infintiy
        /// return y else see code.
        /// </summary>
        /// <param name="x">double</param>
        /// <param name="y">double</param>
        /// <param name="t">double</param>
        /// <returns>double</returns>
        private double Mix(double x, double y, double t)
        {
            if (Math.Abs(x - y) < Math.Max(x, y) * 1e-5)
            {
                return y;
            }
            return x * (1.0 - t) + y * t;
        }
        private void GoToPosition(Position pos)
        {
            m_start = GetPosition(pos);
            m_end = pos;
            m_animation = 0.0;
        }
        private void JumpToPosition(Position pos)
        {
            m_target = GetPosition(pos);
        }
        private void MouseWheel()
        {
            m_far = false;
            m_near = false;
            if (Input.GetAxis("Mouse ScrollWheel") < 0.0f || Input.GetKey(KeyCode.PageUp))
            {
                m_far = true;
            }
            if (Input.GetAxis("Mouse ScrollWheel") > 0.0f || Input.GetKey(KeyCode.PageDown))
            {
                m_near = true;
            }
        }
        private void KeyDown()
        {
            m_forward = Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W);
            m_backward = Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S);
            m_left = Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A);
            m_right = Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D);
        }

        private void MouseMotion()
        {
            if (Input.GetMouseButton(0) && Input.GetKey(KeyCode.LeftControl))
            {
                m_target.Phi -= Input.GetAxis("Mouse X") * m_rotateSpeed;
                m_target.Theta += Input.GetAxis("Mouse Y") * m_rotateSpeed;
            }
            else if (Input.GetMouseButton(0))
            {
                Vector3d mousePos = new Vector3d();
                Vector3d preMousePos = m_previousMousePos;
                mousePos.X = Input.mousePosition.x;
                mousePos.Y = Input.mousePosition.y;
                mousePos.Z = 0.0;
                preMousePos.Z = 0.0;
                Vector3d oldPosition = m_view.CameraToWorldMatrix * preMousePos;
                Vector3d posistion = oldPosition;

                if (!(double.IsNaN(oldPosition.X) || double.IsNaN(oldPosition.Y) || double.IsNaN(oldPosition.Z) || double.IsNaN(posistion.X) || double.IsNaN(posistion.Y) || double.IsNaN(posistion.Z)))
                {
                    Position pos = new Position();
                    pos = GetPosition(pos);
                    SetPosition(m_target);

                    m_view.Move(oldPosition, posistion, m_dragSpeed);
                    m_target = GetPosition(m_target);
                    SetPosition(pos);
                }
            }
            m_previousMousePos = new Vector3d(Input.mousePosition);
        }
    }
}
