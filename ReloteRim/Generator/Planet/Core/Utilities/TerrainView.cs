﻿using System;
using ReloteRim.Common.Utilities.Math;
using ReloteRim.Generator.Planet.Core.Storage.Data;
using UnityEngine;

namespace ReloteRim.Generator.Planet.Core.Utilities
{
    /// <summary>
    /// A view for flat terrains. The camera position is specified
    /// from a "look at" position (x0,y0) on ground, with a distance d between
    /// camera and this position, and two angles (theta,phi) for the direction
    /// of this vector.
    /// </summary>
    [RequireComponent(typeof(Camera))]
    [RequireComponent(typeof(Controller))]
    public class TerrainView : MonoBehaviour
    {
        [SerializeField]
        public bool m_printPositionOnClose = false;
        [SerializeField]
        protected Position m_position;

        //The camera position in world space resulting from the x0,y0,theta,phi and distance parameters.
        protected Vector3d m_worldPos;

        #region Preprtys
        /// <summary>
        /// The x0,y0,theta,phi and distance parameters
        /// </summary>
        public Position Position
        {
            get
            {
                return m_position;
            }
            protected set
            {
                m_position = value;
            }
        }
        public Camera ActiveCamera = Camera.current;
        /// <summary>
        /// the height below the camera of the ground (if its been read back from gpu)
        /// </summary>
        public virtual double GroundHeight { get; set; }
        /// <summary>
        /// The localToWorld matrix in double precision
        /// </summary>
        public Matrix4d WorldToCameraMatrix { get; protected set; }
        /// <summary>
        /// The inverse world to camera matrix
        /// </summary>
        public Matrix4d CameraToWorldMatrix { get; protected set; }
        /// <summary>
        /// The projectionMatrix in double precision
        /// </summary>
        public Matrix4d CameraToScreenMatrix { get; protected set; }
        /// <summary>
        /// inverse projection matrix
        /// </summary>
        public Matrix4d ScreenToCameraMatrix { get; protected set; }
        /// <summary>
        /// The world camera pos
        /// </summary>
        public Vector3d WorldCameraPosition { get; protected set; }
        /// <summary>
        /// The camera direction
        /// </summary>
        public Vector3d CameraDirection { get; protected set; }

        #endregion

        #region Method
        #region Public
        /// <summary>
        /// returns the position the camera is currently looking at
        /// </summary>
        /// <returns>Vector3d</returns>
        public virtual Vector3d GetLookAtPosition()
        {
            return new Vector3d(m_position.X, m_position.Y, 0.0f);
        }

        /// <summary>
        /// will get the Height from origin
        /// </summary>
        /// <returns></returns>
        public virtual double GetHeight()
        {
            return m_worldPos.Z;
        }
        /// <summary>
        /// Any constraints you need on the position are applied here
        /// </summary>
        public virtual void Constrain()
        {
            m_position.Theta = Math.Max(0.0001, Math.Min(Math.PI, m_position.Theta));
            m_position.Distance = Math.Max(0.1, m_position.Distance);
        }
        public virtual void Move(Vector3d old_position, Vector3d position, double speed)
        {
            m_position.X -= (position.X - old_position.X) * speed * Math.Max(1.0, GetHeight());
            m_position.Y -= (position.Y - old_position.Y) * speed * Math.Max(1.0, GetHeight());
        }
        public virtual void MoveForward(double distance)
        {
            m_position.X -= Math.Sin(m_position.Phi) * distance;
            m_position.Y += Math.Cos(m_position.Phi) * distance;
        }
        public virtual void Turn(double angle)
        {
            m_position.Phi += angle;
        }
        /// <summary>
        /// Sets the position as the interpolation of the two given positions with
        /// the interpolation parameter t (between 0 and 1). The source position is
        /// sx0,sy0,stheta,sphi,sd, the destination is dx0,dy0,dtheta,dphi,dd.
        /// </summary>
        /// <param name="sx0"></param>
        /// <param name="sy0"></param>
        /// <param name="stheta"></param>
        /// <param name="sphi"></param>
        /// <param name="sd"></param>
        /// <param name="dx0"></param>
        /// <param name="dy0"></param>
        /// <param name="dtheta"></param>
        /// <param name="dphi"></param>
        /// <param name="dd"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public virtual double Interpolate(double sx0, double sy0, double stheta, double sphi, double sd,
                                           double dx0, double dy0, double dtheta, double dphi, double dd, double t)
        {
            // TODO interpolation
            m_position.X = dx0;
            m_position.Y = dy0;
            m_position.Theta = dtheta;
            m_position.Phi = dphi;
            m_position.Distance = dd;
            return 1.0;
        }
        public virtual void InterpolatePosition(double sx0, double sy0, double dx0, double dy0, double t, ref double x0, ref double y0)
        {
            m_position.X = sx0 * (1.0 - t) + dx0 * t;
            m_position.Y = sy0 * (1.0 - t) + dy0 * t;
        }
        /// <summary>
        /// Returns a direction interpolated between the two given direction.
        /// </summary>
        /// <param name="slon">slon start longitude</param>
        /// <param name="slat">slat start latitude</param>
        /// <param name="elon">elon end longitude</param>
        /// <param name="elat">elat end latitude</param>
        /// <param name="t">t interpolation parameter between 0 and 1</param>
        /// <param name="lon">lon interpolated longitude</param>
        /// <param name="lat">lat interpolated latitude</param>
        public virtual void InterpolateDirection(double slon, double slat, double elon, double elat, double t, ref double lon, ref double lat)
        {
            Vector3d s = new Vector3d(Math.Cos(slon) * Math.Cos(slat), Math.Sin(slon) * Math.Cos(slat), Math.Sin(slat));
            Vector3d e = new Vector3d(Math.Cos(elon) * Math.Cos(elat), Math.Sin(elon) * Math.Cos(elat), Math.Sin(elat));
            Vector3d v = (s * (1.0 - t) + e * t).Normalized();
            lat = MathUtility.SafeASin(v.Z);
            lon = Math.Atan2(v.Y, v.X);
        }

        #endregion
        #region Protected
        protected virtual void SetWorldToCameraMatrix()
        {
            Vector3d po = new Vector3d(m_position.X, m_position.Y, 0.0);
            Vector3d px = new Vector3d(1.0, 0.0, 0.0);
            Vector3d py = new Vector3d(0.0, 1.0, 0.0);
            Vector3d pz = new Vector3d(0.0, 0.0, 1.0);
            double ct = Math.Cos(m_position.Theta);
            double st = Math.Sin(m_position.Theta);
            double cp = Math.Cos(m_position.Phi);
            double sp = Math.Sin(m_position.Phi);

            Vector3d cx = px * cp + py * sp;
            Vector3d cy = (px * -1.0) * sp * ct + py * cp * ct + pz * st;
            Vector3d cz = px * sp * st - py * cp * st + pz * ct;

            m_worldPos = po + cz * m_position.Distance;
            if (m_worldPos.Z < GroundHeight + 10.0)
            {
                m_worldPos.Z = GroundHeight + 10.0;
            }

            Matrix4d view = new Matrix4d(cx.X, cx.Y, cx.Z, 0.0,
                                                cy.X, cy.Y, cy.Z, 0.0,
                                                cz.X, cz.Y, cz.Z, 0.0,
                                                0.0, 0.0, 0.0, 1.0);
            WorldToCameraMatrix = view * Matrix4d.Translate(m_worldPos * -1.0);

            WorldToCameraMatrix.Matrix[0, 0] *= -1.0;
            WorldToCameraMatrix.Matrix[0, 1] *= -1.0;
            WorldToCameraMatrix.Matrix[0, 2] *= -1.0;
            WorldToCameraMatrix.Matrix[0, 3] *= -1.0;

            CameraToWorldMatrix = WorldToCameraMatrix.Inverse();
            ActiveCamera.worldToCameraMatrix = WorldToCameraMatrix.ToMatrix4x4();
            ActiveCamera.transform.position = m_worldPos.Vector;
        }
        /// <summary>
        /// Get a copy of the projection matrix and convert in to double precision
        /// and apply the bias if using dx11 and flip Y if deferred rendering is used 
        /// </summary>
        protected virtual void SetProjectionMatrix()
        {
            float h = (float)(GetHeight() - GroundHeight);
            ActiveCamera.nearClipPlane = 0.1f * h;
            ActiveCamera.farClipPlane = 1e6f * h;

            ActiveCamera.ResetProjectionMatrix();

            Matrix4x4 p = ActiveCamera.projectionMatrix;
            bool d3d = SystemInfo.graphicsDeviceVersion.IndexOf("Direct3D") > -1;
            if (d3d)
            {
                if (ActiveCamera.actualRenderingPath == RenderingPath.DeferredLighting)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        p[1, i] = -p[1, i];
                    }
                    for (int i = 0; i < 4; i++)
                    {
                        p[2, i] = p[2, i] * 0.5f + p[3, i] * 0.5f;
                    }
                }
            }
            CameraToScreenMatrix = new Matrix4d(p);
            ScreenToCameraMatrix = CameraToScreenMatrix.Inverse();
        }

        #endregion

        #endregion

        #region Unity Events
        protected virtual void Start()
        {
            WorldToCameraMatrix = Matrix4d.Identity();
            CameraToWorldMatrix = Matrix4d.Identity();
            CameraToScreenMatrix = Matrix4d.Identity();
            ScreenToCameraMatrix = Matrix4d.Identity();
            WorldCameraPosition = new Vector3d();
            m_worldPos = new Vector3d();

            Constrain();
        }
        /// <summary>
        /// On destroy print out the camera position. This can useful when try to position
        /// the camera in a certain spot
        /// </summary>
        protected virtual void OnDestroy()
        {
            if (m_printPositionOnClose)
            {
                Debug.Log("TerrainView  Event:OnDestroy Position: " + m_position.ToString());
            }
        }
        public virtual void UpdateView()
        {
            Constrain();

        }
        #endregion
    }
}
